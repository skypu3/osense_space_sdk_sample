#!/bin/bash
rm -rf build
rm -rf deploy
npm run build
mkdir -p deploy
mv build deploy/spaceeditor
aws s3 sync deploy/ s3://mochar-editor 
aws cloudfront create-invalidation --distribution-id E3TE5DCRMWVADU  --paths "/*" 