import * as THREE from "three";
import Global from 'core/three/base/Global';
import Scene from 'core/three/scene/Scene';
import AnimeViewportController from "core/three/base/AnimeViewportController";
import { IRaycastResponse } from 'core/three/base/Raycaster';
import { Format } from 'core/three/scene/Format';
import AimationMgr from 'core/three/base/AnimationMgr';
import { ResourcesType } from "core/three/base/ResourcesMgr";
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";
import EventEmitter from 'eventemitter3';
import MediaManager from "core/three/base/MediaManager";
import ObjectController from "core/three/base/ObjectController";
import { saveAs } from 'file-saver';
import FirstPersonControls, { MovingMode } from "core/three/base/FirstPersonControls";
import SceneRaycaster, { RaycastActionType } from 'core/three/SceneRaycaster/SceneRaycaster';
import Stats from 'stats.js';
import store from "core/store/store";
import { SpaceSdkCmd } from "core/store/actions/iframe";
import { TagType } from "core/store/actions/tag";
import { ViewerMode } from "core/store/actions/mode";
import { Mesh } from "three";
import MouseCursor from "core/three/mediaObject/mousecursor/MouseCursor";
import MediaObject3D, { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Media from "core/three/mediaObject/media/Media";
import NewTag from "core/three/mediaObject/media/image/NewTag";
const TWEEN = require( '@tweenjs/tween.js' );

let self: ThreeView;
interface UIFunctions
{
    showPropertyMenu: any
    showFloatDeleteButton: any;
}

// enum Limitation
// {
//     floorHeight = 0.5
// }

export enum ThreeEvents
{
    raycastMouseUp = 'raycast-mouse-up',
    raycastMouseEnter = 'raycast-mouse-enter',
    raycastMouseLeave = 'raycast-mouse-leave',
}

export default class ThreeView
{
    // TODO 模組後移除
    stats: Stats;
    animationMgr: AimationMgr = Global.inst.animationMgr;
    viewportControls: AnimeViewportController = Global.inst.viewportController;
    mouseCursor: MouseCursor;

    mediaManager: MediaManager = Global.inst.mediaManager;
    objectController: ObjectController;
    scene: Scene = null;

    firstPersonControls: FirstPersonControls = Global.inst.firstPersonControls;
    media
    rawData: any;

    //callback to set UI
    uiFunctions: UIFunctions;

    // movingMode: MovingMode;
    sceneRaycaster: SceneRaycaster;
    events: EventEmitter;
    skybox: Mesh;
    checkAutoPlay: boolean = true;

    constructor( uiFunctions: UIFunctions )
    {
        this.uiFunctions = uiFunctions;
        const container = document.getElementById( "three-root" ) as HTMLCanvasElement;

        const mode = store.getState().modeReducer;
        if ( mode === ViewerMode.Editor )
        {
            if ( window.devicePixelRatio >= 1 )
            {
                Global.inst.scaleDeviceRatio = 1 / window.devicePixelRatio;
            }
        }
        this.viewportControls.init( container );

        this.firstPersonControls.init( this.viewportControls.camera, this.viewportControls.OrbitControls, container )

        this.scene = new Scene();
        this.events = new EventEmitter();

        this.sceneRaycaster = new SceneRaycaster( this.scene, container, this.viewportControls.renderer, this.viewportControls.camera, 1, 2 );

        this.godown = this.godown.bind( this )
        this.goup = this.goup.bind( this )

        this.mediaManager.init( this.scene, this.viewportControls );
        this.mediaManager.setAutoplay( false );
        this.objectController = new ObjectController( this.viewportControls, container );
        this.objectController.onClick = uiFunctions.showPropertyMenu;
        Global.inst.objectController = this.objectController;
        this.sceneRaycaster.mouseUp = this.objectController.raycastMouseUp.bind( this.objectController );
        this.sceneRaycaster.mouseMove = this.objectController.raycastMouseMove.bind( this.objectController );
        this.sceneRaycaster.mouseDown = this.raycastMouseDown.bind( this );
        this.sceneRaycaster.mouseEnter = this.raycastMouseEnter.bind( this );
        this.sceneRaycaster.mouseLeave = this.raycastMouseLeave.bind( this );
        this.scene.add( this.objectController.controls );
        this.scene.add( this.objectController.hoverBbox );

        this.addKeyBoardEvent();
        self = this;
    }

    async parseInput( input: object, onProgress?: any ): Promise<Format.IScene>
    {
        return new Promise( async resolve =>
        {
            const data = input as Format.IScene;

            // resolve not extensible
            data.space = Object.create( data.space );
            data.postProcessing = input[ 'scene' ].postProcessing;
            // resolve read only
            data.hotspots = JSON.parse( JSON.stringify( data.hotspots ) );

            this.viewportControls.createPostProcessController( this.scene, data.postProcessing )

            //simple preload without progress bar
            // data.hotspots.forEach( hotspot =>
            // {
            //     if ( hotspot.previewUrl )
            //     {
            //         Global.inst.resLoad.load( hotspot.previewUrl, ResourcesType.Texture )
            //     } else
            //     {
            //         Global.inst.resLoad.load( hotspot.lowerSkybox, ResourcesType.Cubemap )
            //     }
            // } )

            // load gltf 
            const gltf: GLTF = await Global.inst.resLoad.load( data.space.glbUrl, ResourcesType.Model, onProgress );

            data.space.glb = gltf

            if ( data.space.isMp )
            {
                // glb and hotspot position rotate 90 degree
                data.space.glb.scene.quaternion.setFromAxisAngle(
                    new THREE.Vector3( 1, 0, 0 ), -Math.PI / 2
                )
                data.hotspots.forEach( hotspot =>
                {
                    const pos = new THREE.Vector3( hotspot.position.x, hotspot.position.y, hotspot.position.z );
                    pos.applyAxisAngle(
                        new THREE.Vector3( 1, 0, 0 ), -Math.PI / 2
                    )
                    hotspot.position.x = pos.x;
                    hotspot.position.y = pos.y;
                    hotspot.position.z = pos.z;
                } )
            }

            // extract y Roatation for panorama
            data.hotspots.forEach( hotspot =>
            {
                const { rotation } = hotspot;
                const mat = new THREE.Matrix4();
                mat.compose(
                    new THREE.Vector3( 0, 0, 0 ),
                    new THREE.Quaternion( rotation.x, rotation.y, rotation.z, rotation.w ),
                    new THREE.Vector3( 1.0, 1.0, 1.0 )
                );
                const euler = new THREE.Euler();
                euler.setFromRotationMatrix( mat );

                const rot = new THREE.Quaternion().setFromAxisAngle( new THREE.Vector3( 0, -1, 0 ), euler.z - ( data.space.isMp ? 1.57 : 0 ) );
                hotspot.rotation.x = rot.x;
                hotspot.rotation.y = rot.y;
                hotspot.rotation.z = rot.z;
                hotspot.rotation.w = rot.w;
            } )

            if ( data.space.skybox )
            {
                this.skybox = Global.inst.resLoad.loadEnvCubeMap( data.space.skybox )
                this.scene.add( this.skybox )
            }

            // use raycast find camera height
            data.space.glb.scene.updateMatrixWorld( true )

            if ( !data.space.isMp )
            {
                data.hotspots.forEach( hotspot =>
                {
                    const pos = new THREE.Vector3( hotspot.position.x, hotspot.position.y + hotspot.cameraHeight, hotspot.position.z );
                    const dir = new THREE.Vector3( 0, -1, 0 ).normalize();

                    const floorRaycaster = new THREE.Raycaster()
                    floorRaycaster.set( pos, dir )
                    const intersects = floorRaycaster.intersectObject( data.space.glb.scene, true )
                    if ( intersects.length > 0 )
                    {
                        const { y } = intersects[ 0 ].point;
                        hotspot.cameraHeight += hotspot.position.y - y;
                        hotspot.position.y = y;
                    }
                } )
            }

            resolve( data )
        } )
    }

    async init( info, onProgress?: any )
    {
        this.rawData = info;

        const color = info.space.cursorColor ? info.space.cursorColor : '#00ffea';
        this.mouseCursor = new MouseCursor( color );
        this.objectController.mouseCursor = this.mouseCursor;
        this.objectController.animationMgr = this.animationMgr;
        this.scene.add( this.mouseCursor );

        const data = await this.parseInput( info, onProgress )

        data.mediaObjects.forEach( media =>
        {
            media.type = media.type.toString().toUpperCase();
            this.mediaManager.createMediaObjects( [ media ] ).then( newObject =>
            {
                const { newMedias } = newObject;
                for ( let i = 0; i < newMedias.length; i++ )
                {
                    const media = newMedias[ i ]
                    this.sceneRaycaster.addTarget( media, RaycastActionType.OBJECT );
                }
            } )
        } )

        data.hotspots.forEach( hotspot =>
        {
            hotspot.hotspotColor = info.space.hotspotColor ? info.space.hotspotColor : '#ffffff';

            if ( !hotspot.hotspotScale )
            {
                hotspot.hotspotScale = info.space.hotspotScale ? info.space.hotspotScale : 0.0;
            }
        } );

        const { hotspotMeshes } = this.mediaManager.createHotspots( data.hotspots );
        this.animationMgr.initHotspotInfos( data.hotspots, hotspotMeshes );
        this.animationMgr.displayHotspotNeighbors( undefined )
        this.sceneRaycaster.addTarget( hotspotMeshes );

        const sceneModel = this.mediaManager.createSceneModel( data )
        this.sceneRaycaster.addTarget( sceneModel, RaycastActionType.BOTH )

        this.viewportControls.setFocusBbox( sceneModel.center, sceneModel.size );
        // this.createPositioningObjects( data.positioningObjects );

        this.animationMgr.init( this.viewportControls, sceneModel );
        this.animationMgr.enableCalcDurationByDistance = true;
        this.animationMgr.setAnimationDurationParameters( null, null, null, 250 )

        // this.animationMgr.enablePanoramaMesh = false;
        // this.mediaManager.hideHotspots = true;

        /**
         *  make transform object on floor
         */
        const raycaster = this.sceneRaycaster.screenRaycaster;
        this.objectController.setRaycastScene( pointer =>
        {
            const x = ( pointer.x + 1 ) / 2 * window.innerWidth;
            const y = -  ( pointer.y - 1 ) / 2 * window.innerHeight;

            const intersect = raycaster.getNormalAndPosition( { x, y } )
            let result: any = false;

            if ( intersect )
            {
                const distance = intersect.worldPosition.distanceTo( this.viewportControls.camera.position );
                var scalar = 1.0;

                if ( distance < 4 && distance >= 0 )
                {
                    scalar = 1e-2 * 0.8;
                }
                else if ( distance < 30.0 && distance >= 4 )
                {
                    scalar = 1e-1 * distance / 10;
                }
                else
                {
                    scalar = 1e-1 * 5;
                }
                const normal = new THREE.Vector3( 0, 0, 1 ).applyMatrix4( intersect.rotationMat4 );
                result = {
                    point: intersect.worldPosition.add( normal.clone().multiplyScalar( scalar ) ),
                    face: {
                        normal
                    },
                    object: sceneModel.object
                };

                // if ( Math.ceil( normal.y ) === 1 && Math.floor( intersect.worldPosition.y ) < Limitation.floorHeight )
                // {
                //     return false;
                // }
            }
            return result;
        } );
        if ( info.scene != null && info.scene.startHotspotId != null )
        {
            this.viewportControls.azimuthAngle = info.scene.azimuthAngle
            this.animationMgr.goHotspot( info.scene.startHotspotId, sceneModel.center, info.scene.polarAngle );
        }
        else
        {
            this.animationMgr.goHotspot( undefined, sceneModel.center );
        }

        setTimeout( () =>
        {
            // @ts-ignore
            this.enableViewerMode( false )
        }, 3000 )

        this.animationMgr.startAnimate()

        // fps div 
        // if ( process.env.NODE_ENV === "development" )
        // {
        // this.stats = new Stats();
        // this.stats.showPanel( 0 );
        // this.stats.dom.childNodes.forEach( element =>
        // {
        //     element.style.width = '256px';
        //     element.style.height = 'auto';
        // } );
        // document.body.appendChild( this.stats.dom );
        // }

    }

    private raycastMouseEnter( object: MediaObject3D )
    {
        if ( object instanceof Media )
        {
            this.events.emit( ThreeEvents.raycastMouseEnter, object )
        }
    }

    private raycastMouseLeave( object: MediaObject3D )
    {
        if ( object instanceof Media )
        {
            this.events.emit( ThreeEvents.raycastMouseLeave, object )
        }
    }

    private raycastMouseDown( info: IRaycastResponse )
    {
        // handle condition when user click screen
        if ( this.checkAutoPlay )
        {
            if ( store.getState().iframeReducer !== null )
            {
                const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
                send( SpaceSdkCmd.userhasinteract, {} );
            }
            this.checkAutoPlay = false;
        }
    }

    // private raycastMouseUp( info: IRaycastResponse )
    // {
    //     if ( !info ) return;
    //     // if ( RaycastType.hasProperty( info.target.type, RaycastType.SHOWMODAL ) )
    //     // {
    //     //     this.uiFunctions.showModalBox( info.target );
    //     // }
    //     if ( RaycastType.hasProperty( info.target.type, RaycastType.MOVE ) )
    //     {
    //         const { target, position } = info;

    //         if ( this.viewportControls.isTop )
    //         {
    //             this.animationMgr.goHotspot( target.id, position );
    //         } else
    //         {
    //             this.animationMgr.goHotspot( target.id, position );
    //             // if ( this.movingMode === MovingMode.Viewer )
    //             // {
    //             //     this.animationMgr.goHotspot( target.id, position );
    //             // } else
    //             // {
    //             //     this.animationMgr.goHotspot( target.id );
    //             // }
    //         }
    //     }
    //     else
    //     {
    //         this.objectController.detachEvent();
    //     }

    // }

    // private raycastMousemove( info: IRaycastResponse )
    // {
    //     if ( info )
    //     {
    //         const { rotation, position } = info;
    //         this.mouseCursor.set( rotation, position )
    //     } else
    //     {
    //         this.mouseCursor.opacity = 0.0
    //     }

    // }

    public handleTagAction( media?: Media, data?: TagType )
    {
    }

    public playVideos()
    {
        const videos = this.mediaManager.mediaList.filter( ( media ) => media.type === EObjectType.VIDEO );
        videos.forEach( ( video ) =>
        {
            if ( video.autoPlay )
            {
                video.toggle()
            }
        } );
    }

    public save()
    {
        const json = this.mediaManager.toJson()
        const blob = new Blob( [ JSON.stringify( json, null, 4 ) ], { type: "application/json" } );
        saveAs( blob, `${ this.rawData.scene.id }.json` );
    }

    public godown()
    {
        this.animationMgr.godown()
    }

    public goup()
    {
        this.animationMgr.goTop()
    }

    public async createObject( data )
    {
        //TODO: dynamic add object to scene
        let mediaData: Format.IMedia
        mediaData = data;
        mediaData.position = { x: 0, y: 0, z: 0 }
        mediaData.rotation = { x: 0, y: 0, z: 0, w: 0 }
        if ( data.type == EObjectType.TAG )
        {
            mediaData.scale = { x: 0.17, y: 0.17, z: 0.17 }
        } else
        {
            mediaData.scale = { x: 1, y: 1, z: 1 }
        }
        mediaData.editable = true;
        let initialBox: THREE.Mesh;

        if ( mediaData.type === EObjectType.MODEL )
        {
            let boxGeometry = new THREE.BoxGeometry( data.maxWidth ?? 1, data.maxWidth ?? 1, data.maxWidth ?? 1, 1, 1 );
            let material = new THREE.MeshBasicMaterial( { color: 0x420042 } );
            initialBox = new THREE.Mesh( boxGeometry, material );
            this.scene.add( initialBox );
            this.objectController.attachNewObject( initialBox );
        }

        const { raycastTargets, newMedias } = await this.mediaManager.createMediaObjects( [ mediaData ] );

        this.sceneRaycaster.addTarget( newMedias[ 0 ] );

        const customObject3d = newMedias[ 0 ]
        const { object } = customObject3d;

        if ( mediaData.type === EObjectType.MODEL )
        {
            this.objectController.attachTransformControl( customObject3d );
            object.position.copy( initialBox.position );
            this.scene.remove( initialBox );
            initialBox.geometry.dispose();
        }
        else
        {
            this.objectController.attachNewObject( customObject3d );
        }

        this.objectController.selectedInfo = {
            destroy: customObject3d.destroy.bind( customObject3d ),
            target: undefined,
            position: undefined,
            rotation: undefined,
        };
        if ( store.getState().iframeReducer === null ) return;
        const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
        send( SpaceSdkCmd.createobject, ( customObject3d as Media ).json );
    }

    updateFloatBtn()
    {
        const { controls, detachEvent, selectedInfo } = this.objectController;
        const { boundaryBox } = controls;

        if ( selectedInfo != undefined && this.uiFunctions.showFloatDeleteButton && boundaryBox != null )
        {
            const bbox = new THREE.Box3().setFromObject( boundaryBox );
            const position = new THREE.Vector3()
            boundaryBox.getWorldPosition( position )
            position.y = bbox.max.y;

            const coord = this.viewportControls.getSrceenCoordinate( position )
            if ( coord )
            {
                const { destroy } = selectedInfo;
                this.uiFunctions.showFloatDeleteButton( coord, () =>
                {
                    detachEvent()
                    if ( selectedInfo.target && selectedInfo.target.mediaClass != null )
                    {
                        selectedInfo.target.mediaClass.destroy()
                    } else
                    {
                        destroy();
                    }
                    this.uiFunctions.showFloatDeleteButton( undefined )
                } )
                return;
            }

            this.uiFunctions.showFloatDeleteButton( undefined )
        } else
        {
            this.uiFunctions.showFloatDeleteButton( undefined )
        }
    }
    animate()
    {
        this.stats?.begin();

        this.updateFloatBtn()
        this.sceneRaycaster.update( this.scene );
        this.viewportControls.render( this.scene );
        this.firstPersonControls.update();

        // if ( this.skybox )
        // {
        //     this.skybox.rotation.y += 0.0003
        //     this.skybox.rotation.x += 0.0003
        // }

        TWEEN.update();

        this.stats?.end();

        const sprites = this.mediaManager.mediaList.filter( ( media: Media ) => media.type === EObjectType.TAG );
        sprites.forEach( ( sprite: NewTag ) => { if ( sprite.loader != null ) sprite.loader.animate() } );

        // Request new frame.
        requestAnimationFrame( this.animate.bind( this ) );
    }

    clear()
    {

    }

    public enableViewerMode( findHotspot: boolean = true )
    {
        self.mediaManager.hideAllHotspots( true );
        self.animationMgr.enablePanoramaMesh = true;
        if ( findHotspot )
        {
            self.animationMgr.goHotspot( "", self.viewportControls.camera.position );
        }
        // self.animationMgr.latestHotspotId = '';
        self.firstPersonControls.movingMode = MovingMode.Viewer;

        if ( store.getState().iframeReducer === null ) return;
        const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
        send( SpaceSdkCmd.setmovemode, false );
    }

    public enableFreeMode()
    {
        self.mediaManager.hideAllHotspots( false );
        self.animationMgr.enablePanoramaMesh = false;
        self.firstPersonControls.movingMode = MovingMode.Freeform;
        self.firstPersonControls.enabled = true;
        self.objectController.enabled = true;

        if ( store.getState().iframeReducer === null ) return;
        const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
        send( SpaceSdkCmd.setmovemode, true );
    }

    previewModeControl( event: KeyboardEvent )
    {
        if ( event.code === 'KeyP' )
        {
            if ( self.viewportControls.isTop )
            {
                return;
            }
            if ( self.firstPersonControls.movingMode === MovingMode.Freeform )
            {
                self.enableViewerMode()
            } else
            {
                self.enableFreeMode()
            }

            self.sceneRaycaster.mouseUp = self.objectController.raycastMouseUp.bind( self.objectController );
            self.sceneRaycaster.mouseMove = self.objectController.raycastMouseMove.bind( self.objectController );
        }
    }

    addKeyBoardEvent()
    {
        window.addEventListener( "keydown", this.previewModeControl );
    }
}