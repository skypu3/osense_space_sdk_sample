import React from 'react';
import PropertyMenu from 'app/components/PropertyMenu';
import TopMenu from 'app/components/TopMenu';
import SideMenu from 'app/components/SideMenu';
import ThreeCanvas from 'app/components/ThreeCanvas';
import ThreeContext from 'app/components/three-context';
import { ThreeContextType } from 'app/components/three-context';

interface TagEditorProps
{
    buildingId: string
}

function TagEditor( props: TagEditorProps )
{
    const { buildingId } = props;
    const [ property, setProperty ] = React.useState( {
        value: {},
        update: ( obj: object ) => { }
    } );
    const [ actions, setActions ] = React.useState( undefined );
    const [ modalBoxDisplay, setModalDisplay ] = React.useState( "none" );
    const [ url, setUrl ] = React.useState( "" );
    const defaultValue: ThreeContextType = { modalBoxDisplay, setModalDisplay, url, setUrl, actions, setActions, property, setProperty }

    return (

        <div className="TagEditor">
            <ThreeContext.Provider value={ defaultValue }>
                { process.env.REACT_APP_STAGE !== 'sdk' && <TopMenu /> }
                { process.env.REACT_APP_STAGE !== 'sdk' && <PropertyMenu /> }
                { process.env.REACT_APP_STAGE !== 'sdk' && <SideMenu /> }
                {/* <Mo,dalBox /> */ }
                <ThreeCanvas buildingId={ buildingId }></ThreeCanvas>
            </ThreeContext.Provider>
        </div>

    );
}



export default TagEditor;
