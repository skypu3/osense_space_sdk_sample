import { gql } from "@apollo/client";
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import mediaList from "./designMedia.json";

/**
 * Get current user owned meidas, null for all type.
 * @param type NULL | 
 * IMAGE |
 * PIN |
 * GIF |
 * MUSIC |
 * VIDEO |
 * MODEL |
 * TRUEVIEW
 */
export const getMediaList = ( type?: EObjectType ) =>
{
    // const mediaTypequery = type ? `( type: ${ type })` : '';
    // return gql`query {
    //     currentMedialist${ mediaTypequery } {  	
    //         id
    //         name
    //         ...on MusicMedia {
    //             fileUrl
    //         }            
    //     }
    // }`;

    return type ? mediaList.filter( e =>
    {
        // @ts-ignore
        if ( type === "POSITION" )
        {
            return e.type.includes( "POSITION" )
        }
        else
        {
            return e.type === type
        }
    } ) : mediaList;
}

