import * as React from 'react';
import './DeleteButton.css';

interface IDeleteBtn
{
    left: number;
    top: number;
    callback(): void;
}

const DeleteButton: React.FunctionComponent<IDeleteBtn> = ( props ) =>
{
    return (
        <div className="deleteButton" >
            {
                props && props.left && props.top && props.callback &&
                <button style={ {
                    left: `${ props.left.toString() }%`,
                    top: `${ props.top.toString() }%`,
                } }
                    onClick={ props.callback }>
                    <img src={ process.env.REACT_APP_STAGE === 'sdk' ? "/spaceeditor/images/icon/delete.svg" : "/images/icon/delete.svg" } alt="delete" />
                </button>
            }

        </div >
    )
}

export default DeleteButton;
export type { IDeleteBtn };