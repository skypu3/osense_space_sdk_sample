import * as React from 'react';
import './PropertyMenu.css';
import './PropertyMenus/PropertyMenuGlobalStyle.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import ThreeContext from "./three-context";
import ImagePropertyMenu from './PropertyMenus/ImagePropertyMenu';
import TextPropertyMenu from './PropertyMenus/TextPropertyMenu';
import VideoPropertyMenu from './PropertyMenus/VideoPropertyMenu';
import WebPropertyMenu from './PropertyMenus/WebPropertyMenu';
import TagPropertyMenu from './PropertyMenus/TagPropertyMenu';
import ModelPropertyMenu from './PropertyMenus/ModelPropertyMenu';
import PortalPropertyMenu from './PropertyMenus/PortalPropertyMenu';
import SpritePropertyMenu from './PropertyMenus/SpritePropertyMenu';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button/Button';

export interface IPropertyMenuProps
{
}

export enum MediaType
{
    TEXT = "TEXT",
    IMAGE = "PHOTO",
    GIF = "GIF",
    VIDEO = "VIDEO",
    MODEL = "GLB",
    TAG = "TAG",
    WEB = "WEB",
    SPRITE = "SPRITE",
    PORTAL = "PORTAL",
    POSITIONPLANE = "POSITIONPLANE",
    POSITIONCUBE = "POSITIONCUBE"
}

const PropertyMenu: React.FunctionComponent<IPropertyMenuProps> = ( props ) =>
{
    const { property } = React.useContext( ThreeContext );
    const [ open, setOpen ] = React.useState( true );
    const { actions } = React.useContext( ThreeContext );

    function onCloseEvent()
    {
        setOpen( ( prev ) => !prev );
    }

    const isPosition = property.value[ 'type' ] === MediaType.POSITIONCUBE || property.value[ 'type' ] === MediaType.POSITIONPLANE;
    let type = property.value[ 'type' ];
    if ( isPosition && property.value[ 'swappedType' ] )
    {
        console.log( "swappedType", property.value[ 'swappedType' ] );
        console.log( property.value );

        type = property.value[ 'swappedType' ];
    }

    return (
        <div className="propertyMenu">
            <div className="titleBar">
                <button onClick={ onCloseEvent }>
                    <FontAwesomeIcon className={ open ? "fa-icon-open" : "fa-icon-close" } icon={ faAngleRight } size="2x" />
                </button>
                { isPosition && <Button onClick={ actions.swapObject }>
                    <img src="/images/icon/swap.svg" alt="swap" />
                </Button> }
            </div>

            <Slide direction="left" in={ open } mountOnEnter unmountOnExit>
                <div className="MenuContainer">
                    { renderMenu( type ) }
                </div>
            </Slide>
        </div>
    );

    function renderMenu( mediatype: MediaType )
    {
        switch ( mediatype )
        {
            case MediaType.IMAGE:
            case MediaType.GIF:
                return <ImagePropertyMenu type={ mediatype } />
            case MediaType.TEXT:
                return <TextPropertyMenu type={ mediatype } />
            case MediaType.VIDEO:
                return <VideoPropertyMenu type={ mediatype } />
            case MediaType.WEB:
                return <WebPropertyMenu type={ mediatype } />
            case MediaType.TAG:
                return <TagPropertyMenu type={ mediatype } />
            case MediaType.SPRITE:
                return <SpritePropertyMenu type={ mediatype } />
            case MediaType.MODEL:
                return <ModelPropertyMenu type={ mediatype } />
            case MediaType.PORTAL:
                return <PortalPropertyMenu type={ mediatype } />
            default:
                return null;
        }
    }
}
export default PropertyMenu;





