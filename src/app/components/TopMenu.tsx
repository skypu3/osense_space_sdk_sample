import * as React from 'react';
import './TopMenu.css';
import ThreeContext from "./three-context";

export interface ITopMenuProps
{
}

export default function TopMenu( props: ITopMenuProps )
{
    const { actions } = React.useContext( ThreeContext );

    return (
        <div className="top-menu">
            {/* <div className="icon">
                <img src="/images/icon/invalid-name.svg" alt="icon" />
                <span>Space Editor</span>
            </div> */}
            <div className="control-buttons">
                <button onClick={ actions ? actions.save : () => { } }>
                    <img src="/images/icon/save.svg" alt="save" />
                </button>
            </div>
        </div>
    );
}




