import * as React from 'react';
import { useTranslation } from 'react-i18next';
import './TextPropertyMenu.css';
import TextPropertyBox from './PropertyBox/TextPropertyBox';
import StylePropertyBox from './PropertyBox/StylePropertyBox';
import RotatePropertyBox from './PropertyBox/RotatePropertyBox';
import LinkPropertyBox from './PropertyBox/LinkPropertyBox';
import AttributePropertyBox from './PropertyBox/AttributePropertyBox';
import { MediaType } from '../PropertyMenu';

export interface ITextMenuProps
{
    type: MediaType,
}

const TextPropertyMenu: React.FunctionComponent<ITextMenuProps> = ( props ) =>
{
    const type = props.type;
    const { t } = useTranslation();

    return (
        <div className="TextPropertyMenu">
            {t( `media.${ type }` ) }
            <TextPropertyBox />
            <StylePropertyBox />
            <RotatePropertyBox />
            <LinkPropertyBox />
            <AttributePropertyBox type={ type } />
        </div>
    );
}

export default TextPropertyMenu;

