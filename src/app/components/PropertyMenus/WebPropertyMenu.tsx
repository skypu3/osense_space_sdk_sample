import * as React from 'react';
import { useTranslation } from 'react-i18next';
import './WebPropertyMenu.css';
import { MediaType } from '../PropertyMenu';
import LinkPropertyBox from './PropertyBox/LinkPropertyBox';
import AttributePropertyBox from './PropertyBox/AttributePropertyBox';

export interface IWebMenuProps
{
    type: MediaType,
}
const WebPropertyMenu: React.FunctionComponent<IWebMenuProps> = ( props ) =>
{
    const type = props.type;
    const { t } = useTranslation();

    return (
        <div className="WebPropertyMenu">
            {t( `media.${ type }` ) }
            <LinkPropertyBox />
            <AttributePropertyBox type={ type } />
        </div>
    );
}

export default WebPropertyMenu;

