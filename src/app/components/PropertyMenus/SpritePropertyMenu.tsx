import * as React from 'react';
import './ImagePropertyMenu.css';
import { useTranslation } from 'react-i18next';
import TitlePropertyBox from './PropertyBox/TitlePropertyBox';
import DetailPropertyBox from './PropertyBox/DetailPropertyBox';
import LinkPropertyBox from './PropertyBox/LinkPropertyBox';
import AttributePropertyBox from './PropertyBox/AttributePropertyBox';
import HotspotPropertyBox from './PropertyBox/HotspotPropertyBox';
import { MediaType } from '../PropertyMenu';

export interface ISpriteMenuProps
{
    type: MediaType,
}

const SpritePropertyMenu: React.FunctionComponent<ISpriteMenuProps> = ( props ) =>
{
    const type = props.type;
    const { t } = useTranslation();

    return (
        <div className="SpritePropertyMenu">
            { t( `media.${ type }` ) }
            <TitlePropertyBox />
            <DetailPropertyBox />
            <LinkPropertyBox />
            <AttributePropertyBox type={ type } />
        </div>
    );
}

export default SpritePropertyMenu;

