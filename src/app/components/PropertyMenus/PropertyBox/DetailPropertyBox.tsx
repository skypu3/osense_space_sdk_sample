import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import './DetailPropertyBox.css';
import ThreeContext from "../../three-context";
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

enum UIComponetKey
{
    value = "value"
}

enum PropertyKey
{
    detail = "detail"
}

interface IPropertyData
{
    detail: string,
}

const DetailPropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const maxDetailLength: number = 1000;
    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value );

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue;
            }
            else
            {
                propertyData[ propertykey ] = event.target[ key ];
            }
            updatePropertyData();
        }
    }

    const updatePropertyData = () =>
    {
        setProperty( {
            value: propertyData,
            update: property.update
        } )
        property.update( propertyData );
    }

    return (
        <div className="DetailPropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.Detail` ) + ":" }
            </div>
            <TextField
                id="detail"
                multiline rows={ 5 }
                onChange={ handleChange( PropertyKey.detail, UIComponetKey.value ) }
                value={ propertyData.detail ?? "" }
                placeholder={ t( `propertyText.PleaseInputTheDetail` ) }
                inputProps={ { maxLength: maxDetailLength } }
                variant="outlined"
                InputProps={ {
                    endAdornment: <InputAdornment className="endAdornment" position="end">{ ( propertyData.detail?.length ?? "0" ) + "/" + maxDetailLength }</InputAdornment>
                } } />
        </div>
    );
}

export default DetailPropertyBox;