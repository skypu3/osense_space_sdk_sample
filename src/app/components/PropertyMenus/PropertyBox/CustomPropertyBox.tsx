import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import ThreeContext from "../../three-context";
import './CustomPropertyBox.css';
import TextField from '@material-ui/core/TextField';

enum UIComponetKey
{
    value = "value"
}
enum PropertyKey
{
    customData = "customData"
}
interface IPropertyData
{
    customData: string,
}

const CustomPropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();
    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value )

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue;
            }
            else
            {
                propertyData[ propertykey ] = event.target[ key ];
            }
            updatePropertyData();

        }
    }

    const updatePropertyData = () =>
    {
        setProperty( {
            value: propertyData,
            update: property.update
        } )
        property.update( propertyData );
    }

    return (
        <div className="CustomPropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.CustomStyle` ) + ":" }
            </div>
            <TextField
                id="customData"
                value={ propertyData.customData }
                onChange={ handleChange( PropertyKey.customData, UIComponetKey.value ) }
                placeholder={ t( `propertyText.CustomStyleData` ) }
                variant="outlined"
                size="small" />
        </div>
    );
}

export default CustomPropertyBox;