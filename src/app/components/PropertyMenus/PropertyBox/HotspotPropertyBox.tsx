import * as React from 'react';
import './HotspotPropertyBox.css';
import { useTranslation } from 'react-i18next';
import { useContext } from 'react'
import ThreeContext from "../../three-context";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Switch from '@material-ui/core/Switch';
import Slider from "@material-ui/core/Slider";
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import Avatar from '@material-ui/core/Avatar';
// import { HotspotIconPath } from 'core/three/object/media/image/Image';

enum UIComponetKey
{
    value = "value",
    checked = "checked",
}
enum PropertyKey
{
    showHotspotTag = "showHotspotTag",
    hotspotIconIndex = "hotspotIconIndex",
    length = "length",
    HotspotTagWidth = "HotspotTagWidth",
    showTitle = "showTitle",
    showLine = "showLine",
}

interface IPropertyData
{
    showHotspotTag: boolean,
    hotspotIconIndex: number,
    length: number,
    HotspotTagWidth: number,
    showTitle: boolean,
    showLine: boolean,
}

const HotspotPropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value );

    const handleValueChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue / 100;
            }
            else 
            {
                propertyData[ propertykey ] = event.target[ key ] / 100;
            }
            updatePropertyData();
        }
    }

    const handleCheckBoxChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue;
            }
            else 
            {
                propertyData[ propertykey ] = event.target[ key ];
            }
            updatePropertyData();
        }
    }

    const handleIconIndexChange = ( event ) => 
    {
        propertyData[ PropertyKey.hotspotIconIndex ] = event.target.value;
        updatePropertyData();
    }

    const updatePropertyData = () =>
    {
        setProperty( {
            value: propertyData,
            update: property.update
        } )
        property.update( propertyData );
    }

    return (
        <div className="HotspotPropertyBox">
            <div className="BoxTitle">
                <div className="TitleText">
                    { t( `propertyText.HotspotTag` ) + ":" }
                </div>
                <Switch
                    checked={ propertyData.showHotspotTag ?? false }
                    onChange={ handleCheckBoxChange( PropertyKey.showHotspotTag, UIComponetKey.checked ) }
                    color='primary'
                    name="showHotspotTag"
                    inputProps={ { 'aria-label': 'showHotspotTag checkbox' } }
                />
            </div>
            <Grid container className={ `showTagContainer ${ propertyData.showHotspotTag ? "" : "hide" }` }>
                <Grid container direction="row" alignItems="center" spacing={ 1 }>
                    <Grid id="iconGrid" item xs={ 5 }  >
                        <FormControl fullWidth size="small">
                            <Select
                                id="icon-selector"
                                value={ propertyData.hotspotIconIndex ?? 0 }
                                defaultValue={ 0 }
                                onChange={ handleIconIndexChange }
                                variant="outlined"
                                name="icon"
                            >
                                {/* { HotspotIconPath.map( ( itemValue, index ) =>
                                    <MenuItem key={ index } value={ index } >
                                        <Avatar alt="hotspot icon" src={ itemValue } />
                                    </MenuItem> ) } */}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid id="hotspotTagWidthGrid" item xs={ 7 } >
                        <TextField
                            id="HotspotTagWidth"
                            value={ ( propertyData.HotspotTagWidth ?? 0.1 ) * 100 }
                            onChange={ handleValueChange( PropertyKey.HotspotTagWidth, UIComponetKey.value ) }
                            type="number"
                            inputProps={ {
                                style: { textAlign: 'center' }
                            } }
                            InputProps={ {
                                startAdornment: <InputAdornment position="start">W</InputAdornment>,
                                endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                            } }
                            variant="outlined"
                            size="small" />
                    </Grid>
                </Grid>
                <Grid id="lengthGrid" item xs={ 12 } >
                    <div className="TitleText" style={ {
                        marginTop: 3
                    } }>
                        { t( `propertyText.Length` ) }
                    </div>
                    <div>
                        <Slider
                            min={ 0 }
                            max={ 150 }
                            value={ ( propertyData.length ?? 0.5 ) * 100 }
                            onChange={ handleValueChange( PropertyKey.length, UIComponetKey.value ) }
                            valueLabelDisplay="auto"
                            valueLabelFormat={ value => `${ value }cm` }
                        />
                    </div>
                </Grid>
                <Grid id="showTitleGrid" item xs={ 12 }>
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="showTitle"
                                checked={ propertyData.showTitle ?? false }
                                onChange={ handleCheckBoxChange( PropertyKey.showTitle, UIComponetKey.checked ) }
                                color="primary"
                                size="small" /> }
                        label={ t( `propertyText.ShowTitle` ) }
                    />
                </Grid>
                <Grid id="showLineGrid" item xs={ 12 }>
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="showLine"
                                checked={ propertyData.showLine ?? true }
                                onChange={ handleCheckBoxChange( PropertyKey.showLine, UIComponetKey.checked ) }
                                color="primary"
                                size="small" /> }
                        label={ t( `propertyText.ConnectedLine` ) } />
                </Grid>
            </Grid>
        </div >
    );


}

export default HotspotPropertyBox;