import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import './TextPropertyBox.css';
import ThreeContext from "../../three-context";
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

enum UIComponetKey
{
    value = "value",
}

enum PropertyKey
{
    label = "label",
}

interface IPropertyData
{
    label: string,
}
const TextPropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const maxLength: number = 1000;
    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value )

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData.extData[ propertykey ] = newValue;
            }
            else
            {
                propertyData.extData[ propertykey ] = event.target[ key ];
            }
            setProperty( {
                value: propertyData,
                update: property.update
            } )
            property.update( propertyData );
        }
    }

    return (
        <div className="TextPropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.Content` ) + ":" }
            </div>
            <TextField
                id="textContent"
                value={ propertyData.extData?.label ?? "" }
                onChange={ handleChange( PropertyKey.label, UIComponetKey.value ) }
                multiline rows={ 5 }
                inputProps={ { maxLength: maxLength } }
                variant="outlined"
                InputProps={ {
                    endAdornment: <InputAdornment
                        className="endAdornment"
                        position="end">
                        { ( propertyData.extData?.label?.length ?? "0" ) + "/" + maxLength }
                    </InputAdornment>
                } } />
        </div>
    );
}

export default TextPropertyBox;