import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import './TitlePropertyBox.css';
import ThreeContext from "../../three-context";
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

enum UIComponetKey
{
    value = "value",
}

enum PropertyKey
{
    name = "name",
}

interface IPropertyData
{
    name: string,
}

const TitlePropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value );
    const maxNameLength: number = 100;

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue;
            }
            else
            {
                propertyData[ propertykey ] = event.target[ key ];
            }
            setProperty( {
                value: propertyData,
                update: property.update
            } )
            property.update( propertyData );
        }
    }

    return (
        <div className="TitlePropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.Title` ) + "* :" }
            </div>
            <TextField
                id="title"
                placeholder={ t( `propertyText.Title` ) }
                onChange={ handleChange( PropertyKey.name, UIComponetKey.value ) }
                inputProps={ { maxLength: maxNameLength } }
                variant="outlined"
                size="small"
                value={ propertyData.name ?? "" }
                InputProps={ {
                    endAdornment: <InputAdornment position="end">{ ( propertyData.name?.length ?? "0" ) + "/" + maxNameLength }</InputAdornment>
                } } />
        </div>
    );
}

export default TitlePropertyBox;