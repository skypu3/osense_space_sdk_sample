import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import './LinkPropertyBox.css';
import ThreeContext from "../../three-context";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import InputAdornment from '@material-ui/core/InputAdornment';

enum UIComponetKey
{
    value = "value",
    checked = "checked",
}

enum PropertyKey
{
    openLink = "openLink",
    linkName = "linkName",
    openNewWindow = "openNewWindow",
}

interface IPropertyData
{
    openLink: string,
    linkName: string,
    openNewWindow: boolean,
}

const LinkPropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value )
    const maxLength: number = 20;

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event )
        {
            propertyData[ propertykey ] = event.target[ key ]
            updatePropertyData();
        }
    }

    const updatePropertyData = () =>
    {
        setProperty( {
            value: propertyData,
            update: property.update
        } )
        property.update( propertyData );
    }

    return (
        <div className="LinkPropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.Link` ) + ":" }
            </div>
            <TextField
                id="linkName"
                placeholder={ t( `propertyText.LinkName` ) }
                onChange={ handleChange( PropertyKey.linkName, UIComponetKey.value ) }
                inputProps={ { maxLength: maxLength } }
                value={ propertyData.linkName ?? "" }
                variant="outlined"
                size="small"
                margin="dense"
                InputProps={ {
                    endAdornment: <InputAdornment position="end">{ ( propertyData.linkName?.length ?? "0" ) + "/" + maxLength }</InputAdornment>
                } } />
            <TextField
                id="openLink"
                placeholder="http://"
                variant="outlined"
                size="small"
                onChange={ handleChange( PropertyKey.openLink, UIComponetKey.value ) }
                value={ propertyData.openLink ?? "" }
                margin="dense" />
            <FormControlLabel
                control={ <Checkbox name="openNewWindow"
                    checked={ propertyData.openNewWindow ?? false }
                    onChange={ handleChange( PropertyKey.openNewWindow, UIComponetKey.checked ) }
                    color="primary"
                    size="small" /> }
                label={ t( `propertyText.OpenNewWindow` ) } />
        </div>
    );


}

export default LinkPropertyBox;