import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import './IconTagPropertyBox.css';
import ThreeContext from "../../three-context";
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Slider from "@material-ui/core/Slider";
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';

enum UIComponetKey
{
    value = "value",
    checked = "checked",
}

enum PropertyKey
{
    showTitle = "showTitle",
    showLine = "showLine",
    iconWidth = "iconWidth",
    length = "length",
}

interface IPropertyData
{
    showTitle: boolean,
    showLine: boolean,
    iconWidth: number,
    length: number,
}

const IconTagPropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value );

    const handleValueChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                if ( propertykey == PropertyKey.length )
                    propertyData.extData[ propertykey ] = newValue / 100;
                else
                    propertyData[ propertykey ] = newValue / 100;
            }
            else
            {
                if ( propertykey == PropertyKey.length )
                    propertyData.extData[ propertykey ] = event.target[ key ] / 100;
                else
                    propertyData[ propertykey ] = event.target[ key ] / 100;
            }
            updatePropertyData();
        }
    }

    const handleCheckBoxChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue;
            }
            else
            {
                propertyData[ propertykey ] = event.target[ key ];
            }
            updatePropertyData();
        }
    }

    const updatePropertyData = () =>
    {
        setProperty( {
            value: propertyData,
            update: property.update
        } )
        property.update( propertyData );
    }

    return (
        <div className="IconTagPropertyBox">
            <div className="BoxTitle">
                <div className="TitleText">
                    { t( `propertyText.IconTag` ) + ":" }
                </div>
            </div>
            <Grid container>
                <Grid container direction="row" alignItems="center" spacing={ 1 }>
                    <Grid id="widthGrid" item xs={ 12 } >
                        <TextField
                            id="iconWidth"
                            value={ ( propertyData.iconWidth ?? 0.1 ) * 100 }
                            onChange={ handleValueChange( PropertyKey.iconWidth, UIComponetKey.value ) }
                            type="number"
                            inputProps={ {
                                style: { textAlign: 'center' }
                            } }
                            InputProps={ {
                                startAdornment: <InputAdornment position="start">W</InputAdornment>,
                                endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                            } }
                            variant="outlined"
                            size="small" />
                    </Grid>
                </Grid>
                <Grid id="lengthGrid" item xs={ 12 } >
                    <div style={ {
                        marginTop: 3
                    } }>
                        { t( `propertyText.Length` ) }
                    </div>
                    <div>
                        <Slider
                            min={ 0 }
                            max={ 150 }
                            value={ ( propertyData.extData?.length ?? 0.5 ) * 100 }
                            onChange={ handleValueChange( PropertyKey.length, UIComponetKey.value ) }
                            valueLabelDisplay="auto"
                            valueLabelFormat={ value => `${ value }cm` }
                        />
                    </div>
                </Grid>
                <Grid id="showTitleGrid" item xs={ 12 }>
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="showTitle"
                                checked={ propertyData.showTitle ?? true }
                                onChange={ handleCheckBoxChange( PropertyKey.showTitle, UIComponetKey.checked ) }
                                color="primary"
                                size="small" /> }
                        label={ t( `propertyText.ShowTitle` ) }
                    />
                </Grid>
                <Grid id="showLineGrid" item xs={ 12 }>
                    <FormControlLabel
                        control={
                            <Checkbox
                                name="showLine"
                                checked={ propertyData.showLine ?? true }
                                onChange={ handleCheckBoxChange( PropertyKey.showLine, UIComponetKey.checked ) }
                                color="primary"
                                size="small" /> }
                        label={ t( `propertyText.ConnectedLine` ) }
                    />
                </Grid>
            </Grid>
        </div >
    );
}

export default IconTagPropertyBox;