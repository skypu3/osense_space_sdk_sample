import * as React from 'react';
import './StylePropertyBox.css';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import ThreeContext from "../../three-context";
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import FormatBoldIcon from '@material-ui/icons/FormatBold';
import FormatItalicIcon from '@material-ui/icons/FormatItalic';
import FormatAlignLeftIcon from '@material-ui/icons/FormatAlignLeft';
import FormatAlignCenterIcon from '@material-ui/icons/FormatAlignCenter';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FormatAlignRightIcon from '@material-ui/icons/FormatAlignRight';
import Grid from '@material-ui/core/Grid';
import { ChromePicker } from 'react-color'
import Popper from '@material-ui/core/Popper';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';

enum UIComponetKey
{
    value = "value",
    checked = "checked",
}
enum PropertyKey
{
    fontStyle = "fontStyle",
    fontWeight = "fontWeight",
    fontColor = "fontColor",
    aligment = "aligment",
    fontSize = "fontSize",
    lineSpace = "lineSpace",
}
enum FontStyle
{
    Normal = "normal",
    Italic = "italic",
}
enum FontWeight
{
    Normal = "normal",
    Bold = "bold",
}
interface IPropertyData
{
    fontStyle: string,
    fontWeight: string,
    fontColor: string,
    aligment: string,
    fontSize: number,
    lineSpace: number,
}
const StylePropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const textSizeOptions: number[] = [ 14, , 16, 18, 20, 22, 24, 26, 28, 30, 40, 50, 60 ];
    const lineSpaceOptions: number[] = [ 0.5, 1.0, 1.5, 2.0, 2.5, 3, 3.5, 4.0 ];
    const elRef = React.useRef( null ); // use to set color picker anchor

    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value )
    const [ openColorPicker, setOpenColorPicker ] = React.useState( false );

    const handleStyleSelectChange = ( key: PropertyKey, UIkey: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ key ] = newValue;
            }
            else
            {
                propertyData[ key ] = event.target[ UIkey ];
            }
            updatePropertyData()
        }
    }

    const handleStyleInputChange = ( key: PropertyKey ) =>
    {
        return function ( event, newValue: string )
        {
            // Conver the input newValue to Number type
            propertyData[ key ] = Number( newValue )
            updatePropertyData()
        }
    }

    const handleFormatChange = ( key: PropertyKey ) =>
    {
        if ( key === PropertyKey.fontWeight )
        {
            if ( propertyData[ key ] == FontWeight.Bold )
                propertyData[ key ] = FontWeight.Normal;
            else
                propertyData[ key ] = FontWeight.Bold;
        }
        else if ( key === PropertyKey.fontStyle )
        {
            if ( propertyData[ key ] == FontStyle.Italic )
                propertyData[ key ] = FontStyle.Normal;
            else
                propertyData[ key ] = FontStyle.Italic;
        }
        updatePropertyData()
    };

    const handleColorChangeComplete = ( color ) =>
    {
        propertyData[ PropertyKey.fontColor ] = color.hex;
        updatePropertyData()
    };

    const updatePropertyData = () =>
    {
        setProperty( {
            value: propertyData,
            update: property.update
        } )
        property.update( propertyData );
    }

    return (
        <div className="StylePropertyBox" ref={ elRef }>
            <div className="TitleText">
                { t( `propertyText.Style` ) + ":" }
            </div>
            <Grid container spacing={ 1 }>
                <Grid id="formatGrid" item xs={ 12 }>
                    <ButtonGroup aria-label="outlined primary button group">
                        <Popper
                            open={ openColorPicker }
                            anchorEl={ elRef.current }
                            placement="left"
                        >
                            <ChromePicker
                                className="colorPicker"
                                disableAlpha={ true }
                                color={ propertyData.fontColor ?? "#000000" }
                                // onChange={ handleColorChange }
                                onChangeComplete={ handleColorChangeComplete }
                                width="250px" />
                        </Popper>
                        <Button className={ openColorPicker ? "selectedBtn" : "" }
                            onClick={ () => { setOpenColorPicker( prev => !prev ) } }>
                            <ExpandMoreIcon />
                        </Button>
                        <Button className={ propertyData.extData?.fontWeight === FontWeight.Bold ? "selectedBtn" : "" }
                            onClick={ () =>
                            {
                                handleFormatChange( PropertyKey.fontWeight );
                            } }>
                            <FormatBoldIcon />
                        </Button>
                        <Button className={ propertyData.fontStyle === FontStyle.Italic ? "selectedBtn" : "" }
                            onClick={ () =>
                            {
                                handleFormatChange( PropertyKey.fontStyle );
                            } }>
                            <FormatItalicIcon />
                        </Button>
                    </ButtonGroup>
                </Grid>

                <Grid id="alignmentGrid" item xs={ 12 }>
                    <ToggleButtonGroup
                        value={ propertyData.aligment ?? "left" }
                        exclusive
                        onChange={ handleStyleSelectChange( PropertyKey.aligment, UIComponetKey.value ) }
                        aria-label="text-alignment"
                        size='large'
                    >
                        <ToggleButton value="left" aria-label="left-aligned">
                            <FormatAlignLeftIcon />
                        </ToggleButton>
                        <ToggleButton value="center" aria-label="centered">
                            <FormatAlignCenterIcon />
                        </ToggleButton>
                        <ToggleButton value="right" aria-label="right-aligned">
                            <FormatAlignRightIcon />
                        </ToggleButton>
                    </ToggleButtonGroup>
                </Grid>

                <Grid id="textSizeGrid" item xs={ 6 }>
                    <div className="TitleText">
                        { t( `propertyText.Size` ) + ":" }
                    </div>
                    <Autocomplete
                        id="font-size-selector"
                        freeSolo
                        options={ textSizeOptions }
                        getOptionLabel={ ( option ) => option.toString() }
                        value={ propertyData.fontSize ?? 60 }
                        onChange={ handleStyleSelectChange( PropertyKey.fontSize, UIComponetKey.value ) }
                        onInputChange={ handleStyleInputChange( PropertyKey.fontSize ) }
                        renderInput={ ( params ) =>
                            <TextField { ...params }
                                id="font-size-inputText"
                                type="number"
                                variant="outlined"
                            />
                        }
                    />
                </Grid>

                <Grid id="lineSpaceGrid" item xs={ 6 }>
                    <div className="TitleText">
                        { t( `propertyText.LineSpace` ) + ":" }
                    </div>
                    <Autocomplete
                        id="line-space-selector"
                        freeSolo
                        options={ lineSpaceOptions }
                        value={ propertyData.lineSpace ?? 0.5 }
                        getOptionLabel={ ( option ) => option.toString() }
                        onChange={ handleStyleSelectChange( PropertyKey.lineSpace, UIComponetKey.value ) }
                        onInputChange={ handleStyleInputChange( PropertyKey.lineSpace ) }
                        renderInput={ ( params ) =>
                            <TextField { ...params }
                                id="line-space-inputText"
                                type="number"
                                variant="outlined"
                            />
                        }
                    />
                </Grid>
            </Grid>
        </div >
    );
}

export default StylePropertyBox;