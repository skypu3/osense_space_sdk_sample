import * as React from 'react';
import { useContext, useState, useEffect, useRef } from 'react'
import { useTranslation } from 'react-i18next';
import './RotatePropertyBox.css';
import ThreeContext from "../../three-context";
import Slider from "@material-ui/core/Slider";

enum UIComponetKey
{
    value = "value",
}

enum PropertyKey
{
    rotation = "rotation",
}

interface IPropertyData
{
    rotation: number,
}

const RotatePropertyBox: React.FunctionComponent = ( props ) =>
{
    const { t } = useTranslation();

    //Todo: get rotation
    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value );
    const [ data, setData ] = useState<IPropertyData>( {
        rotation: 30,
    } )

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                setData( { ...data, [ propertykey ]: newValue } );
            }
            else
            {
                setData( { ...data, [ propertykey ]: event.target[ key ] } );
            }
        }
    }

    const isInitialMount = useRef( true );
    useEffect( () =>
    {
        if ( isInitialMount.current )
        {
            init();
        }
        else
        {
            update();
        }

        function init()
        {
            isInitialMount.current = false;
            //Todo: init rotation
            setData( {
                rotation: propertyData.rotation?.y ?? 30,
            } )
        }

        function update()
        {
            //Todo: update rotation

            // propertyData.rotation. = data.rotation;
            // setProperty( {
            //     value: propertyData,
            //     update: property.update
            // } )
            // property.update( propertyData );
        }
    }, [ data ] )

    return (
        <div className="RotatePropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.Rotation` ) + ":" }
            </div>
            <Slider
                min={ 0 }
                max={ 360 }
                value={ data.rotation ?? 0 }
                valueLabelDisplay="auto"
                valueLabelFormat={ value => `${ value }\u{00B0}` }
                onChange={ handleChange( PropertyKey.rotation, UIComponetKey.value ) }
            />
        </div>
    );
}

export default RotatePropertyBox;