import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import './AttributePropertyBox.css';
import ThreeContext from "../../three-context";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { MediaType } from '../../PropertyMenu';

export interface IAttributeBoxProps
{
    type: MediaType;
}

enum UIComponetKey
{
    checked = "checked"
}

enum PropertyKey
{
    followCamera = "followCamera",
    unclickable = "unclickable",
    isVertical = "isVertical",
    openLinkDirectly = "openLinkDirectly",
    thisCameraCanSee = "thisCameraCanSee",
    autoplay = "autoplay",
}

interface IPropertyData
{
    followCamera: boolean,
    unclickable: boolean,
    isVertical: boolean,
    openLinkDirectly: boolean,
    thisCameraCanSee: boolean,
    autoplay: boolean,
}

const AttributePropertyBox: React.FunctionComponent<IAttributeBoxProps> = ( props: IAttributeBoxProps ) =>
{
    const { t } = useTranslation();
    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value )

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue;
            }
            else
            {
                propertyData[ propertykey ] = event.target[ key ];
            }
            updatePropertyData();
        }
    }

    const updatePropertyData = () =>
    {
        setProperty( {
            value: propertyData,
            update: property.update
        } )
        property.update( propertyData );
    }

    // all AttributeCompoenet
    const AttributeItemPair =
    {
        followCamera: {
            propertyKey: PropertyKey.followCamera,
            label: t( `propertyText.FolloCamera` )
        },
        unclickable: {
            propertyKey: PropertyKey.unclickable,
            label: t( `propertyText.Unclickable` )
        },
        isVertical: {
            propertyKey: PropertyKey.isVertical,
            label: t( `propertyText.Vertical` )
        },
        openLinkDirectly: {
            propertyKey: PropertyKey.openLinkDirectly,
            label: t( `propertyText.OpenLinkDirectly` )
        },
        thisCameraCanSee: {
            propertyKey: PropertyKey.thisCameraCanSee,
            label: t( `propertyText.ThisCameraCanSee` )
        },
        autoplay: {
            propertyKey: PropertyKey.autoplay,
            label: t( `propertyText.Autoplay` )
        },
    }

    // add / delete attribute in menu (in order)
    const AttributesMenu =
    {
        ImageMenu: [ AttributeItemPair.followCamera, AttributeItemPair.unclickable, AttributeItemPair.isVertical, AttributeItemPair.openLinkDirectly, AttributeItemPair.thisCameraCanSee ],
        TextMenu: [ AttributeItemPair.followCamera, AttributeItemPair.unclickable, AttributeItemPair.isVertical, AttributeItemPair.openLinkDirectly, AttributeItemPair.thisCameraCanSee ],
        VideoMenu: [ AttributeItemPair.followCamera, AttributeItemPair.unclickable, AttributeItemPair.isVertical, AttributeItemPair.autoplay, AttributeItemPair.thisCameraCanSee ],
        WebMenu: [ AttributeItemPair.followCamera, AttributeItemPair.isVertical, AttributeItemPair.thisCameraCanSee ],
        TagMenu: [ AttributeItemPair.openLinkDirectly, AttributeItemPair.thisCameraCanSee ],
        ModelMenu: [ AttributeItemPair.unclickable, AttributeItemPair.openLinkDirectly, AttributeItemPair.thisCameraCanSee ],
        PortalMenu: [ AttributeItemPair.thisCameraCanSee ],
    }

    function AttributeItem( props )
    {
        const { propertyKey, label } = props;
        return (
            <FormControlLabel control={ <Checkbox
                name={ propertyKey }
                checked={ propertyData[ propertyKey ] ?? false }
                onChange={ handleChange( propertyKey, UIComponetKey.checked ) }
                color="primary"
                size="small" /> }
                label={ label } />
        );
    }

    function AttributeMenu( props )
    {
        const menu = props.menu;
        return (
            menu.map( ( item, index ) =>
                < AttributeItem key={ index } propertyKey={ item.propertyKey } label={ item.label } />
            )
        )
    }

    function renderMenuAttribute( mediaType )
    {
        switch ( mediaType )
        {
            case MediaType.IMAGE:
            case MediaType.SPRITE:
            case MediaType.GIF:
                return <AttributeMenu menu={ AttributesMenu.ImageMenu } />
            case MediaType.TEXT:
                return <AttributeMenu menu={ AttributesMenu.TextMenu } />
            case MediaType.WEB:
                return <AttributeMenu menu={ AttributesMenu.WebMenu } />
            case MediaType.VIDEO:
                return <AttributeMenu menu={ AttributesMenu.VideoMenu } />
            case MediaType.TAG:
                return <AttributeMenu menu={ AttributesMenu.TagMenu } />
            case MediaType.MODEL:
                return <AttributeMenu menu={ AttributesMenu.ModelMenu } />
            case MediaType.PORTAL:
                return <AttributeMenu menu={ AttributesMenu.PortalMenu } />
            default:
                return null;
        }
    }

    return (
        <div className="AttributePropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.Attributes` ) + ":" }
            </div>
            { renderMenuAttribute( props.type ) }
        </div>
    );
}

export default AttributePropertyBox;