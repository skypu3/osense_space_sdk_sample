import * as React from 'react';
import { useContext } from 'react'
import { useTranslation } from 'react-i18next';
import TextField from '@material-ui/core/TextField';
import ThreeContext from "../../three-context";
import './SpacePropertyBox.css';

enum UIComponetKey
{
    value = "value",
}
enum PropertyKey
{
    openLink = "openLink",
}
interface IPropertyData
{
    openLink: string,
}
const SpacePropertyBox: React.FunctionComponent = () =>
{
    const { t } = useTranslation();

    const { property, setProperty } = useContext( ThreeContext );
    const propertyData = Object.assign( property.value )

    const handleChange = ( propertykey: PropertyKey, key: UIComponetKey ) =>
    {
        return function ( event, newValue?)
        {
            if ( newValue )
            {
                propertyData[ propertykey ] = newValue;
            }
            else
            {
                propertyData[ propertykey ] = event.target[ key ];
            }
            setProperty( {
                value: propertyData,
                update: property.update
            } )
            property.update( propertyData );
        }
    }

    return (
        <div className="SpacePropertyBox">
            <div className="BoxTitle">
                { t( `propertyText.SpaceID` ) + ":" }
            </div>
            <TextField
                id="spaceId"
                placeholder={ t( `propertyText.SpaceID` ) }
                variant="outlined"
                value={ propertyData.openLink ?? "穿越空間" }
                onChange={ handleChange( PropertyKey.openLink, UIComponetKey.value ) }
                size="small" />
        </div>
    );
}

export default SpacePropertyBox;