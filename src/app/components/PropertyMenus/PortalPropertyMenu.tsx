import * as React from 'react';
import './PortalPropertyMenu.css';
import { useTranslation } from 'react-i18next';
import { MediaType } from '../PropertyMenu';
import AttributePropertyBox from './PropertyBox/AttributePropertyBox';
import TitlePropertyBox from './PropertyBox/TitlePropertyBox';
import SpacePropertyBox from './PropertyBox/SpacePropertyBox';

export interface IPortalMenuProps
{
    type: MediaType,
}

const PortalPropertyMenu: React.FunctionComponent<IPortalMenuProps> = ( props ) =>
{
    const type = props.type;
    const { t } = useTranslation();

    return (
        <div className="PortalPropertyMenu">
            {t( `media.${ type }` ) }
            <TitlePropertyBox />
            <SpacePropertyBox />
            <AttributePropertyBox type={ type } />
        </div>
    );
}

export default PortalPropertyMenu;

