import * as React from 'react';
import './ModelPropertyMenu.css';
import { useTranslation } from 'react-i18next';
import { MediaType } from '../PropertyMenu';
import LinkPropertyBox from './PropertyBox/LinkPropertyBox';
import AttributePropertyBox from './PropertyBox/AttributePropertyBox';
import TitlePropertyBox from './PropertyBox/TitlePropertyBox';
import DetailPropertyBox from './PropertyBox/DetailPropertyBox';

export interface IModelMenuProps
{
    type: MediaType,
}

const ModelPropertyMenu: React.FunctionComponent<IModelMenuProps> = ( props ) =>
{
    const type = props.type;
    const { t } = useTranslation();
    return (
        <div className="ModelPropertyMenu">
            {t( `media.${ type }` ) }
            <TitlePropertyBox />
            <DetailPropertyBox />
            <LinkPropertyBox />
            <AttributePropertyBox type={ type } />
        </div>
    );
}

export default ModelPropertyMenu;

