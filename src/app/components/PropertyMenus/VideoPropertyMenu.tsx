import * as React from 'react';
import { useTranslation } from 'react-i18next';
import './VideoPropertyMenu.css';
import { MediaType } from '../PropertyMenu';
import TitlePropertyBox from './PropertyBox/TitlePropertyBox';
import AttributePropertyBox from './PropertyBox/AttributePropertyBox';

export interface IVideoMenuProps
{
    type: MediaType,
}

const VideoPropertyMenu: React.FunctionComponent<IVideoMenuProps> = ( props ) =>
{
    const type = props.type;
    const { t } = useTranslation();

    return (
        <div className="VideoPropertyMenu">
            {t( `media.${ type }` ) }
            <TitlePropertyBox />
            <AttributePropertyBox type={ type } />
        </div>
    );
}

export default VideoPropertyMenu;

