import * as React from 'react';
import './TagPropertyMenu.css';
import { useTranslation } from 'react-i18next';
import { MediaType } from '../PropertyMenu';
import LinkPropertyBox from './PropertyBox/LinkPropertyBox';
import AttributePropertyBox from './PropertyBox/AttributePropertyBox';
import TitlePropertyBox from './PropertyBox/TitlePropertyBox';
import DetailPropertyBox from './PropertyBox/DetailPropertyBox';
import IconTagPropertyBox from './PropertyBox/IconTagPropertyBox';
import CustomPropertyBox from './PropertyBox/CustomPropertyBox';

export interface ITagMenuProps
{
    type: MediaType,
}

const TagPropertyMenu: React.FunctionComponent<ITagMenuProps> = ( props ) =>
{
    const type = props.type;
    const { t } = useTranslation();

    return (
        <div className="TagPropertyMenu">
            {t( `media.${ type }` ) }
            <TitlePropertyBox />
            <DetailPropertyBox />
            <IconTagPropertyBox />
            <LinkPropertyBox />
            <CustomPropertyBox />
            <AttributePropertyBox type={ type } />
        </div>
    );
}

export default TagPropertyMenu;

