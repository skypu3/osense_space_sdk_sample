import { SpaceSdkCmd } from 'core/store/actions/iframe';
import { setScene } from 'core/store/actions/scene';
import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

export default function useIframe():
    {
        sendMessage: ( command: SpaceSdkCmd, data: any ) => void
    }
{
    const dispatch = useDispatch();
    const sendMessage = ( command: SpaceSdkCmd, data: any ): void =>
    {
        if ( process.env.REACT_APP_STAGE === 'sdk' )
        {
            if ( window && window.parent )
            {
                window.parent.postMessage( JSON.stringify( { command: SpaceSdkCmd[ command ], data } ), '*' );
            }
        }
    }

    useEffect( () =>
    {
        // if (process.env.REACT_APP_STAGE === 'sdk') {
        //     window.addEventListener("message", receiveHandler)
        // }
        // // clean up
        // return () => window.removeEventListener("message", receiveHandler)
    }, [] ) // empty array => run only once

    return { sendMessage }
};
