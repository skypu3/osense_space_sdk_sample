import { SpaceSdkCmd } from 'core/store/actions/iframe';
import { setScene } from 'core/store/actions/scene';
import AnimeViewportController from 'core/three/base/AnimeViewportController';
import Global from 'core/three/base/Global';
import MediaManager from 'core/three/base/MediaManager';
import ObjectController from 'core/three/base/ObjectController';
import AimationMgr from 'core/three/base/AnimationMgr';
import React from 'react';
import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import ThreeContext from '../three-context';
import { IRaycastResponse } from 'core/three/base/Raycaster';
import * as THREE from "three";
import store from 'core/store/store';
import ThreeView from 'app/three/ThreeView';
import { MovingMode } from 'core/three/base/FirstPersonControls';
import { Vector3 } from 'three';
import Media from 'core/three/mediaObject/media/Media';

declare global
{
    interface Window
    {
        getMediaList: Function;
        setScene: Function;
        createObject: Function;
        updateObject: Function;
        deleteObjects: Function;
        selectTag: Function;
        getPostProcessingConfig: Function;
        controls: { // 相機控制
            goTagLocation: Function;
            getCameraInformation: Function;
            goHotspotLocation: Function;
            goFloorLocation: Function;
            setDollHouseMode: Function;
            setMoveMode: Function
            getAllFloorNumbers: Function;
        }
    }
}

export default function useSdkFunctions()
{
    const dispatch = useDispatch();
    const { actions } = React.useContext( ThreeContext );
    let mediaManager: MediaManager;
    let animationMgr: AimationMgr;
    let viewportControls: AnimeViewportController;
    let objectController: ObjectController;

    useEffect( () =>
    {
        if ( process.env.REACT_APP_STAGE === 'sdk' && actions == undefined )
        {
            mediaManager = Global.inst.mediaManager;

            window.setScene = ( sceneData: { [ key: string ]: string } ) =>
            {
                if ( 'sceneId' in sceneData )
                {
                    dispatch( setScene( sceneData ) );
                }
            }

            window.getMediaList = () =>
            {
                return mediaManager.toJson();
            }
            // window.addEventListener( "message", receiveHandler )
        }
        else if ( process.env.REACT_APP_STAGE === 'sdk' )
        {
            mediaManager = Global.inst.mediaManager;
            viewportControls = Global.inst.viewportController;
            objectController = Global.inst.objectController;
            animationMgr = Global.inst.animationMgr;

            window.createObject = ( mediaData: { [ key: string ]: any } ) =>
            {
                if ( 'type' in mediaData )
                {
                    actions.createObject( mediaData );
                }
            }

            window.updateObject = ( mediaData: { [ key: string ]: any } ) =>
            {
                if ( 'type' in mediaData )
                {
                    const medias = mediaManager.mediaList.filter( ( media: Media ) => media.id === mediaData[ 'id' ] );
                    if ( medias.length > 0 )
                    {
                        const media: Media = medias[ 0 ];
                        media.setJson( mediaData );
                    }
                }
            }

            window.deleteObjects = ( mediaList: [ key: string ] ) =>
            {
                const medias = mediaManager.mediaList.filter( ( media: Media ) => mediaList.includes( media.id ) );
                medias.forEach( ( media: Media ) =>
                {
                    media.destroy()
                } )
            }

            window.selectTag = async ( tagData: [ string ] ) =>
            {
                try
                {
                    const medias = mediaManager.mediaList.filter( ( media: Media ) => media.id === tagData[ 0 ] );
                    if ( medias.length > 0 )
                    {
                        const media: Media = medias[ 0 ];
                        await animationMgr.faceObjectRotation( media.object.position );
                        const info: IRaycastResponse = {
                            target: media.targetInfo,
                            position: new THREE.Vector3(),
                            rotation: new THREE.Matrix4(),
                            destroy: media.destroy
                        }
                        objectController.raycastMouseUp( info );
                    }
                } catch ( err )
                {
                    console.log( err );
                }
            }

            window.controls = {
                goTagLocation: async ( tagId: String ) =>
                {
                    try
                    {
                        const medias = mediaManager.mediaList.filter( ( media: Media ) => media.id === tagId );
                        if ( medias.length > 0 )
                        {
                            const media: Media = medias[ 0 ];
                            await animationMgr.goNearHotspotWithAngle( media );
                        }
                    } catch ( err )
                    {
                        console.log( err );
                    }
                },
                getCameraInformation: () =>
                {
                    const hotspotId = animationMgr.latestHotspotId
                    if ( hotspotId )
                    {
                        return {
                            floor: animationMgr.currentFloor,
                            position: {
                                x: animationMgr.hotspotdict[ hotspotId ].position.x,
                                y: animationMgr.hotspotdict[ hotspotId ].position.y,
                                z: animationMgr.hotspotdict[ hotspotId ].position.z
                            },
                            startHotspotId: animationMgr.latestHotspotId,
                            hotspotId: animationMgr.latestHotspotId,
                            azimuthAngle: viewportControls.azimuthAngle,
                            polarAngle: viewportControls.polarAngle
                        }
                    }
                },
                goHotspotLocation: async ( hotspotData: { [ key: string ]: any } ) =>
                {
                    try
                    {
                        if ( 'hotspotId' in hotspotData )
                        {
                            await animationMgr.goHotspot( hotspotData.hotspotId, undefined, hotspotData.polarAngle, hotspotData.azimuthAngle )
                        }
                    } catch ( err )
                    {
                        console.log( err );
                    }
                },
                goFloorLocation: async ( floor ) =>
                {
                    try
                    {
                        await animationMgr.goFloorWithNumber( floor )
                    } catch ( err )
                    {
                        console.log( err );
                    }
                },
                setDollHouseMode: async () =>
                {
                    if ( viewportControls.isTop )
                    {
                        await animationMgr.godown();
                    } else
                    {
                        await animationMgr.goTop()
                    }
                },
                setMoveMode: () =>
                {
                    const three = store.getState().threeViewReducer as ThreeView;
                    if ( three.firstPersonControls.movingMode == MovingMode.Freeform )
                    {
                        three.enableViewerMode()
                    } else
                    {
                        three.enableFreeMode();
                    }
                },
                getAllFloorNumbers: () =>
                {
                    try
                    {
                        let floors = []
                        let hotspotKeys = Object.keys( animationMgr.hotspotdict );
                        hotspotKeys.forEach( key =>
                        {
                            const info = animationMgr.hotspotdict[ key ];
                            if ( !floors.includes( info.floor ) )
                            {
                                floors.push( info.floor )
                            }
                        } )
                        return floors
                    } catch ( err )
                    {
                        console.log( err );
                    }
                }
            }

            window.getPostProcessingConfig = () =>
            {
                const postProcessingController = mediaManager.viewportControls.postProcessingController
                return postProcessingController.getOptions()
            }
        }
        // clean up
        return () => { }
    }, [ actions ] ) // empty array => run only once
};
