import * as React from 'react';
import "./SideMenu.css";
import TitlebarGridList from './SideMenu/TitlebarGridList';
import ThreeViewSwitchButtons from './SideMenu/ThreeViewSwitchButtons';

interface ISideMenuProps
{
}

const SideMenu: React.FunctionComponent<ISideMenuProps> = ( props ) =>
{
    const [ media, setMedia ] = React.useState( null );
    const [ open, setOpen ] = React.useState( false );

    return <div className="sideMenu">
        <div className="sideMain">
            <button onClick={ () => { setMedia( "PHOTO" ); setOpen( true ); } }>
                {/* eslint-disable-next-line*/ }
                <img style={ { fill: 'white' } } src="/images/icon/image.svg" alt="IMAGE-ICON" />
            </button>

            <button onClick={ () => { setMedia( "TAG" ); setOpen( true ); } }>
                <img src="/images/icon/tag.svg" alt="TAG-ICON" />
            </button>

            <button onClick={ () => { setMedia( "GIF" ); setOpen( true ); } }>
                <img src="/images/icon/gif.svg" alt="GIF-ICON" />
            </button>

            <button onClick={ () => { setMedia( "VIDEO" ); setOpen( true ); } }>
                <img src="/images/icon/video.svg" alt="VIDEO-ICON" />
            </button>

            <span className="splitLine"></span>

            <button onClick={ () => { setMedia( "TEXT" ); setOpen( true ); } }>
                <img src="/images/icon/text.svg" alt="TEXT-ICON" />
            </button>

            <button onClick={ () => { setMedia( "WEB" ); setOpen( true ); } }>
                <img src="/images/icon/web.svg" alt="WEB-ICON" />
            </button>

            <button onClick={ () => { setMedia( "GLB" ); setOpen( true ); } }>
                <img src="/images/icon/model.svg" alt="MODEL-ICON" />
            </button>

            <button onClick={ () => { setMedia( "SPRITE" ); setOpen( true ); } }>
                <img src="/images/icon/sprite.svg" alt="MODEL-ICON" />
            </button>

            <span className="splitLine"></span>

            <button onClick={ () => { setMedia( "SHARESCREEN" ); setOpen( true ); } }>
                <img src="/images/icon/web.svg" alt="WEB-ICON" />
            </button>

            <span className="splitLine"></span>

            <button onClick={ () => { setMedia( "PORTAL" ); setOpen( true ); } }>
                <img src="/images/icon/portal.svg" alt="PORTAL-ICON" />
            </button>

            <button onClick={ () => { setMedia( "POSITION" ); setOpen( true ); } }>
                <img src="/images/icon/positioning.svg" alt="PORTAL-ICON" />
            </button>

            <span className="splitLine"></span>
        </div>

        {
            open && <TitlebarGridList
                className="sideSub"
                type={ media }
                onCloseEvent={ () => { setOpen( false ); } }
            />
        }

        <ThreeViewSwitchButtons />

    </div>
};

export default SideMenu;


