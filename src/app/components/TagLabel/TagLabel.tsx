import React from 'react'
import cls from 'classnames'
import { makeStyles } from '@material-ui/core/styles';
import ThreeContext from '../three-context'
import ThreeView, { ThreeEvents } from 'app/three/ThreeView'

import { Format } from 'core/three/scene/Format';
import store from 'core/store/store';
import { EObjectType } from 'core/three/mediaObject/MediaObject3D';
import Media from 'core/three/mediaObject/media/Media';

const useStyles = makeStyles( {
    hovertip: {
        display: '-webkit-box',
        position: 'fixed',
        overflow: 'hidden',
        zIndex: 2,
        maxWidth: 140,
        textAlign: 'left',
        pointerEvents: 'none',
        textOverflow: 'ellipsis',
        '-webkit-line-clamp': 10,
        '-webkit-box-orient': 'vertical',
        color: 'rgba(0,0,0,0.7)',
        borderRadius: 10,
        fontSize: 14,
        backgroundColor: 'rgba(255,255,255,0.7)',
        padding: '6px 6px',
    }
} );

const TagLabel = () =>
{
    const styles = useStyles()
    const three = store.getState().threeViewReducer as ThreeView;
    const [ currentMarker, setCurrentMarker ] = React.useState<Format.IMarker>( null )
    const [ position, setPosition ] = React.useState( { x: 0, y: 0 } )
    const latestHoverRef = React.useRef( '' )
    const displayTypes: EObjectType[] = [ EObjectType.GIF, EObjectType.TAG, EObjectType.PHOTO, EObjectType.MODEL ]

    React.useEffect( () =>
    {
        if ( three == null ) return
        three.events.on( ThreeEvents.raycastMouseEnter, ( media: Media ) =>
        {
            const screenPosition = three.viewportControls.getSrceenCoordinate( media.object.position )

            if ( displayTypes.includes( media.type ) && screenPosition && !media.json.unclickable && media.showTitle )
            {
                setCurrentMarker( media.json as Format.IMarker )
                setPosition( { x: screenPosition.x * 100, y: screenPosition.y * 100 } )
                latestHoverRef.current = media.json.id
                document.body.style.cursor = 'pointer'
                three.mouseCursor.visible = false
            }
        } )
        three.events.on( ThreeEvents.raycastMouseLeave, ( media: Media ) =>
        {
            if ( latestHoverRef.current === media.json.id )
            {
                setCurrentMarker( null )
                document.body.style.cursor = 'auto'
            }
            three.mouseCursor.visible = true
        } )
    }, [ three ] )
    return currentMarker ? (
        <div className={ cls(
            "no-select",
            styles.hovertip,
            { display: currentMarker.name.length === 0 ? 'none' : 'block' }
        ) } style={ {
            left: position.x + '%',
            top: position.y + '%',
        } }>{ currentMarker.title }
        </div > ) : null
}

export default TagLabel