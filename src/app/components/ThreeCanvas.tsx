import * as React from 'react';
import { useEffect } from 'react';
import "./ThreeCanvas.css";
import ThreeView from '../three/ThreeView';
import ThreeContext from "./three-context";
import DeleteButton, { IDeleteBtn } from './DeleteButton';
import ProgressBar from './ProgressBar';
import { getSceneData } from 'core/api/Scene'
import { useDispatch, useSelector } from 'react-redux';
import { setMode, ViewerMode } from 'core/store/actions/mode';
import useIframe from './hooks/useIframe';
import useSdkFunctions from './hooks/useSdkFunctions';
import { storeTypes } from 'core/store/store';
import { SceneType, setScene } from 'core/store/actions/scene';
import { setIframeHandler, SpaceSdkCmd } from 'core/store/actions/iframe';
// import { fetchUser } from 'core/store/actions/users';
import { setThreeView } from 'core/store/actions/threeView';
import TagHoverLayer from './TagLabel/TagLabel';
const pkg = require( '../../../package.json' )

interface IThreeCanvasProps
{
    buildingId: string;
}

// export function ThreeCanvasRouter( props )
// {
//     const buildingId = props.match.params[ 0 ].replace( '/', '' )

//     return <ThreeCanvas buildingId={ buildingId }></ThreeCanvas>
// }

const ThreeCanvas: React.FunctionComponent<IThreeCanvasProps> = ( props ) =>
{
    const { setActions, setUrl, setProperty } = React.useContext( ThreeContext );

    const [ loadingprogress, setProgress ] = React.useState( 0 )
    const [ spaceThumbnail, setSpaceThumbnail ] = React.useState( "" )

    const [ deleteBtn, setDeleteBtn ] = React.useState( undefined as IDeleteBtn )
    const [ valid, setValid ] = React.useState( true )
    const [ threeObject, setThreeObject ] = React.useState( null )

    const sceneData = useSelector( ( state: storeTypes ) => state.sceneReducer ) as SceneType;
    const dispatch = useDispatch();

    const iframe = useIframe();
    useSdkFunctions();
    const uiFunctions = {
        showPropertyMenu: function ( obj, updatefunc )
        {
            setProperty( {
                value: obj,
                update: updatefunc
            } )
        },
        showFloatDeleteButton: function ( info, callback )
        {
            if ( info )
            {
                setDeleteBtn( {
                    left: info.x * 100,
                    top: info.y * 100,
                    callback: callback
                } )
            } else
            {
                setDeleteBtn( undefined )
            }

        }
    }

    useEffect( () =>
    {
        console.log( 'OSENSE Editor SDK VERSION: ' + pkg.version )
    }, [ pkg ] )

    // get scene id from store, and set into viewer sdk
    useEffect( () =>
    {
        if ( process.env.REACT_APP_STAGE === 'sdk' )
        {
            dispatch( setIframeHandler( iframe.sendMessage ) );
        }
    }, [] )
    useEffect( () =>
    {
        if ( process.env.REACT_APP_STAGE === 'sdk' && sceneData !== null && threeObject === null )
        {
            const three = new ThreeView( uiFunctions );
            setThreeObject( three )
            dispatch( setThreeView( three ) );
            dispatch( setMode( ViewerMode.Editor ) );
            getScene( three, sceneData.sceneId );
            return () =>
            {
                three.clear();
            }
        }
    }, [ sceneData ] )

    // 一般讀取模式
    useEffect( () =>
    {
        if ( process.env.REACT_APP_STAGE !== 'sdk' )
        {
            const uiFunctions = {
                showPropertyMenu: function ( obj, updatefunc )
                {
                    setProperty( {
                        value: obj,
                        update: updatefunc
                    } )
                },
                showFloatDeleteButton: function ( info, callback )
                {
                    if ( info )
                    {
                        setDeleteBtn( {
                            left: info.x * 100,
                            top: info.y * 100,
                            callback: callback
                        } )
                    } else
                    {
                        setDeleteBtn( undefined )
                    }

                }
            }
            dispatch( setMode( ViewerMode.Editor ) )
            const three = new ThreeView( uiFunctions );
            setThreeObject( three )
            dispatch( setThreeView( three ) );
            dispatch( setScene( { sceneId: props.buildingId } ) );
            getScene( three, props.buildingId );
            return () =>
            {
                // Init when component unmount
                three.clear();
            }
        }
    }, [] )


    const getScene = ( three: ThreeView, sceneId: string ) =>
    {
        getSceneData( sceneId ).then( data =>
        {
            if ( data )
            {
                setSpaceThumbnail( data.space.thumbnail );
                three.init( data, ( xhr: ProgressEvent<EventTarget> ) =>
                {
                    iframe.sendMessage( SpaceSdkCmd.progressbar, xhr.loaded / xhr.total * 0.99 )
                    setProgress( xhr.loaded / xhr.total * 0.99 )
                } ).then( () =>
                {
                    iframe.sendMessage( SpaceSdkCmd.progressbar, 1.0 )
                    setProgress( 1 )
                } )
            } else
            {
                setValid( false )
            }
        } )
        three.animate()
        setActions( {
            save: three.save.bind( three ),
            goDown: three.godown.bind( three ),
            goTop: three.goup.bind( three ),
            createObject: three.createObject.bind( three ),
        } )
    }

    return (
        <div className="viewerContainer" >
            <div id="css3d"></div>
            <canvas id="three-root" onDragOver={ ( e ) => { e.preventDefault() } } tabIndex={ 1 } />
            <TagHoverLayer />
            {
                deleteBtn &&
                <DeleteButton
                    callback={ deleteBtn.callback }
                    left={ deleteBtn.left }
                    top={ deleteBtn.top }
                ></DeleteButton>
            }

            {
                loadingprogress !== 1 && spaceThumbnail &&
                <ProgressBar url={ spaceThumbnail.replace( '256', '1024' ) } percent={ loadingprogress }></ProgressBar>
            }

        </div >
    )
};

export default ThreeCanvas;
