import * as React from 'react';
import './ProgressBar.css';

interface IProgressBar
{
    url: string;
    percent: number;
}

const ProgressBar: React.FunctionComponent<IProgressBar> = ( props ) =>
{
    const radius = 52
    const circumference = radius * 2 * Math.PI;

    return (
        <div className="progressbar" >
            <img id="thumbnail" src={ props.url } />

            <div id="progress" >

                <svg className="progress-ring" width="120" height="120">
                    <circle
                        className="progress-ring__circle"
                        stroke="white"
                        strokeWidth="4"
                        strokeDasharray={ `${ circumference } ${ circumference }` }
                        strokeDashoffset={ circumference - props.percent * circumference }
                        fill="transparent"
                        r={ radius.toString() }
                        cx="60"
                        cy="60" />
                </svg>

            </div>
        </div >
    )
}

export default ProgressBar;
export type { IProgressBar };