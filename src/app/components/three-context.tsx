import React from "react";

export interface ThreeContextType
{
    modalBoxDisplay: string;
    setModalDisplay: ( value: string ) => void;
    url: string;
    setUrl: ( value: string ) => void;
    actions: {
        save: () => void;
        goTop: () => void;
        goDown: () => void;
        createObject: ( data ) => void;
        swapObject: ( data ) => void;
    }
    setActions: ( value: object ) => void
    property: {
        value: object,
        update: ( value: object ) => void
    },
    setProperty: ( value: object ) => void

};

const ThreeContext = React.createContext<ThreeContextType | undefined>( {
    modalBoxDisplay: "block",
    setModalDisplay: () => { },
    url: "",
    setUrl: () => { },
    actions: {
        save: () => { },
        goTop: () => { },
        goDown: () => { },
        createObject: ( data ) => { },
        swapObject: ( data ) => { }
    },
    setActions: () => { },
    property: {
        value: {},
        update: ( value: object ) => { }
    },
    setProperty: () => { }
} )



export default ThreeContext;
