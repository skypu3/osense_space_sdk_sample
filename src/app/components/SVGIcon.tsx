import * as React from 'react';
import styled from 'styled-components';

const IconWrapper = styled.div`
    width: 32px;
    height: 32px;
    margin: 10px;
    display: flex;
    justify-content: center;
    align-items: center; 
  & path {
    fill:${ props => ( props.selected ? '#FFFFFF' : '#FFFFFF' ) };
  }
   `
interface ISVGprops
{
    icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>
    currentIndex?: Function
    index?: number
    onClick?: Function
}
interface SVGState
{
    selected: boolean
}
export default class SVGIcon extends React.Component<ISVGprops, SVGState>
{
    onClick: Function
    constructor( props: ISVGprops )
    {
        super( props );
        this.state = { selected: false };
        this.handleClick = this.handleClick.bind( this );
        this.onClick = this.props.onClick;
    }

    handleClick()
    {
        const selected = this.state.selected;
        this.onClick( this.props.index );
        this.setState( { selected: !selected } );
    }

    render()
    {
        const { currentIndex, index } = this.props;
        const SVG = this.props.icon;
        const selected = currentIndex() === index ? true : false;

        return <div className={ "ICON" }>
            <IconWrapper selected={ selected } >
                <SVG onClick={ this.handleClick } />
            </IconWrapper>
        </div>
    }
}