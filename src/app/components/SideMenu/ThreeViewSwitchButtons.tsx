import * as React from 'react';
import "./ThreeViewSwitchButtons.css";
import ThreeContext from "../three-context";

interface IThreeViewSwitchButtonsProps
{
}

const ThreeViewSwitchButtons: React.FunctionComponent<IThreeViewSwitchButtonsProps> = ( props ) =>
{
    const { actions } = React.useContext( ThreeContext );

    const [ select, setSelect ] = React.useState( 0 );
    return (
        <div className="three-buttons">
            <button
                className={ isActive( 0, select ) }
                onClick={ () =>
                {
                    actions.goDown()
                    setSelect( 0 );
                } }>
                <img src="/images/icon/human.svg" alt="first-person" />
            </button>
            <button
                className={ isActive( 1, select ) }
                onClick={ () =>
                {
                    actions.goTop()
                    setSelect( 1 )
                } }>
                <img src="/images/icon/rectangle.svg" alt="bird-view" />
            </button>
        </div>
    );
};

function isActive( key: number, select: number )
{
    return key === select ? "active" : "";
}

export default ThreeViewSwitchButtons;

