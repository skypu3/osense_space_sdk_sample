import React from 'react';
import 'core/i18n/i18n';
import './TitlebarGridList.css';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import { getMediaList } from 'app/api/Media';
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import ThreeContext from "../three-context";
import { EObjectType } from 'core/three/mediaObject/MediaObject3D';

const useStyles = makeStyles( ( theme: Theme ) =>
    createStyles( {
        root: {
            backgroundColor: theme.palette.background.paper,
        }
    } ),
);

export interface ITitlebarGridListProps
{
    className: string;
    type: EObjectType;
    onCloseEvent?: () => void;
}

const TitlebarGridList: React.FunctionComponent<ITitlebarGridListProps> = ( props: ITitlebarGridListProps ) =>
{
    const classes = useStyles();
    const { t } = useTranslation();
    const { actions } = React.useContext( ThreeContext );
    const [ dragElement, setDragElement ] = React.useState( undefined );

    // const { loading, error, data } = useQuery( getMediaList( props.type ) );

    // if ( loading ) return null;
    // if ( error ) return <div>`Error! ${ error }`</div>;

    const itemList = getMediaList( props.type ) //data.currentMedialist;

    const pointerDown = ( tile, event ) =>
    {
        actions.createObject( tile );
    }

    return (
        <div className={ classes.root + ` TitlebarGridList ${ props.className }` }>
            <div className="title" key="Subheader">
                <ListSubheader component="div" style={ { color: 'white', fontSize: 18 } }>{ t( `media.${ props.type }` ) }</ListSubheader>
                <button onClick={ () => { props.onCloseEvent(); } }>
                    <FontAwesomeIcon icon={ faAngleLeft } size="2x" />
                </button>
            </div>
            <GridList cellHeight={ 230 } className="gridList">
                {
                    itemList && itemList.map( ( tile, index ) => (
                        <GridListTile onPointerDown={ pointerDown.bind( this, tile ) } key={ `Item${ index }` } draggable="false">
                            <img src={ tile.thumbnail ?? getDefaultIcon( props.type ) } alt={ tile.name } draggable="false" />
                            <GridListTileBar
                                style={ { backgroundColor: 'rgba(99, 114, 128, 0.5)', textAlign: 'center' } }
                                title={ tile.name }
                            />
                        </GridListTile>
                    ) )
                }
            </GridList>
        </div>
    );
}

function getDefaultIcon( type: string ): string
{
    return `/images/icon/${ type.toLowerCase() }.svg`;
}


export default TitlebarGridList;