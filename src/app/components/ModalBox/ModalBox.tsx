import * as React from 'react';
import { useContext } from 'react';
import ThreeContext from "../three-context";
import "./ModalBox.css"
interface IModalBoxProps
{
}

const ModalBox: React.FunctionComponent<IModalBoxProps> = ( props ) =>
{
    const { modalBoxDisplay, setModalDisplay, url, setUrl } = useContext( ThreeContext );

    return (
        <div id="myModal" className="modal" style={ { display: modalBoxDisplay } }>
            <span
                role="presentation"
                className="close"
                onClick={ () => setModalDisplay( 'none' ) }
            >
                &times;{ ' ' }
            </span>

            <div className="modal-content">
                <iframe
                    className="iframe-content"
                    title="Modal-Box"
                    src={ url }
                />
            </div>
        </div>
    )
}

export default ModalBox;