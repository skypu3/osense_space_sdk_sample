import React from 'react';
import './App.css';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Global from './core/three/base/Global';
import TagEditor from "app/pages/TagEditor";
import { useLocation } from 'react-router-dom'
import { Provider } from 'react-redux';
import store from './core/store/store'
// import FloorplanEditor from 'app/pages/FloorplanEditor/FloorplanEditor';

function QueryRouter( props )
{
    const path: string = props.location.pathname;
    const query = new URLSearchParams( useLocation().search );
    const buildingId = query.get( "id" );
    switch ( path.toLowerCase() )
    {
        case "/":
            return <TagEditor buildingId={ buildingId }></TagEditor>
        // case "/floorplaneditor":
        //     return <FloorplanEditor buildingId={ buildingId }></FloorplanEditor>
        default:
            return <TagEditor buildingId={ buildingId }></TagEditor>
    }
}

function App()
{
    return (
        <Provider store={ store }>
            <ApolloProvider client={ Global.inst.network.client }>
                <div className="App">
                    <BrowserRouter>
                        <Switch>
                            <Route path="/*" component={ QueryRouter } />
                        </Switch>
                    </BrowserRouter>
                </div>
            </ApolloProvider>
        </Provider>
    );

}



export default App;
