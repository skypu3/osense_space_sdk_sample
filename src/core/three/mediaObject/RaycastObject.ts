import * as THREE from 'three';
import { EObjectType } from './MediaObject3D';
import Media from './media/Media';
import { BufferGeometryUtils } from 'three/examples/jsm/utils/BufferGeometryUtils.js';
import { mergeBufferGeometries } from '../examples/jsm/utils/BufferGeometryUtils';

export enum EObjectMouseState
{
    ON,
    NOTON,
}

export interface RaycastClickData
{
    type?: number;
    id?: string;
    url?: string;
    object?: THREE.Object3D;
    setJson?( obj: object ): void;
    json?: object;
    mediaClass?: Media;
    editEnabled?: boolean;
}

export class RaycastType
{
    static NONE        = 0b0;
    static SHOWMODAL   = 0b1;
    static EDIT        = 0b10;
    static MOVE        = 0b100;
    static REDIRECT    = 0b1000;
    static OPENWINDOW  = 0b10000;
    static AVATAR      = 0b100000;
    static SHARESCREEN = 0b1000000;
    static BUTTON      = 0b10000000;
    static hasProperty( value: number, type: number )
    {
        return ( value & type ) === type
    }
}

export abstract class RaycastObject
{
    public state: EObjectMouseState;
    constructor()
    {
        this.state = EObjectMouseState.NOTON
    }
    public abstract get targetInfo(): RaycastClickData;
    public abstract get raycastTarget(): THREE.Object3D;
    abstract onMouseEnter(): void;
    abstract onMouseLeave(): void;
    abstract onMouseMove( rot4: THREE.Matrix4, pos: THREE.Vector3 ): void
    abstract onMouseUp( rot4: THREE.Matrix4, pos: THREE.Vector3 ): void;

    public static mergeGroup( group ): THREE.Mesh
    {
        const parent = group.parent;
        if ( parent )
            parent.remove( group );

        group.updateMatrixWorld( true )
        const geometry = new THREE.Geometry()
        group.traverse( ( node: THREE.Mesh ) =>
        {
            if ( !node.isMesh ) return;
            if ( node.geometry.type === 'BufferGeometry' )
            {
                node.updateMatrixWorld( true )
                geometry.merge(
                    new THREE.Geometry().fromBufferGeometry( node.geometry as THREE.BufferGeometry ),
                    node.matrixWorld
                );
            }
        } );

        if ( parent )
            parent.add( group )

        const material = new THREE.MeshBasicMaterial();
        const mesh = new THREE.Mesh( geometry, material );
        mesh.layers.disableAll();
        return mesh
    }

    public static mergeMeshes( meshes: THREE.Mesh | THREE.Mesh[] ): THREE.Mesh
    {
        const meshList = [].concat( meshes || [] );
        const geometry = new THREE.Geometry()


        meshList.forEach( mesh =>
        {
            const parent = mesh.parent;
            if ( parent )
                parent.remove( mesh );

            mesh.updateMatrixWorld( true )
            geometry.merge(
                new THREE.Geometry().fromBufferGeometry( mesh.geometry as THREE.BufferGeometry ),
                mesh.matrixWorld
            );

            if ( parent )
                parent.add( mesh )
        } )
        const mesh = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial() );
        mesh.layers.disableAll();
        return mesh;
    }

    public static mergeGeometries( geos: THREE.BufferGeometry | THREE.BufferGeometry[] ): THREE.Mesh
    {
        const geometrys = [].concat( geos || [] );
        const mesh = new THREE.Mesh(
            mergeBufferGeometries( geometrys ),
            new THREE.MeshBasicMaterial()
        )
        mesh.layers.disableAll();
        return mesh;
    }

    public static emptyRaycastMesh()
    {
        const mesh = new THREE.Mesh(
            new THREE.PlaneBufferGeometry( 1, 1, 4 ),
            new THREE.MeshBasicMaterial()
        )
        mesh.layers.disableAll();
        return mesh;
    }
}