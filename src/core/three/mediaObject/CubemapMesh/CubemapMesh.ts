import { mergeBufferGeometries } from 'core/three/examples/jsm/utils/BufferGeometryUtils';
import * as THREE from 'three'
import CubemapMaterial from './shader/CubemapMaterial'

export default class CubemapMesh extends THREE.Mesh {
    public cubemapMaterial: CubemapMaterial;
    constructor(glb: THREE.Object3D) {
        function mergeGeosBBbox(glb: THREE.Object3D, size = 30000): THREE.BoxGeometry {
            const geometry = new THREE.BoxGeometry(size, size, size);
            for (var i = 0; i < geometry.faces.length; i++) {
                var tmp = geometry.faces[i].a;
                geometry.faces[i].a = geometry.faces[i].c;
                geometry.faces[i].c = tmp;
            }

            let meshesMergedArray: THREE.BufferGeometry[] = [];
            try {
                glb.traverse((node: THREE.Mesh) => {
                    if (!node.isMesh) return;
                    if (node.geometry.type !== 'BufferGeometry') {
                        return;
                    }

                    if (node) {
                        node.updateMatrixWorld(true);
                        let cloneMesh = node.geometry.clone();
                        cloneMesh.applyMatrix4(node.matrixWorld);
                        meshesMergedArray.push(cloneMesh as THREE.BufferGeometry)
                    }
                });

                const mergeGeometries = mergeBufferGeometries(meshesMergedArray, true);

                if (mergeGeometries) {
                    geometry.merge(
                        new THREE.Geometry().fromBufferGeometry(mergeGeometries),
                    );

                    return geometry
                }
            } catch (err) {
            }

            glb.traverse((node: THREE.Mesh) => {
                if (!node.isMesh) return;
                if (node.geometry.type !== 'BufferGeometry') {
                    return;
                }

                if (node)
                    geometry.merge(
                        new THREE.Geometry().fromBufferGeometry(node.geometry as THREE.BufferGeometry),
                        node.matrixWorld
                    );
            });

            return geometry;

        }

        const geometry = mergeGeosBBbox(glb);
        const material = new CubemapMaterial();
        const keyColor = new THREE.Color( 0xffffff );
        material.uniforms.color.value = keyColor;


        super(geometry, material)

        this.cubemapMaterial = material;
    }
}
