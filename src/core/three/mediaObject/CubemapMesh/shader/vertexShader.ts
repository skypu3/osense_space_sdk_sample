export default `
varying vec3 modelPositionCubemap;
varying mediump vec3 modelPositionMap;

void main()	
{
    vec4 worldPosition = modelMatrix * vec4( position, 1.0 ) ;

    modelPositionCubemap = vec3(worldPosition.x,worldPosition.y,-worldPosition.z);
    modelPositionMap = vec3(position.x, -position.y, -position.z);

    gl_Position =  projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}
`;
