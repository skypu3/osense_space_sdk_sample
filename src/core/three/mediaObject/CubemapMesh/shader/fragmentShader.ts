export default `
varying vec3 modelPositionCubemap;
varying mediump vec3 modelPositionMap;

uniform vec3 panoOffset;
uniform samplerCube timePanorama01;
uniform samplerCube timePanorama02;
uniform float compress;

uniform sampler2D tAltas0;
uniform sampler2D tAltas1;

uniform int texture0isAltas;
uniform int texture1isAltas;

uniform vec3 startPos;
uniform vec4 startRot;
uniform vec3 endPos;
uniform vec4 endRot;

uniform lowp float time;
uniform mediump sampler2D tex;
uniform mediump vec3 color;
uniform lowp float blackFactor;
uniform lowp float alpha;

void pano(out float textureId, vec3 a, out vec2 st) {
    
    lowp vec3 posa = abs(a);
    mediump vec3 uvw;

    if (posa.x > posa.y && posa.x > posa.z) {
        lowp float negx = -step(0.0, a.x);
        textureId = negx;
        lowp float posx = step(a.x, 0.0);
        uvw = vec3(a.zy, posa.x) * vec3(mix(-1.0, 1.0, posx), -1, 1);
    } else if (posa.y > posa.z) {
        lowp float negy = -step(0.0, a.y);
        textureId = 2.0 + negy;
        lowp float posy = step(a.y, 0.0);
        uvw = vec3(a.xz, posa.y) * vec3(1.0, mix(1.0, -1.0, posy), 1.0);
    } else {
        lowp float negz = -step(0.0, a.z);
        textureId = 4.0 + negz;
        lowp float posz = step(a.z, 0.0);
        uvw = vec3(a.xy, posa.z) * vec3(mix(1.0, -1.0, posz), -1, 1);
    }
    st = 0.6 * vec2(2. + uvw.xy / uvw.z);
}
 
mediump vec4 makePanorama(vec3 uvw, sampler2D tEquirect) {
    lowp float textureId;
    mediump vec2 vector;

    pano(textureId, uvw, vector);

    lowp float sortfactor;
    if (textureId > 4.0) sortfactor = 12.0;
    else if (textureId > 3.0) sortfactor = 14.0;
    else if (textureId > 2.0) sortfactor = 11.0;
    else if (textureId > 1.0) sortfactor = 10.0;
    else if (textureId > 0.0) sortfactor = 15.0;
    else sortfactor = 13.0;

    return texture2D(tEquirect, vec2(vector.x*compress, (vector.y*compress + sortfactor) / 10.0));
}

vec3 dodotcross(vec4 a, vec3 b){ 
	return 2.0*cross(a.w*b + cross(b, a.xyz), a.xyz) + b;
}
 
mediump vec4 getColorMap(vec3 point, sampler2D texture, vec3 position, vec4 quaternion, mat3 rotateOffset){

    point.x = abs(position.x - point.x);
    point.y = abs(position.y + point.y);
    point.z = abs(position.z + point.z);

    vec3 modelViewMatrix = dodotcross(quaternion,point)*rotateOffset;

    return makePanorama(normalize(modelViewMatrix),texture);
}

mediump vec4 getColorPanorama(vec3 point, samplerCube tex, vec3 position, vec4 quaternion, mat3 rotateOffset){

    point.x = point.x - position.x;
    point.y = point.y - position.y;
    point.z = point.z + position.z;

    vec3 modelViewMatrix = dodotcross(quaternion,point)*rotateOffset;

    return vec4(textureCube(tex, modelViewMatrix).xyz, 1.0);
}

void main()	{	    

    mat3 rotationMatrixX = mat3(1.0, 0.0, 0.0,
        0.0, cos(panoOffset.x), -sin(panoOffset.x),
        0.0, sin(panoOffset.x),cos(panoOffset.x));

    mat3 rotationMatrixY = mat3(cos(panoOffset.y), 0.0, sin(panoOffset.y), 
            0.0, 1.0, 0.0,
            -sin(panoOffset.y), 0.0, cos(panoOffset.y));

    mat3 rotationMatrixZ = mat3(cos(panoOffset.z), -sin(panoOffset.z),0.0,
            sin(panoOffset.z), cos(panoOffset.z), 0.0,
            0.0,0.0,1.0);
    mat3 rotateOffset = rotationMatrixX*rotationMatrixY*rotationMatrixZ;

    lowp vec4 color0 = vec4(0.0,0.0,0.0,1.0);
    lowp vec4 color1 = vec4(0.0,0.0,0.0,1.0);
    
    if(texture0isAltas==0){
        color0 = getColorPanorama(modelPositionCubemap,timePanorama01,startPos,startRot,rotateOffset);
    }else{
        color0 = getColorMap(modelPositionMap,tAltas0,startPos,startRot,rotateOffset);
    }

    if(texture1isAltas==0){
        color1 = getColorPanorama(modelPositionCubemap,timePanorama02,endPos,endRot,rotateOffset);
    }else{
        color1 = getColorMap(modelPositionMap,tAltas1,endPos,endRot,rotateOffset);
    }

    vec3 mixColor = mix(color0.xyz,color1.xyz,time);

    if(color.x != 1.0 && color.y != 1.0 && color.z != 1.0) {
        mediump float a = (length(mixColor - color) - 0.5) * 7.0;
        if(a>1.0)
            discard;
        else
            gl_FragColor = vec4(mixColor.x*blackFactor,mixColor.y*blackFactor,mixColor.z*blackFactor, alpha);
    } 
    else {
        gl_FragColor = vec4(mixColor.x*blackFactor,mixColor.y*blackFactor,mixColor.z*blackFactor, alpha);
    }
}
`;
