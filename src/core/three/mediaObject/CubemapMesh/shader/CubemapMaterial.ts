import { Format } from 'core/three/scene/Format';
import * as THREE from 'three';
import fragmentShader from './fragmentShader';
import vertexShader from './vertexShader';

export interface CubemapInfo
{
    id: string
    position: {
        x: number,
        y: number,
        z: number
    },
    rotation: {
        x: number,
        y: number,
        z: number,
        w: number
    },
    texture: THREE.CubeTexture | THREE.Texture
}

export default class CubemapMaterial extends THREE.ShaderMaterial
{
    public currentIndexFade: boolean;
    constructor()
    {
        super( {
            fragmentShader: fragmentShader,
            vertexShader: vertexShader,
            uniforms: {
                panoOffset: { value: new THREE.Vector3() },
                compress: {value: 0.998 },
                tAltas0: { value: null },
                tAltas1: { value: null },
                texture0isAltas: { value: null },
                texture1isAltas: { value: null },
                timePanorama01: { value: null },
                timePanorama02: { value: null },
                startPos: { value: new THREE.Vector3() },
                startRot: { value: new THREE.Vector3() },
                endPos: { value: new THREE.Vector4() },
                endRot: { value: new THREE.Vector4() },
                tex: {
                    value: null,
                },
                color: {
                    value: null,
                },
                time: { value: 0 },
                blackFactor: { value: 1 },
                alpha: { value: 0.0 },
            },
            transparent: true,
            polygonOffset: true,
            polygonOffsetFactor: 1.0,
            polygonOffsetUnits: 4.0
        } )
        this.currentIndexFade = true;
    }

    public set axisOnZ( value: boolean )
    {
        if ( value )
        {
            this.uniforms.panoOffset.value = new THREE.Vector3( 0.0, 0.0, 0.0 );
        } else
        {
            this.uniforms.panoOffset.value = new THREE.Vector3( 0.0, 0.0, 0.0 );
        }
    }

    public set alpha( value: number )
    {
        this.uniforms.alpha.value = value;
    }

    public set transition( value: number )
    {
        this.uniforms.time.value = value;
    }

    private destroyOriginalTexture( conatiner, texture )
    {
        const oldTexture = conatiner.value;
        conatiner.value = texture;
        if ( oldTexture != null )
            oldTexture.dispose()
    }

    public async setTexture( info: CubemapInfo )
    {
        const { position, rotation, texture } = info

        texture.generateMipmaps = false;
        texture.minFilter = THREE.LinearFilter;
        texture.magFilter = THREE.LinearFilter;
        texture.wrapS = THREE.ClampToEdgeWrapping;
        texture.wrapT = THREE.ClampToEdgeWrapping;

        this.uniforms.tex.value = texture;

        if ( this.currentIndexFade )
        {
            if ( texture instanceof THREE.CubeTexture )
            {
                this.destroyOriginalTexture( this.uniforms.timePanorama01, texture )
                this.uniforms.texture0isAltas.value = 0;
            }
            else if ( texture instanceof THREE.Texture )
            {
                this.destroyOriginalTexture( this.uniforms.tAltas0, texture )
                this.uniforms.texture0isAltas.value = 1;
            }

            this.uniforms.startPos.value = new THREE.Vector3( position.x, position.y, position.z )
            this.uniforms.startRot.value = new THREE.Vector4( rotation.x, rotation.y, rotation.z, rotation.w )
        }
        else
        {
            if ( texture instanceof THREE.CubeTexture )
            {
                this.destroyOriginalTexture( this.uniforms.timePanorama02, texture )
                this.uniforms.texture1isAltas.value = 0;
            }
            else if ( texture instanceof THREE.Texture )
            {
                this.destroyOriginalTexture( this.uniforms.tAltas1, texture )
                this.uniforms.texture1isAltas.value = 1;
            }

            this.uniforms.endPos.value = new THREE.Vector3( position.x, position.y, position.z )
            this.uniforms.endRot.value = new THREE.Vector4( rotation.x, rotation.y, rotation.z, rotation.w )
        }
    }
}
