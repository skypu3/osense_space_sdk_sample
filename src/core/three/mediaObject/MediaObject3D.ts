import { SpaceSdkCmd } from 'core/store/actions/iframe';
import store from 'core/store/store';
import * as THREE from 'three';
import NormalDepthMapMaterial from '../SceneRaycaster/NormalDepthMap/NormalDepthMapMaterial';
import { RaycastObject, RaycastClickData } from './RaycastObject';

export enum EObjectType {
    TEXT = "TEXT",
    PHOTO = "PHOTO",
    GIF = "GIF",
    VIDEO = "VIDEO",
    MODEL = "GLB",
    SPRITE = "SPRITE",
    TAG = "TAG",
    HOTSPOT = "HOTSPOT",
    WEB = "WEB",
    MOUSEICON = "MOUSEICON",
    SCENEMODEL = "SCENEMODEL",
    PORTAL = "PORTAL",
    BOOTH = "BOOTH",
    AVATAR = "AVATAR",
    POSITIONPLANE = "POSITIONPLANE",
    POSITIONCUBE = "POSITIONCUBE",
    BUTTON = "BUTTON",
    SHARESCREEN = "SHARESCREEN" 
}

export default abstract class MediaObject3D extends RaycastObject {
    public id: string;
    public type: EObjectType;
    private _object: THREE.Object3D;
    private renderDepthAndNormalMat = new NormalDepthMapMaterial();
    private oldMaterial = null;
    public get object() {
        return this._object;
    }

    // public renderIndexColor(renderIndex) {
    // }

    public renderNormalAndDepth(value) {
        const raycastTarget = this.raycastTarget
        if (raycastTarget) {
            if (this.oldMaterial === null)
                this.oldMaterial = raycastTarget.material;
            if (value) {
                raycastTarget.material = this.renderDepthAndNormalMat;
            } else {
                raycastTarget.material = this.oldMaterial;
            }
        }
    }


    public enableRaycasterLayer(visiblelayer) {
        const raycastTarget = this.raycastTarget
        if (raycastTarget)
            raycastTarget.layers.enable(visiblelayer)
    }

    public setRaycastMaterial(visibleLayer, index: number): boolean {
        function genColorMaterial(index): THREE.ShaderMaterial {
            return new THREE.ShaderMaterial({
                vertexShader: `
                void main()	
                {
                    mediump vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
                    gl_Position = projectionMatrix * mvPosition;
                }`,
                fragmentShader: `
                uniform vec3 color;            
                void main() {           
                    gl_FragColor = vec4(color,1.0);
                }
                `,
                uniforms: {
                    color: { value: new THREE.Color().setHex(index.toString()) }
                },
            });
        }

        const raycastTarget = this.raycastTarget
        if (raycastTarget) {
            raycastTarget.layers.enable(visibleLayer)

            const oldMaterial = raycastTarget.material;
            raycastTarget.material = genColorMaterial(index);
            if (oldMaterial && oldMaterial.dispose) {
                oldMaterial?.dispose()
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param type object type
     */
    constructor(type: EObjectType) {
        super();
        this._object = new THREE.Group();
        this.updateRaycastMesh();
        this.type = type;
        this.custumDestoryFuncs = []
    }

    public get json(): object {
        return {
            type: this.type,
            position: {
                x: this.object.position.x,
                y: this.object.position.y,
                z: this.object.position.z
            },
            rotation: {
                x: this.object.quaternion.x,
                y: this.object.quaternion.y,
                z: this.object.quaternion.z,
                w: this.object.quaternion.w
            },
            scale: {
                x: this.object.scale.x,
                y: this.object.scale.y,
                z: this.object.scale.z,
            }
        }
    }

    public setJson(json: object) { };

    public get targetInfo(): RaycastClickData {
        return {
            setJson: this.setJson.bind(this),
            json: this.json,
            object: this.object,
            id: this.id,
            editEnabled: true,
        };
    }

    protected raycastMesh: THREE.Mesh;
    public get raycastTarget(): any {
        return this.raycastMesh;
    }
    protected updateRaycastMesh(mesh = undefined, parent = undefined) {
        if (!mesh) {
            this.raycastMesh = RaycastObject.emptyRaycastMesh()
            this.object.add(this.raycastMesh);
            return;
        }
        let mergeFunction;

        if (mesh instanceof THREE.Group)
            mergeFunction = RaycastObject.mergeGroup
        else if (mesh instanceof THREE.BufferGeometry || mesh[0] instanceof THREE.BufferGeometry)
            mergeFunction = RaycastObject.mergeGeometries
        else if (mesh instanceof THREE.Mesh || mesh[0] instanceof THREE.Mesh) {
            mergeFunction = RaycastObject.mergeMeshes
        } else {
            mergeFunction = RaycastObject.mergeGeometries
        }

        if (this.raycastMesh) {
            const oldGeomerty = this.raycastMesh.geometry
            this.raycastMesh.geometry = mergeFunction(mesh).geometry
            if (oldGeomerty)
                oldGeomerty.dispose()
        } else {
            this.raycastMesh = mergeFunction(mesh)
            if (parent) {
                parent.add(this.raycastMesh);
            } else {
                this.object.add(this.raycastMesh);
            }
        }
    }

    /**
     * clean this object, js wiil not call this automaticlly
     */
    private custumDestoryFuncs: (() => void)[];
    public addCustumDestory(func: () => void) {
        this.custumDestoryFuncs.push(func)
    }
    public destroy() {
        this.custumDestoryFuncs.forEach(destroy => {
            destroy();
        });

        if (this.object.parent)
            this.object.parent.remove(this.object);

        if (store.getState().iframeReducer == null) return;
        const send = store.getState().iframeReducer?.sendMessage as (command: SpaceSdkCmd, data: any) => void;
        send(SpaceSdkCmd.deleteobject, {
            'tagId': this.id
        });
    }
}
