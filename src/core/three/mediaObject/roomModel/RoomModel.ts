import * as THREE from 'three'
import { EObjectType } from 'core/three/mediaObject/MediaObject3D';
import Model from 'core/three/mediaObject/media/model/Model';
import CubemapMesh from 'core/three/mediaObject/CubemapMesh/CubemapMesh';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import { RaycastClickData, RaycastType } from 'core/three/mediaObject/RaycastObject';

export default class RoomModel extends Model {
    meshGroup: THREE.Group;
    panoMesh: CubemapMesh;
    private _animationPercentage;
    constructor() {
        super()
        this.type = EObjectType.SCENEMODEL;
        this.object.remove(this.boundaryBox);
    }

    private traverse(object: THREE.Group, seteMat?: (mtl: THREE.Material) => void, setMesh?: (node: THREE.Mesh) => void) {
        object.traverse((node: THREE.Mesh) => {
            if (!node.isMesh) return;
            const materials = Array.isArray(node.material)
                ? node.material
                : [node.material];
            if (seteMat)
                materials.forEach(seteMat);
            if (setMesh)
                setMesh(node);
        });
    }

    public overwriteGLTFmat(obj) {
        this.traverse(obj, (material) => {
            // @ts-ignore
            if (material.map) material.map.encoding = THREE.sRGBEncoding;
            // @ts-ignore
            if (material.emissiveMap) material.emissiveMap.encoding = THREE.sRGBEncoding;
            // @ts-ignore
            if (material.map || material.emissiveMap) material.needsUpdate = true;
            material.transparent = true;

        }, (mesh) => {
            mesh.renderOrder = -1;
        });

        this.meshGroup = obj;
        this.panoMesh = new CubemapMesh(obj);
        this.panoMesh.renderOrder = -3;

    }

    public setGLTF(obj: GLTF) {
        super.setGLTF(obj, this.overwriteGLTFmat.bind(this))
    }

    public setMesh(obj) {
        this.overwriteGLTFmat(obj);
        super.setMesh(obj);
    }

    public removeSceneMesh() {
        if (this.glb) {
            this.glb.traverse((node: THREE.Mesh) => {
                const oldGeometry = node.geometry;
                const oldMaterial = node.material;
    
                if (oldMaterial) {
                    const map = (oldMaterial as any).map
                    if (map && map.dispose) { 
                        map.dispose()
                    }
                    (oldMaterial as any).dispose()
                } 
    
                if (oldGeometry) {
                    oldGeometry.dispose()
                }
            });
            this.meshGroup = null;
            this.object.remove(this.glb)
            this.glb = null;
        }
    }

    public async load(url: string): Promise<void> {
        return super.load(url, this.overwriteGLTFmat.bind(this))
    }

    get panoMaterial() {
        return this.panoMesh.cubemapMaterial;
    }

    set transitionPercentage(value: number) {
        
        this._animationPercentage = value;

        if (this._animationPercentage === 0) {
            this.object.remove(this.glb);
            this.object.add(this.panoMesh);
        }
        else if (this._animationPercentage === 1) {
            this.object.add(this.glb);
            this.object.remove(this.panoMesh);
        } else {
            this.object.add(this.glb);
            this.object.add(this.panoMesh);
        }
        this.traverse(this.meshGroup, mat => {
            mat.opacity = value;
        })
    }

    get transitionPercentage() {
        return this._animationPercentage;
    }

    public get sceneBoundary(): THREE.Box3 {
        const boundary = new THREE.Box3();
        boundary.setFromObject(this.panoMesh);
        return boundary;
    }

    public get targetInfo(): RaycastClickData {
        return {
            ...super['targetInfo'],
            type: RaycastType.MOVE,
            id: "sceneModel",
            url: '',
            editEnabled: false,
        }
    }

}