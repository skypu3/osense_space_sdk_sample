import * as THREE from 'three';
import RoomModel from './RoomModel';
import { IAction } from 'core/anime/Timeline';
import { Format } from 'core/three/scene/Format';
import { CubemapInfo } from 'core/three/mediaObject/CubemapMesh/shader/CubemapMaterial';

export default class AnimeRoomModel extends RoomModel
{
    constructor()
    {
        super()
        this.hotspotid = undefined
    }

    public hotspotid: string;

    public setHdTexture( hotspot: CubemapInfo )
    {
        if ( this.hotspotid === hotspot.id )
            this.panoMaterial.setTexture( hotspot );
    }

    private fromTop( hotspot: CubemapInfo, showPanoramaMesh: boolean = true ): IAction
    {
        function easing( t )
        {
            return 1 + ( t - 1 ) ** 5;
        }

        this.hotspotid = hotspot.id;
        this.panoMaterial.setTexture( hotspot )

        const scope = this;
        return {
            progress: {
                inverse: true,
                update: function ( percentage )
                {
                    const percent = easing( percentage );
                    scope.transitionPercentage = showPanoramaMesh ? percent : 1;
                    scope.panoMaterial.alpha = 1 - percent;
                }
            }
        }
    }

    private fromHotspot( hotspot: CubemapInfo ): IAction
    {
        this.hotspotid = hotspot.id;
        this.panoMaterial.currentIndexFade = !this.panoMaterial.currentIndexFade;
        this.panoMaterial.setTexture( hotspot )

        const scope = this;
        return {
            easing: 'easeInOutSine',
            progress: {
                inverse: scope.panoMaterial.currentIndexFade,
                update: function ( percentage )
                {
                    scope.panoMaterial.transition = percentage;
                }
            },
            complete: function ()
            {
                scope.panoMaterial.transition = scope.panoMaterial.currentIndexFade ? 0 : 1
            }
        }
    }

    public goHotspot( hotspot: CubemapInfo, showPanoramaMesh: boolean = true ): IAction
    {
        if ( this.hotspotid )
            return this.fromHotspot( hotspot )
        else
            return this.fromTop( hotspot, showPanoramaMesh )
    }

    public goTop(): IAction
    {
        this.hotspotid = undefined;
        this.transitionPercentage = 1;
        return {
            progress: {
                inverse: true,
                update: function ( percentage )
                {

                }
            }
        }
    }

}