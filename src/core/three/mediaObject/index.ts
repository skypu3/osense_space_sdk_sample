import Model from 'core/three/mediaObject/media/model/Model';
import Image from 'core/three/mediaObject/media/image/Image';
import Tag from 'core/three/mediaObject/media/image/Tag';
import Gif from 'core/three/mediaObject/media/image/Gif';
import Text from 'core/three/mediaObject/media/text/Text';
import Video from 'core/three/mediaObject/media/video/Video';
import Hotspot from 'core/three/mediaObject/hotspot/Hotspot';
import Portal from 'core/three/mediaObject/media/image/Portal';
import Sprite from 'core/three/mediaObject/media/image/Sprite';
import NewTag from 'core/three/mediaObject/media/image/NewTag';
import PositioningPlane from 'core/three/mediaObject/media/positioningObject/PositioningPlane';
import PositioningCube from 'core/three/mediaObject/media/positioningObject/PositioningCube';

const MediaObjects = {
    PositioningPlane,
    PositioningCube,
    Model,
    Image,
    Tag,
    Gif,
    Sprite,
    NewTag,
    Text,
    Video,
    Hotspot,
    Portal,
}

export default MediaObjects;