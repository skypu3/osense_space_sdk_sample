import * as THREE from "three";
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Object3D from "core/three/mediaObject/MediaObject3D";
import { Format } from "core/three/scene/Format";
import { RaycastClickData, RaycastType } from "core/three/mediaObject/RaycastObject";
import { Tween } from "@tweenjs/tween.js";
import { isMobile } from "react-device-detect";

export default class Hotspot extends Object3D {
    //public
    public lowerSkybox: string[6];
    public cameraHeight: number;
    public spaceId: string;
    public higherSkybox: string[6];
    public neighbors: string[];
    public uvRotation: {
        x: number,
        y: number,
        z: number,
        w: number,
    }

    public get targetInfo(): RaycastClickData {
        return {
            type: RaycastType.MOVE,
            ...super['targetInfo']
        }
    }

    public hide: boolean;
    private hotspotColor: string;
    private hotspotScale: number;

    //private
    private mouseIsEnter: boolean = false;
    private outterRing: THREE.RingBufferGeometry;
    private innerRing: THREE.RingBufferGeometry;

    private outterMesh: THREE.Group;
    private tween: Tween<any>;
    private innerMesh: THREE.Mesh;

    private material: THREE.MeshBasicMaterial;

    constructor() {
        super(EObjectType.HOTSPOT);
    }

    set info(data: Format.IHotspot) {
        this.id = data.id;
        this.lowerSkybox = data.lowerSkybox;
        this.cameraHeight = data.cameraHeight;
        this.spaceId = data.spaceId;
        this.higherSkybox = data.higherSkybox;
        this.neighbors = data.neighbors;
        // this uvRotation is for panorama projecting,
        // not the object rotation
        this.uvRotation = data.rotation
        this.hide = data.hide ? true : false;
        this.outterMesh = new THREE.Group()
        this.hotspotScale = data.hotspotScale
        this.hotspotColor = data.hotspotColor
        if (!this.hide) {
            this.createHotspot(data);
        }
    }

    public set visible(value: boolean) {
        if (this.outterMesh && this.innerMesh) {
            this.outterMesh.visible = value
            this.innerMesh.visible = value
        }
    }

    private createOutterMesh() {
        const ring1 = new THREE.RingBufferGeometry(0.17, 0.2, 36, 1, 1, 1.7);
        const ringMesh1 = new THREE.Mesh(ring1, this.material)
        ringMesh1.quaternion.setFromAxisAngle(
            new THREE.Vector3(1, 0, 0), -Math.PI / 2
        )

        const ring2 = new THREE.RingBufferGeometry(0.17, 0.2, 36, 1, 3, 1.7);
        const ringMesh2 = new THREE.Mesh(ring2, this.material)
        ringMesh2.quaternion.setFromAxisAngle(
            new THREE.Vector3(1, 0, 0), -Math.PI / 2
        )

        const ring3 = new THREE.RingBufferGeometry(0.17, 0.2, 36, 1, 5, 1.7);
        const ringMesh3 = new THREE.Mesh(ring3, this.material)
        ringMesh3.quaternion.setFromAxisAngle(
            new THREE.Vector3(1, 0, 0), -Math.PI / 2
        )

        this.outterMesh.add(ringMesh1)
        this.outterMesh.add(ringMesh2)
        this.outterMesh.add(ringMesh3)
    }

    private createHotspot(data: Format.IHotspot) {
        const { position: pos } = data;
        this.material = new THREE.MeshBasicMaterial({
            map: null,
            side: THREE.DoubleSide,
            transparent: true,
            opacity: 0.3,
            depthTest: true,
            color: new THREE.Color(this.hotspotColor)
        });

        this.createOutterMesh()

        this.tween = new Tween(this.outterMesh.rotation)
            .to({ y: "-" + Math.PI / 2 }, 1000)
            .delay(100)
            .onComplete(function () {
                if (Math.abs(this.outterMesh.rotation.rotation.y) >= 2 * Math.PI) {
                    this.outterMesh.rotation.y = this.outterMesh.rotation.y % (2 * Math.PI);
                }
            })

        if (isMobile) {
            this.tween.start()
            this.tween.repeat(Infinity)
        }

        this.innerRing = new THREE.RingBufferGeometry(0.11, 0.13, 36, 1, 32);
        this.innerMesh = new THREE.Mesh(this.innerRing, this.material)
        this.innerMesh.quaternion.setFromAxisAngle(
            new THREE.Vector3(1, 0, 0), -Math.PI / 2
        )

        this.object.add(this.outterMesh)
        this.object.add(this.innerMesh)
        this.object.position.set(pos.x, pos.y, pos.z);
        this.object.scale.addScalar(this.hotspotScale);

        const geometry = new THREE.SphereBufferGeometry(0.5, 32, 32);
        this.updateRaycastMesh(geometry)
    }

    public onMouseEnter(): void {
        if (this.mouseIsEnter === false || this.mouseIsEnter == null) {
            this.mouseIsEnter = true;
            this.material.opacity = 1;
            this.tween.start()
            this.tween.repeat(Infinity)
        }
    }
    public onMouseLeave(): void {
        if (this.mouseIsEnter === true) {
            this.mouseIsEnter = false;
            this.material.opacity = 0.3;
            this.tween.stop()
        }
    }

    onMouseMove(): void { }
    public onMouseUp(): void { }

    public destroy() {
        this.tween.stop()
        this.outterMesh.traverse((element) => element.remove())
        this.innerRing.dispose()
        this.material.dispose()
    }
}
