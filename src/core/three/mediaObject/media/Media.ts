import MediaObject3D from 'core/three/mediaObject/MediaObject3D';
import * as THREE from 'three';
import { RaycastClickData , RaycastType } from 'core/three/mediaObject/RaycastObject';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";

export default class Media extends MediaObject3D
{
    public name: string = "";
    public fileUrl: string = "";
    public openLink: string = "";
    public thumbnail: string = "";
    public imageHash: string = "";
    public isFrame: boolean = false;
    public linkName: string = ""
    public openNewWindow: boolean = false;
    public detail: string = "";
    public unclickable: boolean = false;
    public openLinkDirectly: boolean = false;
    public thisCameraCanSee: boolean = false;
    public customData: string = "";
    public isVertical: boolean = false;
    public heightoffset: number = 0;
    public widthoffset: number = 0;
    public followCamera: boolean = false;
    public productId?: string;
    public title?: string;
    public showBox?: boolean;
    public showTitle?: boolean;
    private billboradControls: THREE.Object3D;
    public boundaryBox: THREE.Object3D;

    public underPositioningObject?: boolean;
    public targetHotspotId?: string
    public hotspotAnimation?: boolean
    public azimuthAngle: number;
    public polarAngle: number;
    public componentName?: string
    public floorTargetId?: string
    public componentData?: string
    public moduleId?: string
    // NFT fields 
    public tokenId?: string
    public nftSource?: string

    public glbObjectUrl?: string
    public usdzObjectUrl?: string
    public purchaseInfo?: string
    public maxWidth: number;

    constructor( type )
    {
        super( type )
        this.createBoundaryBox();
    }

    private originRotation: THREE.Quaternion;

    public get targetInfo(): RaycastClickData
    {
        return {
            ...super[ 'targetInfo' ],
            id: this.id,
            type: ( this.openNewWindow ? RaycastType.OPENWINDOW : RaycastType.SHOWMODAL ),
            mediaClass: this,
        }
    }

    public get json(): { [ key: string ]: any }
    {
        return {
            ...super[ 'json' ],
            id: this.id,
            name: this.name,
            title: this.title,
            fileUrl: this.fileUrl,
            openLink: this.openLink,
            thumbnail: this.thumbnail,
            imageHash: this.imageHash,
            linkName: this.linkName,
            openNewWindow: this.openNewWindow,
            detail: this.detail,
            unclickable: this.unclickable,
            openLinkDirectly: this.openLinkDirectly,
            thisCameraCanSee: this.thisCameraCanSee,
            customData: this.customData,
            isVertical: this.isVertical,
            followCamera: this.followCamera,
            productId: this.productId,
            glbObjectUrl: this.glbObjectUrl,
            usdzObjectUrl: this.usdzObjectUrl,
            purchaseInfo: this.purchaseInfo,
            floorTargetId: this.floorTargetId,
            componentName: this.componentName,
            componentData: this.componentData,
            moduleId: this.moduleId,
            targetHotspotId: this.targetHotspotId,
            azimuthAngle: this.azimuthAngle,
            polarAngle: this.polarAngle,
            hotspotAnimation: this.hotspotAnimation,
            showBox: this.showBox,
            showTitle: this.showTitle,
            maxWidth: this.maxWidth,
            tokenId: this.tokenId,
            nftSource: this.nftSource
        }
    }

    public setJson( data: any )
    {
        this.name = data.name;
        this.title = data.title ? data.title : this.name;
        this.fileUrl = data.fileUrl;
        this.openLink = data.openLink;
        this.thumbnail = data.thumbnail;
        this.linkName = data.linkName;
        this.openNewWindow = data.openNewWindow;
        this.detail = data.detail;
        this.unclickable = data.unclickable;
        this.openLinkDirectly = data.openLinkDirectly;
        this.thisCameraCanSee = data.thisCameraCanSee;
        this.moduleId = data.moduleId;
        this.customData = data.customData;
        this.maxWidth = data.maxWidth ?? 1.0;

        this.showBox = data.showBox
        this.showTitle = data.showTitle;

        if ( data.hasOwnProperty( "isVertical" ) )
        {
            if ( this.isVertical != data.isVertical )
            {
                this.isVertical = data.isVertical;
                this.setObjVertical( data.isVertical );
            }

        }
        if ( this.followCamera != data.followCamera )
        {
            this.followCamera = data.followCamera;
            this.updateFollowCameraProperty();
        }
        this.handleCustomizedData( data );
    }

    private handleCustomizedData( data: any )
    {
        this.productId = data.productId ? data.productId : null;
        this.moduleId = data.moduleId;
        this.floorTargetId = data.floorTargetId;
        this.tokenId = data.tokenId;
        this.nftSource = data.nftSource;

        // 跳轉站點tag的設定
        this.targetHotspotId = data.targetHotspotId ? data.targetHotspotId : null;
        this.azimuthAngle = data.azimuthAngle ? data.azimuthAngle : null;
        this.polarAngle = data.polarAngle ? data.polarAngle : null;
        this.hotspotAnimation = data.hotspotAnimation !== undefined ? data.hotspotAnimation : null;
        this.usdzObjectUrl = data.usdzObjectUrl ? data.usdzObjectUrl : null;

        // 圖片或模型時ar物件跟購買資訊
        if ( data.glbObjectUrl !== undefined && data.glbObjectUrl !== '' )
        {
            this.glbObjectUrl = data.glbObjectUrl;
            this.purchaseInfo = data.purchaseInfo;
        }
        // component ui with viewer sdk
        if ( data.componentName !== undefined )
        {
            this.componentName = data.componentName;
            if ( data.componentData )
            {
                this.componentData = data.componentData;
            }
        }
    }

    public init( data: any )
    {
        this.id = data.id;
        this.name = data.name;
        this.title = data.title ? data.title : this.name;
        this.fileUrl = data.fileUrl;
        this.openLink = data.openLink;
        this.thumbnail = data.thumbnail;
        this.imageHash = data.imageHash;
        this.linkName = data.linkName;
        this.openNewWindow = data.openNewWindow;
        this.detail = data.detail;
        this.unclickable = data.unclickable;
        this.openLinkDirectly = data.openLinkDirectly;
        this.thisCameraCanSee = data.thisCameraCanSee;
        this.customData = data.customData;
        this.isVertical = data.isVertical;
        this.followCamera = data.followCamera;
        this.showBox = data.showBox;
        this.showTitle = data.showTitle;
        this.maxWidth = data.maxWidth ?? 1.0;

        this.updateFollowCameraProperty()
        this.handleCustomizedData( data );
    }

    public captureMedia( enable: boolean )
    {
        if ( this.followCamera == false ) return;
        if ( enable )
            this.disableFollowCamera();
        else
            this.enableFollowCamera();
    }

    protected updateFollowCameraProperty(): void
    {
        if ( this.followCamera == true )
        {
            this.enableFollowCamera()
            this.getOriginRotation();
        }
        else
        {
            this.disableFollowCamera();
            this.setOriginRotation();
        }
    }

    private enableFollowCamera(): void
    {
        if ( !this.billboradControls )
        {
            const geometry = new THREE.BoxGeometry( 1e-5, 1e-5, 1e-5 );
            const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
            this.billboradControls = new THREE.Mesh( geometry, material );
            this.billboradControls.frustumCulled = false;
            this.object.add( this.billboradControls )
        }

        const parent = this.object;

        this.billboradControls.onAfterRender = function ( renderer, scene, camera )
        {
            const cameraPos = new THREE.Vector3();
            const objectPos = new THREE.Vector3();
            camera.getWorldPosition( cameraPos );
            parent.getWorldPosition( objectPos );
            const n = new THREE.Vector3().subVectors( cameraPos, objectPos ).normalize();
            n.y = 0;
            const mat4 = new THREE.Matrix4().lookAt( n, new THREE.Vector3( 0, 0, 0 ), camera.up );
            parent.setRotationFromMatrix( mat4 );
        }
    }

    private disableFollowCamera(): void
    {
        if ( this.billboradControls )
            this.billboradControls.onAfterRender = function () { };
    }

    private getOriginRotation()
    {
        this.originRotation = this.object.quaternion.clone();
    }

    private setOriginRotation()
    {
        if ( this.originRotation )
            this.object.quaternion.set( this.originRotation.x, this.originRotation.y, this.originRotation.z, this.originRotation.w );
    }


    onMouseMove( rot4: THREE.Matrix4, pos: THREE.Vector3 ): void { }
    onMouseEnter(): void
    {
        //TODO : hover effect
    };
    onMouseLeave(): void
    {
        //TODO : hover effect
    };
    onMouseUp(): void
    {
        //TODO : show property tab
    };

    setObjVertical( enable: boolean )
    {
        let offsetY = this.heightoffset * this.object.scale.y;
        let offsetZ = this.widthoffset * this.object.scale.z;

        if ( enable )
        {
            if ( this.type === EObjectType.MODEL )
            {
                this.object.rotateX( Math.PI / 2 );
                if ( this.widthoffset > this.heightoffset )
                    this.object.translateZ( -offsetZ );
                else
                    this.object.translateZ( offsetZ );
            }
            else
            {
                this.object.rotateX( Math.PI / 2 );
                this.object.translateY( offsetY );
            }
        }
        else
        {
            if ( this.type === EObjectType.MODEL )
            {
                this.object.rotateX( -Math.PI / 2 );
                if ( this.widthoffset > this.heightoffset )
                    this.object.translateY( -offsetZ );
                else
                    this.object.translateY( offsetZ );
            }
            else
            {
                this.object.rotateX( -Math.PI / 2 );
                this.object.translateZ( -offsetY );
            }
        }
    }

    /**
     * create initial boundary box, default height:1,width:1,depth:1
     */
    protected createBoundaryBox()
    {
        const geometry = new THREE.BoxGeometry( 1, 1, 1 );
        const material = new THREE.MeshBasicMaterial( { transparent: true, opacity: 0 } );
        this.boundaryBox = new THREE.Mesh( geometry, material );
        this.boundaryBox.visible = false;
        this.object.add( this.boundaryBox );
    }

    /**
     * update boundary box size
     * @param width    Boundary Box width, default value 1
     * @param height   Boundary Box height, default value 1
     * @param depth    Boundary Box depth, default value 1
     */
    protected updateBoundaryBox( width: number = 1, height: number = 1, depth: number = 1 )
    {
        const oldGeometry = ( this.boundaryBox as THREE.Mesh ).geometry;
        const newGeometry = new THREE.BoxGeometry( width, height, depth );
        ( this.boundaryBox as THREE.Mesh ).geometry = newGeometry;

        if ( oldGeometry )
            oldGeometry.dispose()

        this.heightoffset = Math.abs( height / 2 );
    }
}
