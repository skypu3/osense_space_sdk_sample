import * as THREE from 'three';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import { Format } from 'core/three/scene/Format';
import Media from 'core/three/mediaObject/media/Media';
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";
import { RaycastClickData } from '../../RaycastObject';
import store from 'core/store/store';
import { ViewerMode } from 'core/store/actions/mode';
const TWEEN = require('@tweenjs/tween.js')

// interface IModelInfo {
//     animations: string[]
// }

export default class Model extends Media {
    public animations: any[];
    public animationList: any[];
    public animationName?: string;
    public animation:any
    public size: THREE.Vector3;
    public center: THREE.Vector3;

    protected glb: THREE.Group | THREE.Object3D;
    private tween: any;

    constructor() {
        super(EObjectType.MODEL);

        this.size = new THREE.Vector3();
        this.center = new THREE.Vector3();
    }

    public get json() {
        return {
            ...super['json'],
            animation: this.animationName,
            animationList: this.animationList,
        }
    }
    public async setJson(data) {
        super.setJson(data)

        if (this.type !== EObjectType.SCENEMODEL) {
            this.animationName = data.animation;
            this.animationList = data.animationList;
        }

        this.playAnimation()
    }

    public async init(data: Format.IModel): Promise<void> {
        super.init(data)

        if (this.type !== EObjectType.SCENEMODEL) {
            this.animationName = data.animation;
            this.animationList = data.animationList;
        }

        await this.updateModel();
        this.playAnimation();
    }

    private autoRotate(mesh) {
        this.tween = new TWEEN.Tween(mesh.rotation)
            .to({ y: "-" + Math.PI / 2 }, 2500) // relative animation
            .onComplete(function () {
                // Check that the full 360 degrees of rotation, 
                // and calculate the remainder of the division to avoid overflow.
                if (Math.abs(mesh.rotation.z) >= 2 * Math.PI) {
                    mesh.rotation.y = mesh.rotation.z % (2 * Math.PI);
                }
            })
            .start();
        this.tween.repeat(Infinity)
    }

    private async playAnimation() {
        if (this.type !== EObjectType.SCENEMODEL && store.getState().modeReducer === ViewerMode.Viewer) {
            if (this.tween != null) {
                this.tween.stop();
                this.tween = null;
            }

            if (this.animationName && this.animationName === 'autoRotate') {
                this.autoRotate(this.object);
            }
            else {
                this.play(this.animationName);
            }
        }
    }

    private async updateModel() {
        await this.load(this.fileUrl)
        this.normalize(this.glb)
        this.normalize(this.raycastMesh)
        this.updateBoundaryBox();
    }

    private normalize(mesh) {
        let size = new THREE.Vector3()
        this.object.remove(mesh);
        let bbox = new THREE.Box3().setFromObject(mesh);
        bbox.getSize(size);
        this.object.add(mesh);

        let maxSize = Math.max(size.x, size.y, size.z)
        mesh.scale.x = this.maxWidth / maxSize
        mesh.scale.y = this.maxWidth / maxSize
        mesh.scale.z = this.maxWidth / maxSize
    }

    protected setMesh(obj) {
        if (this.glb)
            this.object.remove(this.glb)

        this.glb = obj;
        // if (this.type !== EObjectType.SCENEMODEL) {
        //     const postProcessingController = Global.inst.mediaManager.viewportControls.postProcessingController;
        //     this.glb.children.forEach((mesh) => {
        //         postProcessingController.selectiveBloomEffect.selection.add(mesh);
        //         mesh.layers.enable(3)
        //     })
        // }
        this.object.add(this.glb);

        this.updateRaycastMesh(this.glb);

        const bbox = new THREE.Box3().setFromObject(this.object);
        bbox.getSize(this.size);
        bbox.getCenter(this.center);
    }


    public setGLTF(obj: GLTF, callback?: (n: THREE.Group) => void) {
        this.setMesh(obj.scene)

        if (callback)
            callback(obj.scene)
    }

    public async load(url: string, callback?: (n: THREE.Group) => void): Promise<void> {
        const obj: any = await Global.inst.resLoad.load(url, ResourcesType.Model);
        this.animations = obj.animations
        this.setMesh(obj.scene)

        if (callback)
            callback(obj.scene)
    }

    public get targetInfo(): RaycastClickData {
        return {
            ...super['targetInfo'],
            url: this.openLink,
        }
    }

    public play(animationName?: string) {
        clearTimeout(this.animation)
        const mixer = new THREE.AnimationMixer(this.object);
        const clips = this.animations;
        if (clips && clips.length > 0) {
            var clip = clips[0];
            if (animationName != null) {
                const findClip = THREE.AnimationClip.findByName(clips, animationName);
                if (findClip != null) {
                    clip = findClip;
                }
            }
            const action = mixer.clipAction(clip);
            action.play();
            let time = performance.now()
            this.animation = setInterval(() => {
                const now = performance.now()
                const delta = now - time
                time = now
                mixer.update(delta / 1000)
            }, 30)
        }
    }

    public stop() {
        if (this.animation) {
            clearTimeout(this.animation)
        }
    }

    // 清除glb記憶體
    public destroy(): void {
        super.destroy()

        this.stop();
        this.glb.traverse((object: THREE.Mesh) => {

            if (!object.isMesh) return

            console.log('dispose geometry!')
            object.geometry.dispose()

            if ((object.material as any).isMaterial) {
                cleanMaterial(object.material)
            } else {
                // an array of materials
                for (const material of (object as any).material) cleanMaterial(material)
            }
        });
        
        this.object.remove(this.glb)
        this.glb = null
    }

    protected updateBoundaryBox() {
        let size = new THREE.Vector3();
        this.object.remove(this.glb);
        let bbox = new THREE.Box3().setFromObject(this.glb);
        bbox.getSize(size);
        this.object.add(this.glb);

        let maxSize = Math.max(size.x, size.y, size.z)
        const oldGeometry = (this.boundaryBox as THREE.Mesh).geometry;
        const newGeometry = new THREE.BoxGeometry(size.x / maxSize, size.y / maxSize, size.z / maxSize);
        (this.boundaryBox as THREE.Mesh).geometry = newGeometry;

        let center = (bbox.max.y + bbox.min.y) / 2
        this.boundaryBox.position.y = center

        this.heightoffset = Math.abs(bbox.min.y);
        this.widthoffset = Math.abs(bbox.max.z);

        if (oldGeometry)
            oldGeometry.dispose()
    }
}

const cleanMaterial = material => {
    console.log('dispose material!')
    material.dispose()

    // dispose textures
    for (const key of Object.keys(material)) {
        const value = material[key]
        if (value && typeof value === 'object' && 'minFilter' in value) {
            console.log('dispose texture!')
            value.dispose()
        }
    }
}
