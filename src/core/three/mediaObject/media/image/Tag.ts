import * as THREE from 'three';
import { Format } from 'core/three/scene/Format';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Global from 'core/three/base/Global';
import { RaycastClickData } from '../../RaycastObject';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import Media from 'core/three/mediaObject/media/Media';
import TextTexture from 'core/three/TextTexture/TextTexture';

export default class Tag extends Media
{
    private label: string;

    private lineMesh: THREE.Mesh
    private iconSprite: THREE.Sprite
    private textSprite: THREE.Sprite
    private raycastCube: THREE.Mesh
    private textHeight: number;
    private textWidth: number;

    private length: number = 0.5;
    private iconWidth: number = 0.1;
    private showLine: boolean = true;

    private iconTag: THREE.Group;

    constructor()
    {
        super( EObjectType.TAG );

        this.iconTag = new THREE.Group();
        this.iconTag.renderOrder = 10;
        this.object.add( this.iconTag );
        this.createIconTag();

        const geometry = new THREE.BoxBufferGeometry( 1.0, 1.0, 1.0 );
        const material = new THREE.MeshBasicMaterial( { color: 0xffffff } );
        material.opacity = 0;
        this.raycastCube = new THREE.Mesh( geometry, material );
        this.object.add( this.raycastCube );
    }

    async createIconTag()
    {
        const hotspotmaterial = new THREE.SpriteMaterial( { map: null } );
        this.iconSprite = new THREE.Sprite( hotspotmaterial );
        this.iconTag.add( this.iconSprite );

        //create text mesh
        const textmaterial = new THREE.SpriteMaterial( { transparent: true } );
        this.textSprite = new THREE.Sprite( textmaterial );
        this.iconTag.add( this.textSprite );

        // create line mesh, use box instead line because line has width limit
        const lineWidth = 0.01;
        const lineGeo = new THREE.BoxGeometry( lineWidth, 1, lineWidth );
        const lineMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
        this.lineMesh = new THREE.Mesh( lineGeo, lineMaterial );
        this.iconTag.add( this.lineMesh );
    }

    public get json()
    {
        return {
            ...super[ 'json' ],
            extData: {
                length: this.length,
                label: this.label,
            },
            iconWidth: this.iconWidth,
            showLine: this.showLine,
        }
    }

    public async setJson( data: Format.ITag )
    {
        super.setJson( data );

        this.length = data.extData.length;
        this.showLine = data.showLine;
        this.label = data.extData.label;
        //TODO: update showTitle,showLine iconWidth property
        this.iconWidth = data.iconWidth;
        await this.updateIcon();

        this.updateTag()
        this.updateTagRaycast()
    }

    public async init( data: Format.ITag )
    {
        super.init( data )

        this.length = data.extData.length;
        this.showLine = data.showLine;
        this.label = data.extData.label;
        //TODO: update showTitle,showLine iconWidth property
        this.iconWidth = data.iconWidth;
        await this.updateIcon();

        this.updateTag()
        this.updateTagRaycast()
    }

    async updateTagRaycast()
    {
        // store old geometry
        const oldGeometry = ( this.raycastCube as THREE.Mesh ).geometry;

        let boundaryWidth = Math.max( this.iconWidth, this.textWidth );
        let boundaryHeight = this.length + this.iconWidth + this.textHeight

        const material = new THREE.MeshBasicMaterial( { color: 0xffffff } );
        material.opacity = 0
        const geometry = new THREE.BoxBufferGeometry( boundaryWidth, boundaryWidth, boundaryHeight );

        ( this.raycastCube as THREE.Mesh ).geometry = geometry;
        this.raycastCube.position.z = boundaryHeight / 2;

        //dispose old texture and geometry
        if ( oldGeometry )
            oldGeometry.dispose()

        this.updateRaycastMesh( [ this.raycastCube ] )
        this.updateBoundaryBox( boundaryWidth, boundaryWidth, boundaryHeight );
        this.boundaryBox.position.z = boundaryHeight / 2;
    }

    async updateIcon()
    {
        let texture = await Global.inst.resLoad.load( this.fileUrl, ResourcesType.Texture )

        // store old texture and geometry
        const oldTexture = ( ( this.iconSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map;
        //set new geometry and texture
        ( ( this.iconSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map = texture;

        //dispose old texture 
        if ( oldTexture )
            oldTexture.dispose()
    }

    private updateIconScale(): void
    {
        this.iconSprite.scale.set( this.iconWidth, this.iconWidth, this.iconWidth );
    }

    private updateLabel(): void
    {
        // create a canvas element
        const textTexture = new TextTexture( {
            labels: this.name,
            fontWeight: "Normal",
            fontStyle: "Normal",
            fontColor: "#ffffff",
            padding: 15,
            fontSize: 40,
            aligment: "center",
            lineSpace: 0
        }, {
            fillStyle: "rgba(0,0,0,0.5)"
        } );

        textTexture.needsUpdate = true;
        textTexture.encoding = THREE.GammaEncoding;

        let sizeX = textTexture.width * 4;
        let sizeY = textTexture.height * 4;

        const oldTexture = ( ( this.textSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map;

        //set new geometry and texture
        ( ( this.textSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map = textTexture;
        this.textSprite.scale.set( sizeX, sizeY, 1 );
        this.textWidth = sizeX
        this.textHeight = sizeY

        //dispose old texture 
        if ( oldTexture )
            oldTexture.dispose()
    }

    private updateTag(): void
    {
        this.iconTag.onBeforeRender = function () { };

        if ( this.showTitle )
        {
            const self = this;
            this.textSprite.onBeforeRender = function () 
            {
                //update text position
                let iconworldPos = new THREE.Vector3();
                let textworldPos = new THREE.Vector3();
                self.iconSprite.getWorldPosition( iconworldPos );
                textworldPos = iconworldPos.clone().add( new THREE.Vector3( 0, self.iconWidth / 2 + self.textHeight / 2, 0 ) );

                let textlocalPos = self.iconTag.worldToLocal( textworldPos );
                self.textSprite.position.set( textlocalPos.x, textlocalPos.y, textlocalPos.z );
            }
            this.textSprite.visible = true;
        }
        else
        {
            this.textSprite.onBeforeRender = function () { };
            this.textSprite.visible = false;
        }


        if ( this.showLine )
            this.lineMesh.visible = true;
        else
            this.lineMesh.visible = false;

        this.updateLabel()

        //update tag scale
        this.updateIconScale()
        this.lineMesh.scale.setY( this.length );

        //update tag position
        this.lineMesh.position.setY( this.length / 2 );
        this.iconSprite.position.setY( this.length + this.iconWidth / 2 );

        this.iconTag.rotation.set( Math.PI / 2, 0, 0 );
    }

    public get targetInfo(): RaycastClickData
    {
        return {
            ...super[ 'targetInfo' ],
            url: this.openLink,
        }
    }
}
