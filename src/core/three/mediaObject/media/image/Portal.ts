import * as THREE from 'three';
import { Format } from 'core/three/scene/Format';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import Media from 'core/three/mediaObject/media/Media'
import { RaycastClickData, RaycastType } from '../../RaycastObject';

export default class Portal extends Media
{
    constructor()
    {
        super( EObjectType.PORTAL );
    }

    public get json(): object
    {
        return {
            ...super[ 'json' ],
            openLink: this.openLink,
        }
    }

    public setJson( value: Format.IImage )
    {
        super.setJson( value )
    }

    public async init( data: Format.IImage )
    {
        super.init( data );
        await this.load( this.fileUrl, this.maxWidth )
    }

    private async load( srcUrl: string, maxWidth: number ): Promise<void>
    {
        const texture = await Global.inst.resLoad.load( srcUrl, ResourcesType.Texture )
        texture.needsUpdate = true;
        const geometry = new THREE.PlaneBufferGeometry( 1.0 * maxWidth, ( texture.image.height / texture.image.width ) * maxWidth, 1, 1 );
        const material = new THREE.MeshBasicMaterial( {
            map: texture,
            side: THREE.DoubleSide,
            transparent: true,
            combine: 0
        } );
        const mesh = new THREE.Mesh( geometry, material );
        this.object.add( mesh );
        this.updateRaycastMesh( geometry )
        this.updateBoundaryBox( 1.0 * maxWidth, ( texture.image.height / texture.image.width ) * maxWidth, 0.001 );
    }

    public get targetInfo(): RaycastClickData
    {
        return {
            ...super[ 'targetInfo' ],
            type: RaycastType.REDIRECT,
            url: this.openLink
        }
    }
}