import * as THREE from 'three';
import { Format } from 'core/three/scene/Format';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import Media from 'core/three/mediaObject/media/Media'
import { RaycastClickData } from '../../RaycastObject';
import TextTexture from 'core/three/TextTexture/TextTexture';
import { Material, Vector3 } from 'three';
import { SpriteGifLoader } from '../../customLoader/SpriteGifLoader';

const TWEEN = require('@tweenjs/tween.js')
const scaleScalar = 0.3;

export enum IconType {
    PHOTO = "photo",
    SPRITE = "sprite",
}

export default class NewTag extends Media {
    private imageMesh: THREE.Mesh;
    private imgMaterial: Material;
    // Sprite tag
    private verticalNumber: number;
    private horizontalNumber: number;
    private frameNumber: number;
    private speed: number;
    public loader: SpriteGifLoader;

    private textMesh: THREE.Mesh;
    private fontSize: number;
    private animation: string = '';
    private text?: string;
    private texts?: string[];
    private iconType: string;
    private tweens: any[] = [];
    private timeouts: any[] = [];

    constructor() {
        super(EObjectType.TAG);

        //create image Tag
        const imgGeometry = new THREE.PlaneBufferGeometry(1.0, 1.0, 1, 1);
        this.imgMaterial = new THREE.MeshBasicMaterial({ map: null, side: THREE.DoubleSide, transparent: true, combine: 0 });
        this.imageMesh = new THREE.Mesh(imgGeometry, this.imgMaterial);
        this.object.add(this.imageMesh)

        //create text mesh Tag
        const textGeometry = new THREE.PlaneBufferGeometry(1.0, 1.0, 1, 1);
        const textMaterial = new THREE.MeshBasicMaterial({ transparent: true });
        this.textMesh = new THREE.Mesh(textGeometry, textMaterial);
        this.textMesh.position.y = 0.6;
        this.textMesh.position.z = 0.1;
        this.textMesh.visible = false;
        this.object.add(this.textMesh);

        this.updateRaycastMesh([imgGeometry])
    }

    /**
     * Fade in and out animation
     * @returns 
     */
    private fadeOut() {
        const scope = this;
        let option = { 'fade': 1.0 };
        return new TWEEN.Tween(option)
            .to({ 'fade': 0.3 }, 1700)
            .easing(TWEEN.Easing.Cubic.Out)
            .onUpdate(function () {
                scope.imgMaterial.opacity = option.fade;
            }).onComplete(function () {
                const tween = scope.fadeIn();
                tween.start()
                scope.tweens.push(tween);
            });
    };

    private fadeIn() {
        const scope = this;
        let option = { 'fade': 0.3 };
        return new TWEEN.Tween(option)
            .to({ 'fade': 1.0 }, 2000)
            .easing(TWEEN.Easing.Cubic.In)
            .onUpdate(function () {
                scope.imgMaterial.opacity = option.fade;
            }).onComplete(function () {
                scope.timeouts.push(window.setTimeout((() => {
                    const tween = scope.fadeOut();
                    tween.start()
                    scope.tweens.push(tween);
                }), 3500));
            });
    };

    /**
     * Scale in and out animation
     * @returns 
     */
    private scaleUp() {
        const scope = this;
        let scale = new Vector3(1.0, 1.0, 1.0);
        return new TWEEN.Tween(scale)
            .to(scale.addScalar(scaleScalar), 500)
            .easing(TWEEN.Easing.Cubic.In)
            .onUpdate(function () {
                scope.imageMesh.scale.copy(scale);
            }).onComplete(function () {
                const tween = scope.scaleDown();
                tween.start()
                scope.tweens.push(tween);
            });
    };
    private scaleDown() {
        const scope = this;
        let scale = new Vector3(1.0, 1.0, 1.0).addScalar(scaleScalar);
        return new TWEEN.Tween(scale)
            .to(scale.addScalar(-scaleScalar), 500)
            .easing(TWEEN.Easing.Cubic.Out)
            .onUpdate(function () {
                scope.imageMesh.scale.copy(scale);
            }).onComplete(function () {
                const tween = scope.scaleUp();
                tween.start()
                scope.tweens.push(tween);
            });
    };

    //.......................................

    public get json() {
        return {
            ...super['json'],
            animation: this.animation,
            verticalNumber: this.verticalNumber,
            horizontalNumber: this.horizontalNumber,
            frameNumber: this.frameNumber,
            speed: this.speed,
            text: this.text,
            texts: this.texts,
            fontSize: this.fontSize,
            iconType: this.iconType
        }
    }

    public async init(data: Format.INewTag) {
        super.init(data)

        this.name = data.name ? data.name : '';
        this.verticalNumber = data.verticalNumber
        this.horizontalNumber = data.horizontalNumber
        this.frameNumber = data.frameNumber ? data.frameNumber : 15
        this.speed = data.speed ? data.speed : 10
        this.text = data.text ? data.text : null;
        this.texts = data.texts ? data.texts : null;
        this.iconType = data.iconType ? data.iconType : 'PHOTO';
        this.fontSize = data.fontSize ? data.fontSize : 58;

        this.animation = data.animation ? data.animation : '';
        await this.updateImage()
        this.updateLabelText()
        this.playAnimation()
    }

    public playAnimation() {
        this.stopAnimation()
        if (this.animation !== '' && this.iconType !== IconType.SPRITE) {
            if (this.animation === 'scale') {
                const tween = this.scaleUp();
                tween.start()
                this.tweens.push(tween)
            }
            else if (this.animation === 'fade') {
                const tween = this.fadeOut();
                tween.start()
                this.tweens.push(tween)
            }
        }
    }

    private stopAnimation() {
        if (this.tweens.length > 0) {
            this.tweens.forEach((tween) => { tween.stop() })
            for (var i = 0; i < this.timeouts.length; i++) {
                clearTimeout(this.timeouts[i]);
            }

            this.tweens = []
            this.timeouts = []
        }
    }

    public get targetInfo(): RaycastClickData {
        return {
            ...super['targetInfo'],
            url: this.openLink,
        }
    }

    public setJson(data: Format.INewTag) {
        super.setJson(data);
        this.name = data.name ? data.name : '';
        this.verticalNumber = data.verticalNumber
        this.horizontalNumber = data.horizontalNumber
        this.frameNumber = data.frameNumber ? data.frameNumber : 15
        this.speed = data.speed ? data.speed : 10
        this.text = data.text ? data.text : null;
        this.texts = data.texts ? data.texts : null;
        this.iconType = data.iconType ? data.iconType : 'PHOTO';
        this.fontSize = data.fontSize ? data.fontSize : 58;

        this.animation = data.animation ? data.animation : '';
        this.updateLabelText()
        this.playAnimation()
    }

    async updateImage() {
        const resourcesMgr = Global.inst.resLoad

        const oldTexture = ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).map;
        // dispose old texture and geometry
        if (oldTexture)
            oldTexture.dispose()

        if (resourcesMgr.mediaResources[this.fileUrl] !== undefined) {    
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).map = resourcesMgr.mediaResources[this.fileUrl];
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).needsUpdate = true;
            return;
        }

        const texture = await Global.inst.resLoad.load(this.fileUrl, ResourcesType.Texture)
        texture.needsUpdate = true;

        if (this.iconType == IconType.SPRITE) {
            this.loader = new SpriteGifLoader(texture, this.horizontalNumber, this.verticalNumber, this.frameNumber, this.speed)
            const animatedTexture = this.loader.init();
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).map = animatedTexture;
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).needsUpdate = true;
            resourcesMgr.mediaResources[this.fileUrl] = animatedTexture;
        } else {
            //set new geometry and texture
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).map = texture;
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).needsUpdate = true;
            resourcesMgr.mediaResources[this.fileUrl] = texture;

            // const rayCastGeo = new THREE.PlaneBufferGeometry(2.5 * this.maxWidth, 2.5 * (texture.image.height / texture.image.width) * this.maxWidth, 1, 1);
            // this.updateRaycastMesh([rayCastGeo])
            // this.updateBoundaryBox(1.0 * this.maxWidth, (texture.image.height / texture.image.width) * this.maxWidth, 0.001);
        }
    }

    public enableLabels(enabled: boolean) {
        if (this.text !== null || this.texts !== null) {
            this.textMesh.visible = enabled;
        }
    }

    private updateLabelText(): void {
        if (this.text !== null || this.texts !== null) {
            // create a canvas element
            const textTextTure = new TextTexture({
                labels: this.texts ? this.texts : this.text,
                fontWeight: "Bold",
                fontStyle: "Normal",
                fontColor: "#ffffff",
                padding: 15,
                fontSize: this.fontSize,
                aligment: "center",
                lineSpace: 20
            }, {
                fillStyle: "rgba(0,0,0,0.5)"
            });

            textTextTure.needsUpdate = true;
            textTextTure.encoding = THREE.GammaEncoding;

            let sizeX = textTextTure.width * 4;
            let sizeY = textTextTure.height * 4;

            const oldTexture = ((this.textMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).map;

            //set new geometry and texture
            ((this.textMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).map = textTextTure;
            this.textMesh.scale.set(sizeX, sizeY, 1);

            //dispose old texture 
            if (oldTexture)
                oldTexture.dispose()
        } else
            this.textMesh.visible = false;
    }
}