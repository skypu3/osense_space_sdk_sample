import * as THREE from 'three';
import { Format } from 'core/three/scene/Format';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import Media from 'core/three/mediaObject/media/Media'
import { RaycastClickData } from '../../RaycastObject';
import TextTexture from 'core/three/TextTexture/TextTexture';
import { Material, Vector3 } from 'three';
import store from 'core/store/store';
import ThreeView from 'app/three/ThreeView';

const TWEEN = require( '@tweenjs/tween.js' )
const scaleScalar = 0.3;

export default class Image extends Media
{
    private imageMesh: THREE.Mesh;
    private imgMaterial: Material;

    // private showHotspotTag: boolean = false;
    // private hotspotIconIndex: number = 0;
    // private length: number = 0.5;
    // private HotspotTagWidth: number = 0.2;
    // private showTitle: boolean = false;
    // private showLine: boolean = true;

    //hotspot Mesh
    // private hotspotTag: THREE.Group;
    // private hotspotIconSprite: THREE.Sprite;
    // private lineMesh: THREE.Mesh;
    // private textSprite: THREE.Sprite;
    // private textHeight: number = 0.5;

    private animation: string = '';
    private isThickness: boolean = false;

    constructor()
    {
        super( EObjectType.PHOTO );

        if ( !this.isThickness )
        {
            const imgGeometry = new THREE.PlaneBufferGeometry( 1.0, 1.0, 1, 1 );
            this.imgMaterial = new THREE.MeshBasicMaterial( { map: null, side: THREE.DoubleSide, transparent: true, combine: 0 } );
            this.imageMesh = new THREE.Mesh( imgGeometry, this.imgMaterial );
            this.object.add( this.imageMesh )
            this.updateRaycastMesh( [ imgGeometry ] )
        }

        //create Hotspot Tag
        // this.hotspotTag = new THREE.Group();
        // this.hotspotTag.renderOrder = 10;
        // this.object.add( this.hotspotTag );
        // this.createHotspotTag();
    }

    /**
     * Fade in and out animation
     * @returns 
     */
    private FadeOut()
    {
        const scope = this;
        let option = { 'fade': 1.0 };
        return new TWEEN.Tween( option )
            .to( { 'fade': 0.3 }, 1700 )
            .easing( TWEEN.Easing.Cubic.Out )
            .onUpdate( function ()
            {
                scope.imgMaterial.opacity = option.fade;
            } ).onComplete( function ()
            {
                scope.FadeIn().start();
            } );
    };

    private FadeIn()
    {
        const scope = this;
        let option = { 'fade': 0.3 };
        return new TWEEN.Tween( option )
            .to( { 'fade': 1.0 }, 1700 )
            .easing( TWEEN.Easing.Cubic.In )
            .onUpdate( function ()
            {
                scope.imgMaterial.opacity = option.fade;
            } ).onComplete( function ()
            {
                scope.FadeOut().start();
            } );
    };

    /**
     * Scale in and out animation
     * @returns 
     */
    private scaleUp()
    {
        const scope = this;
        let scale = new Vector3( 1.0, 1.0, 1.0 );
        return new TWEEN.Tween( scale )
            .to( scale.addScalar( scaleScalar ), 500 )
            .easing( TWEEN.Easing.Cubic.In )
            .onUpdate( function ()
            {
                scope.imageMesh.scale.copy( scale );
            } ).onComplete( function ()
            {
                scope.scaleDown().start();
            } );
    };
    private scaleDown()
    {
        const scope = this;
        let scale = new Vector3( 1.0, 1.0, 1.0 ).addScalar( scaleScalar );
        return new TWEEN.Tween( scale )
            .to( scale.addScalar( -scaleScalar ), 500 )
            .easing( TWEEN.Easing.Cubic.Out )
            .onUpdate( function ()
            {
                scope.imageMesh.scale.copy( scale );
            } ).onComplete( function ()
            {
                scope.scaleUp().start();
            } );
    };

    //.......................................

    public get json()
    {
        return {
            ...super[ 'json' ],
            isThickness: this.isThickness,
            // showHotspotTag: this.showHotspotTag,
            // hotspotIconIndex: this.hotspotIconIndex,
            // length: this.length,
            // HotspotTagWidth: this.HotspotTagWidth,
            // showTitle: this.showTitle,
            // showLine: this.showLine,
            animation: this.animation,
        }
    }

    public async init( data: Format.IImage )
    {
        super.init( data )
        this.name = data.name ? data.name : '';
        this.isThickness = data.isThickness ?? true;
        //TODO: update image hotspot tag 
        // this.showHotspotTag = data.showHotspotTag ? data.showHotspotTag : false;
        // this.hotspotIconIndex = data.hotspotIconIndex ? data.hotspotIconIndex : 0;
        // this.length = data.length ? data.length : 0.5;
        // this.HotspotTagWidth = data.HotspotTagWidth ? data.HotspotTagWidth : 0.1;
        // this.showTitle = data.showTitle ? data.showTitle : false;
        // this.showLine = data.showLine ? data.showLine : true;

        this.animation = data.animation ? data.animation : '';
        await this.updateImage();
    }

    public playAnimation()
    {
        if ( this.animation !== '' )
        {
            if ( this.animation === 'scale' )
                this.scaleUp().start();
            else if ( this.animation === 'fade' )
                this.FadeOut().start();
        }
    }

    public get targetInfo(): RaycastClickData
    {
        return {
            ...super[ 'targetInfo' ],
            url: this.openLink,
        }
    }

    public setJson( data: Format.IImage )
    {
        super.setJson( data );
        this.maxWidth = data.maxWidth;

        //TODO: update image hotspot tag 
        // this.showHotspotTag = data.showHotspotTag;
        // this.hotspotIconIndex = data.hotspotIconIndex ? data.hotspotIconIndex : 0;
        // this.length = data.length ? data.length : 0.5;
        // this.HotspotTagWidth = data.HotspotTagWidth ? data.HotspotTagWidth : 0.1;
        // this.showTitle = data.showTitle ? data.showTitle : false;
        // this.showLine = data.showLine ? data.showLine : true;
        this.animation = data.animation ? data.animation : '';
        this.isThickness = data.isThickness ?? true;

        this.updateImage()
        // this.updateHotspot();
    }

    async updateImage()
    {
        if ( this.imageMesh )
        {
            const oldGeometry = ( this.imageMesh as THREE.Mesh ).geometry;
            const material = ( this.imageMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial;

            if ( material && material.map && material.map.dispose )
            {
                material.dispose()
            }
            else
            {
                const materials = ( this.imageMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial[];
                if ( materials && materials.length > 0 )
                {
                    materials.forEach( ( material ) => material.dispose() );
                }
            }
            if ( oldGeometry )
                oldGeometry.dispose()

            this.object.remove( this.imageMesh );
            this.imageMesh = null;
        }

        const texture = await Global.inst.resLoad.load( this.fileUrl, ResourcesType.Texture )
        texture.needsUpdate = true;

        if ( this.isThickness )
        {
            const threeView = store.getState().threeViewReducer as ThreeView

            // hardcode
            if ( this.id === 'cc37f020-a372-11ec-8de0-ab92a34d08f9' )
            {
                this.imgMaterial = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide, transparent: true, combine: 0, color: 0x515254 } );
            }
            else if ( threeView && threeView.viewportControls.bloomController != null )
            {
                this.imgMaterial = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide, transparent: true, combine: 0, color: 0x939596 } );
            } else
            {
                this.imgMaterial = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide, transparent: true, combine: 0 } );
            }

            var materials = [
                new THREE.MeshBasicMaterial( { color: 0x000000 } ),
                new THREE.MeshBasicMaterial( { color: 0x000000 } ),
                new THREE.MeshBasicMaterial( { color: 0x000000 } ),
                new THREE.MeshBasicMaterial( { color: 0x000000 } ),
                this.imgMaterial,
                new THREE.MeshBasicMaterial( { color: 0x000000 } ),
            ];
            const imgGeometry = new THREE.BoxBufferGeometry( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 0.03, 1 );
            this.imageMesh = new THREE.Mesh( imgGeometry, materials );
            this.object.add( this.imageMesh )
            this.imageMesh.layers.enable( 3 )
            this.updateRaycastMesh( [ imgGeometry ] )
            this.updateBoundaryBox( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 0.03 );
        }
        else
        {
            const material = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide, transparent: true, combine: 0 } );
            const newGeometry = new THREE.PlaneBufferGeometry( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 1, 1 );
            this.imageMesh = new THREE.Mesh( newGeometry, material );
            this.object.add( this.imageMesh )
            this.imageMesh.layers.enable( 3 )
            this.updateRaycastMesh( [ newGeometry ] )
            this.updateBoundaryBox( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 0.001 );
        }
    }

    // public get gethotspotTag()
    // {
    //     return this.hotspotTag;
    // }

    // async createHotspotTag()
    // {
    //     //create hotspotIcon
    //     let texture = await Global.inst.resLoad.load( HotspotIconPath[ this.hotspotIconIndex ], ResourcesType.Texture )
    //     texture.needsUpdate = true;
    //     texture.encoding = THREE.GammaEncoding;
    //     const hotspotmaterial = new THREE.SpriteMaterial( { map: texture } );
    //     this.hotspotIconSprite = new THREE.Sprite( hotspotmaterial );
    //     this.hotspotTag.add( this.hotspotIconSprite );

    //     //create text mesh
    //     const textmaterial = new THREE.SpriteMaterial( { transparent: true } );
    //     this.textSprite = new THREE.Sprite( textmaterial );
    //     this.hotspotTag.add( this.textSprite );

    //     // create line mesh, use box instead line because line has width limit
    //     const lineWidth = 0.01;
    //     const lineGeo = new THREE.BoxGeometry( lineWidth, 1, lineWidth );
    //     const lineMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff } );
    //     this.lineMesh = new THREE.Mesh( lineGeo, lineMaterial );
    //     this.hotspotTag.add( this.lineMesh );

    //     this.updateHotspot();
    // }

    // private updateHotspot()
    // {
    //     if ( this.showHotspotTag )
    //         this.hotspotTag.visible = true;
    //     else
    //         this.hotspotTag.visible = false;
    //     this.hotspotTag.onBeforeRender = function () { };

    //     if ( this.showTitle )
    //     {
    //         this.textSprite.onBeforeRender = function ()
    //         {
    //             //update text position
    //             let iconworldPos = new THREE.Vector3();
    //             let textworldPos = new THREE.Vector3();
    //             this.hotspotIconSprite.getWorldPosition( iconworldPos );
    //             textworldPos = iconworldPos.clone().add( new THREE.Vector3( 0, this.HotspotTagWidth / 2 + this.textHeight / 2, 0 ) );

    //             let textlocalPos = this.hotspotTag.worldToLocal( textworldPos );
    //             this.textSprite.position.set( textlocalPos.x, textlocalPos.y, textlocalPos.z );
    //         }.bind( this );
    //         this.textSprite.visible = true;
    //     }
    //     else
    //     {
    //         this.textSprite.onBeforeRender = function () { };
    //         this.textSprite.visible = false;
    //     }


    //     if ( this.showLine )
    //         this.lineMesh.visible = true;
    //     else
    //         this.lineMesh.visible = false;

    //     this.updateHotspotIcon();
    //     this.updateLabelText();

    //     //update tag scale
    //     this.hotspotIconSprite.scale.set( this.HotspotTagWidth, this.HotspotTagWidth, this.HotspotTagWidth );
    //     this.lineMesh.scale.setY( this.length );

    //     //update tag position
    //     this.lineMesh.position.setY( this.length / 2 );
    //     this.hotspotIconSprite.position.setY( this.length + this.HotspotTagWidth / 2 );

    //     this.hotspotTag.rotation.set( Math.PI / 2, 0, 0 );
    // }

    // async updateHotspotIcon()
    // {
    //     //load texture
    //     let texture = await Global.inst.resLoad.load( HotspotIconPath[ this.hotspotIconIndex ], ResourcesType.Texture )

    //     // store old texture and geometry
    //     const oldTexture = ( ( this.hotspotIconSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map;
    //     //set new geometry and texture
    //     ( ( this.hotspotIconSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map = texture;

    //     //dispose old texture 
    //     if ( oldTexture )
    //         oldTexture.dispose()
    // }

    // private updateLabelText(): void
    // {
    //     // create a canvas element
    //     const Texttexture = new TextTexture( {
    //         labels: this.name,
    //         fontWeight: "Normal",
    //         fontStyle: "Normal",
    //         fontColor: "#ffffff",
    //         padding: 15,
    //         fontSize: 40,
    //         aligment: "center",
    //         lineSpace: 0
    //     }, {
    //         fillStyle: "rgba(0,0,0,0.5)"
    //     } );

    //     Texttexture.needsUpdate = true;
    //     Texttexture.encoding = THREE.GammaEncoding;

    //     let sizeX = Texttexture.width * 4;
    //     let sizeY = Texttexture.height * 4;

    //     const oldTexture = ( ( this.textSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map;

    //     //set new geometry and texture
    //     ( ( this.textSprite as THREE.Sprite ).material as THREE.SpriteMaterial ).map = Texttexture;
    //     this.textSprite.scale.set( sizeX, sizeY, 1 );
    //     this.textHeight = sizeY

    //     //dispose old texture 
    //     if ( oldTexture )
    //         oldTexture.dispose()
    // }
}