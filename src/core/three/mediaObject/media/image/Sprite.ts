import * as THREE from 'three';
import { Format } from 'core/three/scene/Format';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Media from 'core/three/mediaObject/media/Media'
import { RaycastClickData } from '../../RaycastObject';
import { SpriteGifLoader } from '../../customLoader/SpriteGifLoader';
// import { MeshBasicMaterial, Texture } from 'three';
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import { Material } from 'three';
import TextTexture from 'core/three/TextTexture/TextTexture';

export default class Sprite extends Media
{
    private imageMesh: THREE.Mesh;
    private imgMaterial: Material;
    private verticalNumber: number;
    private horizontalNumber: number;
    private frameNumber: number;
    private speed: number;
    private textMesh: THREE.Mesh;
    private fontSize: number;

    private text?: string;
    private texts?: string[];

    public loader: SpriteGifLoader;

    constructor()
    {
        super( EObjectType.SPRITE );

        const imgGeometry = new THREE.PlaneBufferGeometry( 1.0, 1.0, 1, 1 );
        this.imgMaterial = new THREE.MeshBasicMaterial( { map: null, side: THREE.DoubleSide, transparent: true, combine: 0 } );
        this.imageMesh = new THREE.Mesh( imgGeometry, this.imgMaterial );
        this.object.add( this.imageMesh )

        const textGeometry = new THREE.PlaneBufferGeometry( 1.0, 1.0, 1, 1 );
        const textMaterial = new THREE.MeshBasicMaterial( { transparent: true } );
        this.textMesh = new THREE.Mesh( textGeometry, textMaterial );
        this.textMesh.position.y = 0.6;
        this.textMesh.position.z = 0.1;
        this.textMesh.visible = false;

        this.object.add( this.textMesh );

        this.updateRaycastMesh( [ imgGeometry, textGeometry ] )
    }

    public get json()
    {
        return {
            verticalNumber: this.verticalNumber,
            horizontalNumber: this.horizontalNumber,
            frameNumber: this.frameNumber,
            speed: this.speed,
            text: this.text,
            texts: this.texts,
            fontSize: this.fontSize,
            ...super[ 'json' ],
        }
    }

    public async init( data: Format.ISprite ): Promise<void>
    {
        this.verticalNumber = data.verticalNumber
        this.horizontalNumber = data.horizontalNumber
        this.frameNumber = data.frameNumber ? data.frameNumber : 15
        this.speed = data.speed ? data.speed : 10
        this.text = data.text ? data.text : null;
        this.texts = data.texts ? data.texts : null;
        this.fontSize = data.fontSize ? data.fontSize : 58;

        super.init( data )
        await this.updateSpriteImage()

        this.updateLabelText()
    }

    /**
    * Init a Sprite object
    * @param {string} fileURL - file url of image
    * @param {number} verticalNumber - number of columns in your sprite image
    * @param {number} horizontalNumber - number of rows in your sprite image
    * @param {number} frameNumber - number of frames in your sprite image
    * @param {number} speed - number of frames per second, for example 15
    */
    public async initObject(
        fileURL: string,
        verticalNumber: number,
        horizontalNumber: number,
        frameNumber: number,
        speed: number
    ) {
        this.fileUrl = fileURL;
        this.verticalNumber = verticalNumber;
        this.horizontalNumber = horizontalNumber;
        this.frameNumber = frameNumber;
        this.speed = speed;
        await this.updateSpriteImage()
    }

    public enableLabels( enabled: boolean )
    {
        if ( this.text !== null || this.texts !== null )
        {
            this.textMesh.visible = enabled;
        }
    }

    public get targetInfo(): RaycastClickData
    {
        return {
            ...super[ 'targetInfo' ],
            url: this.openLink,
        }
    }

    public setJson( data: Format.ISprite )
    {
        this.verticalNumber = data.verticalNumber
        this.horizontalNumber = data.horizontalNumber
        this.frameNumber = data.frameNumber ? data.frameNumber : 15
        this.speed = data.speed ? data.speed : 10
        this.text = data.text ? data.text : null;
        this.texts = data.texts ? data.texts : null;
        this.fontSize = data.fontSize ? data.fontSize : 70;

        super.setJson( data );
    }

    async updateSpriteImage()
    {
        const resourcesMgr = Global.inst.resLoad;
        if (resourcesMgr.mediaResources[this.fileUrl] !== undefined)
        {    
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).map = resourcesMgr.mediaResources[this.fileUrl];
            ((this.imageMesh as THREE.Mesh).material as THREE.MeshBasicMaterial).needsUpdate = true;
            return;
        }

        const texture = await resourcesMgr.load( this.fileUrl, ResourcesType.Texture )
        texture.needsUpdate = true;

        this.loader = new SpriteGifLoader( texture, this.horizontalNumber, this.verticalNumber, this.frameNumber, this.speed )
        const animatedTexture = this.loader.init();

        // store old texture and geometry
        const oldTexture = ( ( this.imageMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial ).map;
        // const oldGeometry = ( this.imageMesh as THREE.Mesh ).geometry;

        // //set new geometry and texture
        ( ( this.imageMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial ).map = animatedTexture;
        ( ( this.imageMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial ).needsUpdate = true;
        resourcesMgr.mediaResources[this.fileUrl] = animatedTexture;

        // const newGeometry = new THREE.PlaneBufferGeometry( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 1, 1 );
        // ( this.imageMesh as THREE.Mesh ).geometry = newGeometry;

        //dispose old texture and geometry
        if ( oldTexture )
            oldTexture.dispose()
    }

    private updateLabelText(): void
    {
        if ( this.text !== null || this.texts !== null )
        {
            // create a canvas element
            const textTextTure = new TextTexture( {
                labels: this.texts ? this.texts : this.text,
                fontWeight: "Bold",
                fontStyle: "Normal",
                fontColor: "#ffffff",
                padding: 15,
                fontSize: this.fontSize,
                aligment: "center",
                lineSpace: 20
            }, {
                fillStyle: "rgba(0,0,0,0.5)"
            } );

            textTextTure.needsUpdate = true;
            textTextTure.encoding = THREE.GammaEncoding;

            let sizeX = textTextTure.width * 4;
            let sizeY = textTextTure.height * 4;

            const oldTexture = ( ( this.textMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial ).map;

            //set new geometry and texture
            ( ( this.textMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial ).map = textTextTure;
            this.textMesh.scale.set( sizeX, sizeY, 1 );

            //dispose old texture 
            if ( oldTexture )
                oldTexture.dispose()
        } else
            this.textMesh.visible = false;
    }
}
