import * as THREE from 'three';
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { Format } from 'core/three/scene/Format';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Media from 'core/three/mediaObject/media/Media'
import { RaycastClickData } from '../../RaycastObject';

export default class Gif extends Media
{
    private css3dObject: CSS3DObject;
    private gifMesh: THREE.Mesh;
    private imgElement: HTMLImageElement;

    constructor()
    {
        super( EObjectType.GIF );

        this.imgElement = document.createElement( 'img' );
        this.css3dObject = new CSS3DObject( this.imgElement );

        // const imgGeometry = new THREE.PlaneBufferGeometry( 1.0, 1.0, 1, 1 );
        // const imgMaterial = new THREE.MeshBasicMaterial( { map: null, side: THREE.DoubleSide, transparent: true } );
        // this.gifMesh = new THREE.Mesh( imgGeometry, imgMaterial );
        // this.gifMesh.add( this.css3dObject );
        // this.object.add( this.gifMesh )
        // this.object.up.copy( new THREE.Vector3( 0, 0, 1 ) );
    }

    public get json()
    {
        return {
            ...super[ 'json' ],
        }
    }

    public async init( data: Format.IGif ): Promise<void>
    {
        super.init( data )
        await this.updateGif();
    }

    onload2promise( element )
    {
        return new Promise( ( resolve, reject ) =>
        {
            element.onload = () => resolve( element );
            element.onerror = reject;
        } );
    }

    public get targetInfo(): RaycastClickData
    {
        return {
            ...super[ 'targetInfo' ],
            url: this.openLink,
        }
    }

    public setJson( data: Format.IGif )
    {
        super.setJson( data );
        //this.updateGif();
    }

    async updateGif()
    {
        this.imgElement.src = this.fileUrl;
        const gifpromise = this.onload2promise( this.imgElement );
        await gifpromise;

        const width = this.imgElement.naturalWidth;
        const height = this.imgElement.naturalHeight
        const ratio = height / width;

        this.imgElement.style.width = width + 'px';
        this.imgElement.style.height = height + 'px';
        this.css3dObject.scale.set( 1 / width, ( 1 / height ) * ratio, 1 );

        const imgGeometry = new THREE.PlaneBufferGeometry( 1, ratio, 1, 1);
        const material = new THREE.MeshBasicMaterial( {
            opacity: 0,
            color: new THREE.Color( 0x000000 ),
            blending: THREE.NoBlending,
            side: THREE.DoubleSide,
            transparent: true,
        } );

        // const materials = [
        //     new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
        //     new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
        //     new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
        //     new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
        //     material,
        //     new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
        // ];

        this.gifMesh = new THREE.Mesh( imgGeometry, material );
        this.gifMesh.add( this.css3dObject );
        this.object.add( this.gifMesh )
        this.object.up.copy( new THREE.Vector3( 0, 0, 1 ) );

        this.updateRaycastMesh( [ imgGeometry ] );
        this.updateBoundaryBox( 1, ratio, 0.05 );
    }

    public destroy()
    {
        super.destroy();
        if ( this.gifMesh )
        {
            const materials = ( this.gifMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial[];
            if ( materials && materials.length > 0 )
            {
                materials.forEach( ( material ) => material?.dispose() );
            }

            const oldGeometry = ( this.gifMesh as THREE.Mesh ).geometry;
            if ( oldGeometry )
                oldGeometry.dispose()
        }
        if ( this.imgElement )
            this.imgElement.parentNode.removeChild( this.imgElement );
    }
}
