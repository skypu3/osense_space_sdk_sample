export default class VideoPlayerBase 
{
    public videoID: string;
    public videoElement: HTMLDivElement;
    public timer: any;
    public isPlaying: boolean;
    public isMuted: boolean;
    public player: any;
    public width: number = 640;
    public height: number = 360;
    public autoAudio: boolean;
    public autoPlay: boolean;
    public maxWidth: number = 1.0;

    constructor( videoId: string, autoPlay: boolean = false, autoAudio: boolean = false, )
    {
        this.videoID = videoId;
        this.autoPlay = autoPlay;
        this.autoAudio = autoAudio;
        this.isMuted = !this.autoAudio;
    }

    init( data?: any )
    {
        if ( data )
        {
            this.maxWidth = data.maxWidth;
        }
        if ( this.autoPlay ) this.playVideo();
    }

    play()
    {
        //TODO: control player play or pause video
        if ( this.isPlaying )
            this.pauseVideo()
        else
            this.playVideo();
        this.isPlaying = !this.isPlaying;
    };

    playVideo() { }

    pauseVideo() { }

    getVolume() { }

    muted(muted: boolean) {
        this.isMuted = muted
    }

    setVolume( volume: number ) { }

    destroy()
    {
        if ( this.videoElement )
        {
            this.videoElement.parentNode.removeChild( this.videoElement );
        }
        if ( this.timer )
        {
            clearInterval( this.timer );
        }
    }

    public get ratio()
    {
        return this.height / this.width;
    }
}