import * as THREE from 'three'
import GreenVideoShaderMaterial from './shader/ChromaVideoShader'
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import VideoPlayerBase from '../VideoPlayerBase';

export default class GreenVideoPlayer extends VideoPlayerBase {
    player: HTMLVideoElement;
    GreenVideoObj: THREE.Mesh;
    videoTexture: THREE.Texture;
    url: string;

    async init(url: string): Promise<THREE.Mesh> {

        this.url = url;

        const videoData = await Global.inst.resLoad.load(url, ResourcesType.Video);
        this.width = videoData.width;
        this.height = videoData.height;
        this.player = videoData.video;
        this.GreenVideoObj = this.createGreenVideo();

        super.init();
        await this.playVideo();

        return this.GreenVideoObj;
    }

    async playVideo() {
        await this.player.load()
        await this.player.play()
    }

    pauseVideo() {
        Global.inst.resLoad.removeVideo(this.player)
    }

    destroy() {
        // store old texture and geometry
        const oldTexture = ((this.GreenVideoObj as THREE.Mesh).material as THREE.MeshBasicMaterial).map;
        const oldGeometry = (this.GreenVideoObj as THREE.Mesh).geometry;

        //dispose old texture and geometry
        if (oldTexture)
            oldTexture.dispose()
        if (oldGeometry) {
            oldGeometry.dispose()
        }
    }

    private createGreenVideo(): THREE.Mesh {

        const geometry = new THREE.PlaneBufferGeometry(this.maxWidth * this.width / this.height, this.maxWidth, 1, 1);

        const texture = new THREE.VideoTexture(this.player);
        texture.generateMipmaps = true;
        texture.minFilter = THREE.LinearFilter;
        texture.magFilter = THREE.LinearFilter;
        texture.format = THREE.RGBFormat;

        const keyColor = [0.0, 212 / 255, 50 / 255]
        const shader = new GreenVideoShaderMaterial()
        shader.uniforms.tex.value = texture;
        shader.uniforms.color.value = keyColor
        const material = new THREE.ShaderMaterial({
            fragmentShader: shader.fragmentShader,
            vertexShader: shader.vertexShader,
            uniforms: shader.uniforms,
            transparent: true,
        });

        const mesh = new THREE.Mesh(geometry, material);

        return mesh

        // Global.inst.resLoad.videoResources[this.url] = {
        //     width: this.width,
        //     height: this.height,
        //     video: this.player,
        //     mesh
        // }

        // return mesh.clone();
    }
}
