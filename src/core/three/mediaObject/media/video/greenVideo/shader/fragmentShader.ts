export default `
uniform mediump sampler2D tex;
uniform mediump vec3 color;
uniform float similarity;
uniform float smoothness;
varying mediump vec2 vUv;

void main() {
  mediump vec4 tColor = texture2D( tex, vUv );

  float Y1 = 0.299 * color.r + 0.587 * color.g + 0.114 * color.b;
  float Cr1 = color.r - Y1;
  float Cb1 = color.b - Y1;

  float Y2 = 0.299 * tColor.r + 0.587 * tColor.g + 0.114 * tColor.b;
  float Cr2 = tColor.r - Y2;
  float Cb2 = tColor.b - Y2;

  float blend = smoothstep(similarity, similarity + smoothness, distance(vec2(Cr2, Cb2), vec2(Cr1, Cb1)));
  gl_FragColor = vec4(tColor.rgb, tColor.a * blend);
}
`;
