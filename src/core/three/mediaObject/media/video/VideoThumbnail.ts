import Global from "core/three/base/Global";
import { ResourcesType } from "core/three/base/ResourcesMgr";
import * as THREE from "three";
import { Vector3 } from "three";
import Media from "../Media";
import playBtnImage from './play-btn.svg';
import loaderImage from './loader.png';
import { SpriteGifLoader } from "../../customLoader/SpriteGifLoader";

export default class VideoThumbnail extends THREE.Group
{
    public thumbnail: THREE.Mesh;
    private playButton: THREE.Mesh;
    private loaderMesh: THREE.Mesh;
    private thumbnailURL: string;
    private thumbnailGeo: THREE.PlaneBufferGeometry;
    private thumbnailMat: THREE.MeshBasicMaterial;
    private playBtnGeo: THREE.PlaneBufferGeometry;
    private playBtnMat: THREE.MeshBasicMaterial;
    private loaderGeo: THREE.PlaneBufferGeometry;
    private loaderMat: THREE.MeshBasicMaterial;
    private maxWidth: number;
    private isH5Video: boolean;
    private loader: SpriteGifLoader;
    private animation: NodeJS.Timeout;
    public boxScale: Vector3;

    constructor( thumbnailURL: string, maxWidth?: number, isH5Video?: boolean )
    {
        super()
        this.thumbnailURL = thumbnailURL
        this.maxWidth = maxWidth ?? 1.0
        this.isH5Video = isH5Video ?? true;
    }

    async init()
    {
        await this.updateThumbnail();
        await this.updatePlayBtn();
        await this.updateLoaderIcon();
    }

    public enablePlayButton( enable: boolean )
    {
        this.getObjectByName( 'thumbnail' ).visible = false;
        if ( enable )
        {
            this.getObjectByName( 'play' ).visible = true;
        }
        else
        {
            this.getObjectByName( 'play' ).visible = false;
        }
    }

    public enableLoader( enable: boolean )
    {
        if ( enable )
        {
            this.animation = setInterval( () =>
            {
                this.loader.animate();
            }, 30 )
            this.getObjectByName( 'loader' ).visible = true;
            this.getObjectByName( 'play' ).visible = false;
        }
        else
        {
            clearTimeout( this.animation )
            this.getObjectByName( 'loader' ).visible = false;
            this.getObjectByName( 'play' ).visible = true;
        }
    }

    async updateThumbnail()
    {
        const texture = await Global.inst.resLoad.load( this.thumbnailURL, ResourcesType.Texture );
        this.thumbnailMat = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide, transparent: true, combine: 0 } );
        var materials = [
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            this.thumbnailMat,
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
        ];

        this.thumbnailGeo = new THREE.BoxBufferGeometry( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 0.03, 1 );
        this.boxScale = new Vector3( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 0.032 );
        this.thumbnail = new THREE.Mesh( this.thumbnailGeo, materials );
        // this.thumbnail.layers.enable(3);
        this.thumbnail.name = 'thumbnail';

        // youtube影片格式
        if ( !this.isH5Video )
        {
            const ratio = 1.25;
            const scale = this.thumbnail.scale.clone();
            const textureRatio = texture.image.height / texture.image.width;
            scale.x = 1.6 * ratio;
            scale.y = 0.9 * ratio / textureRatio;
            this.thumbnail.scale.copy( scale );
        }
        this.add( this.thumbnail );
    }

    async updateLoaderIcon()
    {
        const texture = await Global.inst.resLoad.load( loaderImage, ResourcesType.Texture );
        texture.needsUpdate = true;

        this.loader = new SpriteGifLoader( texture, 5, 2, 15, 10 )
        const animatedTexture = this.loader.init();

        this.loaderMat = new THREE.MeshBasicMaterial( { map: animatedTexture, side: THREE.DoubleSide, transparent: true, combine: 0 } );
        this.loaderMat.needsUpdate = true;

        this.loaderGeo = new THREE.PlaneBufferGeometry( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 1, 1 );
        this.loaderMesh = new THREE.Mesh( this.loaderGeo, this.loaderMat );
        this.loaderMesh.name = 'loader';
        this.loaderMesh.scale.copy( new THREE.Vector3( 0.25, 0.6, 1 ) );
        this.loaderMesh.translateZ( 0.032 );
        this.loaderMesh.visible = false;
        this.add( this.loaderMesh );
    }

    async updatePlayBtn()
    {
        const texture = await Global.inst.resLoad.load( playBtnImage, ResourcesType.Texture );
        this.playBtnMat = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide, transparent: true, combine: 0 } );
        this.playBtnGeo = new THREE.PlaneBufferGeometry( 1.0 * this.maxWidth, ( texture.image.height / texture.image.width ) * this.maxWidth, 1, 1 );
        this.playButton = new THREE.Mesh( this.playBtnGeo, this.playBtnMat );
        this.playButton.name = 'play';
        this.playButton.scale.copy( new THREE.Vector3( 0.2, 0.2, 1 ) );
        this.playButton.translateZ( 0.032 );
        this.add( this.playButton );
    }

    destroy()
    {
        if ( this.parent )
        {
            this.parent.remove( this );
        }

        const materials = ( this.thumbnail as THREE.Mesh ).material as THREE.MeshBasicMaterial[];
        if ( materials && materials.length > 0 )
        {
            materials.forEach( ( material ) => material?.dispose() );
        }

        this.playBtnGeo?.dispose();
        this.playBtnMat?.dispose();
        this.loaderGeo?.dispose();
        this.loaderMat?.dispose();
        this.thumbnailGeo?.dispose();
        this.thumbnailMat?.dispose();
        this.playBtnGeo = null;
        this.playBtnMat = null;
        this.thumbnailGeo = null;
        this.thumbnailMat = null;
    }

}
