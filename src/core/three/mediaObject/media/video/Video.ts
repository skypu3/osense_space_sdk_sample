import { Format } from 'core/three/scene/Format';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Media from 'core/three/mediaObject/media/Media';
import Youtube from './videoPlayer/Youtube'
import Vimeo from './videoPlayer/Vimeo';
import GreenVideoPlayer from './greenVideo/GreenVideo';
import { RaycastClickData } from '../../RaycastObject';
import VideoPlayerBase from './VideoPlayerBase';
import * as log from 'loglevel';
import H5VideoPlayer from './h5Video/H5Video';
import VideoThumbnail from './VideoThumbnail';
import store from 'core/store/store';
import { ViewerMode } from 'core/store/actions/mode';
import { isMobile } from 'react-device-detect';
import isWebview from 'is-ua-webview';

export enum VideoType {
    youtube = "youtube",
    vimeo = "vimeo",
    greenVideo = "greenVideo",
    h5Video = "h5Video"
}

export default class Video extends Media {
    public videoType: VideoType;
    public videoId: String;
    public videoPlayer: Youtube | Vimeo | GreenVideoPlayer | H5VideoPlayer;
    public autoPlay: boolean
    public autoAudio: boolean
    private thumbnailTexture: VideoThumbnail;

    get json() {
        return {
            ...super['json'],
            videoType: this.videoType.toString(),
            videoId: this.videoId,
            autoPlay: this.autoPlay,
            autoAudio: this.autoAudio
        }
    }

    get targetInfo(): RaycastClickData {
        return {
            ...super['targetInfo']
        }
    }

    get videoStatus(): boolean {
        return (this.videoPlayer as VideoPlayerBase).isPlaying;
    }

    constructor() {
        super(EObjectType.VIDEO)
        this.updateRaycastMesh()
    }

    async init(data: Format.IVideo): Promise<void> {
        super.init(data)
        const { videoType, videoId, fileUrl } = data
        const { autoPlay, autoAudio } = data

        this.autoPlay = autoPlay ?? false;
        this.autoAudio = autoAudio ?? false;
        this.videoId = videoId;
        this.videoType = VideoType[videoType];
        this.fileUrl = fileUrl;
        this.thumbnail = this.thumbnail.replace('256', '1024')

        if (this.videoType) {
            switch (this.videoType) {
                case VideoType.vimeo:
                case VideoType.youtube:
                    await this.loadThumbnail(false);
                    break;
                case VideoType.h5Video:
                    await this.loadThumbnail(true);
                    if (store.getState().modeReducer == ViewerMode.Viewer && this.autoPlay && !this.autoAudio) {
                        if (!isMobile && !isWebview(navigator.userAgent)) {
                            this.toggle()
                        }
                    }
                    break;
                case VideoType.greenVideo:
                    this.toggle()
                    break;
            }
        }
    }

    setJson(data: Format.IVideo) {
        super.setJson(data);
        this.videoId = data.videoId;
        this.videoType = VideoType[data.videoType];
        this.autoPlay = data.autoPlay;
        this.autoAudio = data.autoAudio;
        this.updateVideo();
    }

    async loadThumbnail(isH5Video: boolean) {
        if (isH5Video) {
            this.thumbnailTexture = new VideoThumbnail(this.thumbnail, this.maxWidth);
        } else {
            this.thumbnailTexture = new VideoThumbnail(this.thumbnail, this.maxWidth, false);
        }
        await this.thumbnailTexture.init();
        this.thumbnailTexture.position.copy(this.object.position);
        this.thumbnailTexture.rotation.copy(this.object.rotation);
        this.object.attach(this.thumbnailTexture);
        this.updateBoundaryBox(this.thumbnailTexture.boxScale.x, this.thumbnailTexture.boxScale.y, this.thumbnailTexture.boxScale.z);
        this.updateRaycastMesh([this.thumbnailTexture.thumbnail])
    }

    async loadVideoUrl() {

        if (this.videoType) {
            switch (this.videoType) {
                case VideoType.vimeo:
                case VideoType.youtube:
                    if (this.videoId) {
                        await this.loadVideo(this.videoType, this.videoId as string)
                    }
                    break;
                case VideoType.h5Video: // .mp4 video
                    if (this.fileUrl) {
                        await this.loadH5Video(this.fileUrl)
                    }
                    else {
                        log.error(`Video url not found: ${this.id}`)
                    }
                    break;
                case VideoType.greenVideo: //.mp4 green video
                    if (this.fileUrl) {
                        await this.loadGreenVideo(this.fileUrl)
                        this.isVertical = true;
                    }
                    else {
                        log.error(`Video url not found: ${this.id}`)
                    }
                    break;
                default:
                    // To fit old data format
                    if (this.fileUrl) {
                        await this.loadH5Video(this.fileUrl)
                    }
                    else {
                        log.error(`videoSource undefined`)
                    }
            }

            this.updateVideo();
        }
        else {
            log.error(`Video data not found: ${this.id}`)
        }
    }

    public async toggle() {
        if (this.videoPlayer && this.videoPlayer.isPlaying) {
            this.videoPlayer.pauseVideo();
            this.thumbnailTexture.enablePlayButton(true)
        } else {
            // 通常是NFT的影片
            if (!this.videoPlayer) {
                this.thumbnailTexture.enableLoader(true);
                await this.loadVideoUrl();
                this.thumbnailTexture.enableLoader(false);
            }
            this.videoPlayer.playVideo();

            this.thumbnailTexture.enablePlayButton(false)
        }
    }


    pauseVideo() {
        this.videoPlayer.pauseVideo()
    }

    playVideo() {
        this.videoPlayer.playVideo()
    }

    updateVideo() {
        //TODO: update video texture         
    }

    public destroy() {
        super.destroy();
        this.thumbnailTexture.destroy()
        if (this.videoPlayer) {
            this.videoPlayer.destroy();
        }
    }

    private async loadVideo(videoSource: VideoType, videoId: string): Promise<void> {
        let ratio;
        if (videoSource === VideoType.youtube) {
            this.videoPlayer = new Youtube(videoId, this.autoPlay, this.autoAudio);
            this.videoType = VideoType.youtube
            const mesh = this.videoPlayer.init()
            mesh.name = 'youtube';
            this.object.add(mesh);
            this.updateRaycastMesh(mesh.geometry)
            ratio = this.videoPlayer.ratio;
        }
        else if (videoSource === VideoType.vimeo) {
            this.videoPlayer = new Vimeo(videoId);
            this.videoType = VideoType.vimeo
            const mesh = this.videoPlayer.init()
            this.object.add(mesh);
            this.updateRaycastMesh(mesh.geometry)

            if (this.autoPlay) {
                this.videoPlayer.play();
            }
            ratio = this.videoPlayer.ratio;
        }
        this.updateBoundaryBox(2, 2 * ratio, 0.05);
    }

    private async loadGreenVideo(fileUrl: string): Promise<void> {
        this.videoPlayer = new GreenVideoPlayer("", this.autoPlay, this.autoAudio);
        this.videoType = VideoType.greenVideo;
        this.object.add(await this.videoPlayer.init(fileUrl));
        this.updateBoundaryBox(1 * this.maxWidth, (this.videoPlayer.height / this.videoPlayer.width) * this.maxWidth, 0.05);
    }

    private async loadH5Video(fileUrl: string): Promise<void> {
        this.videoPlayer = new H5VideoPlayer("", this.autoPlay, this.autoAudio);
        this.videoType = VideoType.h5Video;
        this.object.add(await this.videoPlayer.init(fileUrl, this.maxWidth));
        this.updateBoundaryBox(1 * this.maxWidth, (this.videoPlayer.height / this.videoPlayer.width) * this.maxWidth, 0.05);
    }
}
