import * as THREE from 'three'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import VideoPlayerBase from '../VideoPlayerBase'
import YoutubePlayer from 'youtube-player'
import PlayerStates from "youtube-player/dist/constants/PlayerStates"
import { Mesh } from 'three'
import { isIPad13 } from 'react-device-detect'

export default class Youtube extends VideoPlayerBase {
    play: YoutubePlayer;
    videoObj: Mesh;

    init(): THREE.Mesh {
        super.init({ maxWidth: 2 });
        return this.createObject();
    }

    public playVideo(muteAudio: boolean = undefined) {
        //TODO : Play youtube video
        // sometimes video is unstarted ...
        if (!this.timer) {
            this.timer = setInterval(function () {
                this.player.getPlayerState().then(async state => {
                    if (state !== PlayerStates.PLAYING) {
                        await this.player.pauseVideo()
                        if (!isIPad13) {
                            this.muted(true)
                            this.player.setVolume(0);
                        }
                        await this.player.playVideo();

                        //當replay時
                        if (muteAudio !== undefined) {
                            if (muteAudio) {
                                this.muted(true)
                            } else {
                                this.muted(false)
                            }
                            return
                        }

                        if (this.autoAudio) {
                            this.muted(false)
                            this.player.setVolume(80)
                        } else {
                            this.muted(true)
                        }
                    }
                    if (state === PlayerStates.PLAYING) {
                        this.isPlaying = true
                        clearInterval(this.timer);
                        this.timer = null;
                    }
                })
            }.bind(this), 500);
        }
    }

    pauseVideo() {
        this.player.pauseVideo()
        clearInterval(this.timer);
        this.timer = null;
        this.isPlaying = false;
    }

    public setVolume(volume: number) {
        this.player.setVolume(volume);
    }

    public getVolume() {
        return this.player.getVolume()
    }

    public muted(muted: boolean) {
        super.muted(muted)
        if (muted) {
            this.player.mute()
        } else {
            this.player.unMute()
        }
    }

    destroy() {
        super.destroy()
        if (this.videoObj) {
            const oldGeometry = (this.videoObj as THREE.Mesh).geometry;
            const materials = (this.videoObj as THREE.Mesh).material as THREE.MeshBasicMaterial[];
            if (materials && materials.length > 0) {
                materials.forEach((material) => material.dispose());
            }
            if (oldGeometry)
                oldGeometry.dispose()
        }
    }

    private createObject(): THREE.Mesh {
        const element = document.createElement('div');
        element.style.width = this.width + "px"
        element.style.height = this.height + "px"

        const iframe = document.createElement('div');
        element.appendChild(iframe);
        iframe.style.width = '100%'
        iframe.style.height = '100%'

        this.player = new YoutubePlayer(iframe);
        this.player.loadVideoById(this.videoID);
        this.isPlaying = false;

        this.player.on('ready', (event) => {
        })

        this.player.on('stateChange', (event) => {
            if (event.data === PlayerStates.ENDED) {
                this.playVideo(this.isMuted);
            }
        });

        this.videoElement = element;

        const css3dObject = new CSS3DObject(element);

        let sizeX = this.maxWidth;
        let sizeY = this.ratio * this.maxWidth;

        css3dObject.scale.set(1 / this.width * sizeX, (1 / this.height) * sizeY, 1);

        const material = new THREE.MeshBasicMaterial({
            opacity: 0,
            color: new THREE.Color(0x000000),
            blending: THREE.NoBlending,
            side: THREE.DoubleSide,
            transparent: true,
        });

        let geometry = new THREE.PlaneBufferGeometry(sizeX, sizeY);
        let mesh = new THREE.Mesh(geometry, material);
        mesh.add(css3dObject);
        this.videoObj = mesh;
        return mesh;
    }
}