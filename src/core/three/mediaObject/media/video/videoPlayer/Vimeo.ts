import * as THREE from 'three'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import VideoPlayerBase from '../VideoPlayerBase';
import Vimeo from "@vimeo/player"
import { Mesh } from 'three';

export default class VimeoPlayer extends VideoPlayerBase
{
    player: Vimeo
    videoObj: Mesh

    init(): THREE.Mesh
    {
        super.init( { maxWidth: 2 } );
        return this.createObject();
    }

    async playVideo()
    {
        //TODO : Play vimeo video
        // sometimes video is unstarted ...
        this.timer = setInterval( function ()
        {
            this.player.getPaused().then( function ( paused )
            {
                if ( paused === true )
                {
                    this.player.play();
                    this.player.setVolume( 0 );
                    this.player.setMuted( true );
                    this.isPlaying = true;

                    //this.player.unMute();
                }
                else
                {
                    clearInterval( this.timer );
                }
            }.bind( this ) );
        }.bind( this ), 500 );
    }

    pauseVideo()
    {
        this.player.pause();
        clearInterval( this.timer );
        this.isPlaying = false;
    }

    public setVolume( volume: number )
    {
        this.player.setVolume( volume / 100 );
    }

    public async getVolume()
    {
        const volume = await this.player.getVolume()
        return volume * 100
    }

    destroy()
    {
        if ( this.videoObj )
        {
            const oldGeometry = ( this.videoObj as THREE.Mesh ).geometry;
            const materials = ( this.videoObj as THREE.Mesh ).material as THREE.MeshBasicMaterial[];
            if ( materials && materials.length > 0 )
            {
                materials.forEach( ( material ) => material.dispose() );
            }
            if ( oldGeometry )
                oldGeometry.dispose()
        }
    }

    private createObject(): THREE.Mesh
    {

        const element = document.createElement( 'div' );
        element.style.width = this.width + "px"
        element.style.height = this.height + "px"

        var options = {
            id: this.videoID,
            autoplay: false,
            autopause: false,
            loop: true,
            muted: true,
            width: this.width,
            height: this.height,
        };

        this.player = new Vimeo( element, options );

        this.isPlaying = false;

        this.player.ready().then( () =>
        {
            // the player is ready
            this.pauseVideo();
        } );

        this.player.on( 'ended', () =>
        {
            this.playVideo();
        } )

        this.videoElement = element;

        const css3dObject = new CSS3DObject( element );

        let sizeX = this.maxWidth;
        let sizeY = this.ratio * this.maxWidth;
        css3dObject.scale.set( 1 / this.width * sizeX, ( 1 / this.height ) * sizeY, 1 );

        const material = new THREE.MeshBasicMaterial( {
            opacity: 0,
            color: new THREE.Color( 0x000000 ),
            blending: THREE.NoBlending,
            side: THREE.DoubleSide,
            transparent: true,
        } );

        // let geometry = new THREE.PlaneBufferGeometry( sizeX, sizeY );
        const geometry = new THREE.BoxBufferGeometry( sizeX, sizeY, 0.05, 1 );
        var materials = [
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
            material,
            new THREE.MeshBasicMaterial( { color: 0x5b5b5b } ),
        ];
        let mesh = new THREE.Mesh( geometry, materials );
        this.videoObj = mesh;
        mesh.add( css3dObject );
        return mesh;
    }

}