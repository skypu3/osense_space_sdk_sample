import * as THREE from 'three'
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import VideoPlayerBase from '../VideoPlayerBase';
import H5VideoShaderMaterial from '../h5Video/shader/H5VideoShader';
import { Vector3 } from 'three';

export default class Video360Player extends VideoPlayerBase
{
    player: HTMLVideoElement;
    videoTexture: THREE.Texture;
    public videoObj: THREE.Mesh;

    async init( url: string, maxWidth?: number )
    {
        const videoData = await Global.inst.resLoad.load( url, ResourcesType.Video );

        this.width = videoData.width;
        this.height = videoData.height;
        this.player = videoData.video;
        super.init( { maxWidth } );
        this.videoObj = this.createVideo();

        return this.videoObj;
    }

    playVideo()
    {
        this.player.play();
    }

    pauseVideo()
    {
        this.isPlaying = false;
        this.player.pause();
    }

    public muted( muted: boolean )
    {
        super.muted( muted )
        if (this.player)
            this.player.muted = muted;
    }

    public setVolume( volume: number )
    {
        if ( volume > 0 )
        {
            this.player.muted = false;
        }
        this.player.volume = volume / 100;
    }

    public async getVolume()
    {
        if ( this.player )
        {
            const volume = this.player.volume
            return volume * 100
        }
    }

    destroy()
    {
        if ( this.player )
        {
            this.player.pause()
            this.player.removeAttribute( 'src' )
            this.player.remove()
            if ( this.videoObj )
            {
                const oldGeometry = ( this.videoObj as THREE.Mesh ).geometry;
                const oldTexture = ((this.videoObj as THREE.Mesh).material as THREE.ShaderMaterial)
                if (oldTexture)
                    oldTexture.dispose()
                if ( oldGeometry )
                    oldGeometry.dispose()
            }
        }
    }

    private createVideo()
    {
        // const scale = new Vector3( 1.0, 1.0, 1.0 );
        // scale.x = this.maxWidth * 1.0;
        // scale.y = this.maxWidth * this.height / this.width;

        var geometry = new THREE.SphereBufferGeometry( 500, 60, 40 );
        geometry.scale( -1, 1, 1 );

        // const geometry = new THREE.BoxBufferGeometry( scale.x, scale.y, 0.03, 1 );
        const texture = new THREE.VideoTexture( this.player );
        texture.format = THREE.RGBAFormat;

        const shader = new H5VideoShaderMaterial();
        shader.uniforms.tex.value = texture;
        const material = new THREE.ShaderMaterial( {
            fragmentShader: shader.fragmentShader,
            vertexShader: shader.vertexShader,
            uniforms: shader.uniforms,
            transparent: true,
        } );

        this.playVideo()

        const videoMesh = new THREE.Mesh( geometry, material );
        return videoMesh;
    }
}