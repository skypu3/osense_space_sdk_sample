import * as THREE from 'three'
import Global from 'core/three/base/Global';
import { ResourcesType } from 'core/three/base/ResourcesMgr';
import VideoPlayerBase from '../VideoPlayerBase';
import H5VideoShaderMaterial from './shader/H5VideoShader';
import { Vector3 } from 'three';

export default class H5VideoPlayer extends VideoPlayerBase {
    player: HTMLVideoElement;
    videoTexture: THREE.Texture;
    videoObj: THREE.Mesh;

    async init(url: string, maxWidth?: number) {

        // opensea video cors 問題
        if (url.includes('https://storage.opensea.io/files')) {
            url = url.replace('https://storage.opensea.io/files', 'https://openseauserdata.com/files')
        }
        const videoData = await Global.inst.resLoad.load(url, ResourcesType.Video);

        this.width = videoData.width;
        this.height = videoData.height;
        this.player = videoData.video;
        super.init({ maxWidth });
        this.videoObj = this.createVideo();

        return this.videoObj;
    }

    playVideo(enableAudio: boolean = true) {
        if (!this.player) return

        if (this.autoAudio && enableAudio)
            this.muted(false)
        else
            this.muted(true)
        this.isPlaying = true
        // const vol = this.player.volume;
        this.player.volume = 1;
        this.player.play();
    }

    pauseVideo() {
        this.isPlaying = false;
        this.player.pause();
    }

    public muted(muted: boolean) {
        super.muted(muted)
        if (this.player)
            this.player.muted = muted;
    }

    public setVolume(volume: number) {
        if (volume > 0) {
            this.player.muted = false;
        }
        this.player.volume = volume / 100;
    }

    public async getVolume() {
        if (this.player) {
            const volume = this.player.volume
            return volume * 100
        }
    }

    destroy() {
        if (this.player) {
            this.player.pause()
            this.player.removeAttribute('src')
            this.player.remove()
            if (this.videoObj) {
                const oldGeometry = (this.videoObj as THREE.Mesh).geometry;
                const materials = (this.videoObj as THREE.Mesh).material as THREE.MeshBasicMaterial[];
                if (materials && materials.length > 0) {
                    materials.forEach((material) => material.dispose());
                }
                // const oldTexture = ((this.videoObj as THREE.Mesh).material as THREE.ShaderMaterial)
                // if (oldTexture)
                //     oldTexture.dispose()
                if (oldGeometry)
                    oldGeometry.dispose()
            }
        }
    }

    private createVideo() {
        const scale = new Vector3(1.0, 1.0, 1.0);
        scale.x = this.maxWidth * 1.0;
        scale.y = this.maxWidth * this.height / this.width;
        const geometry = new THREE.BoxBufferGeometry(scale.x, scale.y, 0.03, 1);
        const texture = new THREE.VideoTexture(this.player);
        texture.format = THREE.RGBAFormat;

        const shader = new H5VideoShaderMaterial();
        shader.uniforms.tex.value = texture;
        const material = new THREE.ShaderMaterial({
            fragmentShader: shader.fragmentShader,
            vertexShader: shader.vertexShader,
            uniforms: shader.uniforms,
            transparent: true,
        });

        var materials = [
            new THREE.MeshBasicMaterial({ color: 0x000000 }),
            new THREE.MeshBasicMaterial({ color: 0x000000 }),
            new THREE.MeshBasicMaterial({ color: 0x000000 }),
            new THREE.MeshBasicMaterial({ color: 0x000000 }),
            material,
            new THREE.MeshBasicMaterial({ color: 0x000000 }),
        ];

        const videoMesh = new THREE.Mesh(geometry, materials);
        // videoMesh.layers.enable(3);
        return videoMesh;
    }
}