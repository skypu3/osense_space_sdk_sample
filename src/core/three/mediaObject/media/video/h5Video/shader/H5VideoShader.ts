import * as THREE from 'three';
import fragmentShader from './fragmentShader';
import vertexShader from './vertexShader';


export default class H5VideoShaderMaterial extends THREE.ShaderMaterial
{
    constructor()
    {
        super( {
            fragmentShader: fragmentShader,
            vertexShader: vertexShader,
            uniforms: {
                tex: {
                    value: null,
                },
                color: {
                    value: null,
                },
            },
        } )
    }
}

