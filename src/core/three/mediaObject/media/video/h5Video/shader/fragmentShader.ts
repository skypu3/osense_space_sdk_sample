export default `
uniform mediump sampler2D tex;
uniform mediump vec3 color;
varying mediump vec2 vUv;
void main() {
  mediump vec3 tColor = texture2D( tex, vUv ).rgb;
  gl_FragColor = vec4(tColor, 1.0);
}
`;
