import * as THREE from 'three';
import { Format } from 'core/three/scene/Format';
import { EObjectType } from '../../MediaObject3D';
import Media from '../Media';

export default class PositioningCube extends Media
{
    private _swappedType: string;
    private _swappedMedia: Media;
    private swappedObject: THREE.Object3D;
    private mesh: THREE.Mesh;

    public get json()
    {
        const sizeMesh = this.mesh.clone();
        sizeMesh.position.set( 0, 0, 0 );
        sizeMesh.rotation.set( 0, 0, 0 );
        sizeMesh.updateMatrix();

        let size = new THREE.Vector3();
        new THREE.Box3().setFromObject( sizeMesh ).getSize( size );

        return {
            ...super[ 'json' ],
            size: size,
            swappedType: this._swappedType,
            swappedProps: this.swappedProps
        };
    }

    public get swappedMedia()
    {
        return this._swappedMedia;
    }

    public get swappedProps(): any
    {
        if ( this._swappedMedia )
        {
            let obj = this._swappedMedia.json;

            // Remove useless property
            obj.position && delete obj.position
            obj.rotation && delete obj.rotation
            obj.scale && delete obj.scale
            obj.isVertical !== undefined && delete obj.isVertical
            obj.followCamera !== undefined && delete obj.followCamera

            return obj;
        }
        else
        {
            return undefined;
        }
    }

    public get swappedType()
    {
        return this._swappedType;
    }

    public set swappedMedia( media: Media )
    {
        if ( this._swappedMedia !== undefined )
        {
            this._swappedMedia.destroy();
        }

        this._swappedMedia = media;
        this._swappedType = media.type;
        media.object.remove( media.boundaryBox );

        this.swappedObject = media.object;
        this.swappedObject.position.set( 0, 2, 0 );
        this.swappedObject.rotation.set( 0, 0, 0 );
        this.swappedObject.updateMatrix();
        this.fit2Box( this.swappedObject );
        this.object.add( this.swappedObject );

        this.mesh.visible = false;
        this.name = media.name;
        // this.updateRaycastMesh( this.object );
    }

    constructor()
    {
        super( EObjectType.POSITIONCUBE );
    }

    public async init( data: Format.IPositionCube )
    {
        super.init( data );
        const geometry = new THREE.BoxBufferGeometry( data.size?.x ?? 1, data.size?.y ?? 1, data.size?.z ?? 1 );
        const material = new THREE.MeshBasicMaterial( { color: 0xad6bfc, transparent: true, opacity: 0.7, side: THREE.DoubleSide } );
        this.mesh = new THREE.Mesh( geometry, material );

        this.object.add( this.mesh );
        this.object.name = "Positioning Cube";

        this.updateBoundaryBox( data.size?.x ?? 1, data.size?.y ?? 1, data.size?.z ?? 1 );
        this.updateRaycastMesh( this.mesh );
    }

    public clearSwappedMedia()
    {
        if ( this._swappedType === undefined )
            return;
        this._swappedMedia.destroy();
        this._swappedMedia = undefined;
        this._swappedType = undefined;
        this.mesh.visible = true;
        this.name = "Positioning Cube";
        // this.updateRaycastMesh();
    }

    public setJson( data: Format.IPositionCube )
    {
        if ( data.type === EObjectType.POSITIONCUBE )
        {
            super.setJson( data );
        }
        else if ( data.type === EObjectType.MODEL )
        {
            this.swappedMedia.setJson( data );
        }
        else
        {
            console.error( `Position Cube assign type error: ${ data.type }` )
        }
    }

    destroy()
    {
        super.destroy();
        this.mesh.geometry?.dispose();
        this._swappedMedia?.destroy();
    }

    private fit2Box( object: THREE.Object3D )
    {
        const targetBox = new THREE.Box3();
        targetBox.setFromObject( this.mesh );
        const targetSize = targetBox.max.sub( targetBox.min );

        const objBox = new THREE.Box3();
        objBox.setFromObject( object );
        let objSize = objBox.max.sub( objBox.min );

        const { x, y, z } = new THREE.Vector3( targetSize.x / objSize.x, targetSize.y / objSize.y, targetSize.z / objSize.z );
        let arr = [ x, y, z ];
        let min = Math.min( ...arr );

        const scale = object.scale.clone();
        object.scale.copy( scale.multiplyScalar( min ) );

        object.position.set( 0, -targetSize.y / 2, 0 );
        object.updateMatrix();

    }
}