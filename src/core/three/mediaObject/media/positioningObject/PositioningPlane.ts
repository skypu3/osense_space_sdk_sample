import * as THREE from 'three';
import { Format } from 'core/three/scene/Format';
import { EObjectType } from '../../MediaObject3D';
import Media from '../Media';
import store from 'core/store/store';
import { ViewerMode } from 'core/store/actions/mode';

export default class PositioningPlane extends Media
{
    private _swappedType: string;
    private _swappedMedia: Media;
    private swappedObject: THREE.Object3D;
    private mesh: THREE.Mesh;
    private fit: string;

    public get json()
    {
        const sizeMesh = this.mesh.clone();
        sizeMesh.position.set( 0, 0, 0 );
        sizeMesh.rotation.set( 0, 0, 0 );
        sizeMesh.updateMatrix();

        let size = new THREE.Vector3();
        new THREE.Box3().setFromObject( sizeMesh ).getSize( size );

        return {
            ...super[ 'json' ],
            size: {
                x: size.x,
                y: size.y
            },
            swappedType: this._swappedType,
            swappedProps: this.swappedProps
        };
    }

    public get swappedMedia()
    {
        return this._swappedMedia;
    }

    public get swappedProps(): any
    {
        if ( this._swappedMedia )
        {
            let obj = this._swappedMedia.json;

            // Remove useless property
            obj.position && delete obj.position
            obj.rotation && delete obj.rotation
            obj.scale && delete obj.scale
            obj.isVertical !== undefined && delete obj.isVertical
            obj.followCamera !== undefined && delete obj.followCamera

            return obj;
        }
        else
        {
            return undefined;
        }
    }

    public get swappedType()
    {
        return this._swappedType;
    }

    public set swappedMedia( media: Media )
    {
        if ( this._swappedMedia !== undefined )
        {
            this._swappedMedia.destroy();
        }

        this._swappedMedia = media;
        this._swappedType = media.type;

        this.fit2Plane( media.object );
        this.swappedObject = media.object;
        this.swappedObject.position.set( 0, 0, 0 );
        this.swappedObject.rotation.set( 0, 0, 0 );
        this.swappedObject.updateMatrix();

        this.object.add( this.swappedObject );

        this.mesh.visible = false;
        this.name = media.name;
        this.updateRaycastMesh( media.object );
    }

    private fit2Plane( object: THREE.Object3D )
    {
        const posBox3 = new THREE.Box3();
        const mesh = this.mesh.clone();
        const objectClone = object.clone();
        mesh.rotation.set( 0, 0, 0 );
        objectClone.rotation.set( 0, 0, 0 );
        posBox3.setFromObject( mesh );
        const posSize = posBox3.max.sub( posBox3.min );
        const box3 = new THREE.Box3();
        box3.setFromObject( objectClone );

        let size = box3.max.sub( box3.min );
        if ( this.fit === 'cover' )
        {
            object.scale.copy( new THREE.Vector3( posSize.x / size.x, posSize.y / size.y, 1 ) );
        } else
        {
            const { x, y, z } = new THREE.Vector3( posSize.x / size.x, posSize.y / size.y, posSize.z / size.z );
            let arr = [ x, y, z ];
            let min = Math.min( ...arr );
            let max = Math.max( ...arr );
            let i = 0;
            for ( i = 0; i < arr.length; i++ )
            {
                if ( arr[ i ] > min && arr[ i ] < max )
                {
                    break;
                }
            }
            const scale = object.scale.clone();
            switch ( i )
            {
                case 0:
                    object.scale.copy( scale.multiplyScalar( x ) );
                    break;
                case 1:
                    object.scale.copy( scale.multiplyScalar( y ) );
                    break;
                case 2:
                    object.scale.copy( scale.multiplyScalar( z ) );
                    break;
            }
        }
    }

    public clearSwappedMedia()
    {
        if ( this._swappedType === undefined )
            return;
        this._swappedMedia.destroy();
        this._swappedMedia = undefined;
        this._swappedType = undefined;
        this.mesh.visible = true;
        this.name = "Plane";
        this.updateRaycastMesh( this.mesh );

    }

    public setJson( data: Format.IPositionPlane )
    {
        this.name = data.name;
        if ( data.type === EObjectType.POSITIONPLANE )
        {
            super.setJson( data );
        }
        else
        {
            this.swappedMedia.setJson( data );
        }
    }

    constructor()
    {
        super( EObjectType.POSITIONPLANE );
    }

    public async init( data: Format.IPositionPlane )
    {
        super.init( data );

        const geometry = new THREE.PlaneBufferGeometry( data.size?.x ?? 1, data.size?.y ?? 1 );
        const material = new THREE.MeshBasicMaterial( { color: 0xad6bfc, transparent: true, opacity: 0.7, side: THREE.DoubleSide } );
        this.mesh = new THREE.Mesh( geometry, material );

        const mode = store.getState().modeReducer;
        if ( mode === ViewerMode.Editor )
        {
            this.object.add( this.mesh );
        }
        this.fit = data.fit ? data.fit : 'cover';
        this.name = `position-${ data.positionObjectsCount }`;
        this.updateRaycastMesh( this.mesh );
        this.updateBoundaryBox( data.size?.x ?? 1, data.size?.y ?? 1, 0.0001 );
        this._swappedType = data.swappedType;
    }

    destroy()
    {
        super.destroy();
        this.mesh.geometry?.dispose();
        this._swappedMedia?.destroy();
    }
}