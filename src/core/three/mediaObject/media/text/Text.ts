import * as THREE from 'three';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import { Format } from 'core/three/scene/Format';
import TextTexture from 'core/three/TextTexture/TextTexture';
import Media from '../Media';
import { RaycastClickData } from '../../RaycastObject';

export default class Text extends Media
{
    private label: string = "Text";
    private fontWeight: string = "normal"; //"bold, normal"
    private fontStyle: string = "normal"; //"italic, normal"
    private fontColor: string = "#000000"; //"#000000"
    private fontSize: number = 60;
    private lineSpace: number = 0.5;
    private aligment: string = "left"; // "left,center,right"
    private textMesh: THREE.Mesh;
    private angle: number = 0;

    constructor()
    {
        super( EObjectType.TEXT );

        const material = new THREE.MeshBasicMaterial( { map: null, side: THREE.DoubleSide } );
        material.transparent = true;

        this.textMesh = new THREE.Mesh(
            new THREE.PlaneGeometry( 1, 1 ),
            material
        );

        this.object.add( this.textMesh );
    }

    public get json()
    {
        return {
            ...super[ 'json' ],
            extData: {
                label: this.label,
                fontWeight: this.fontWeight,
            },
            fontStyle: this.fontStyle,
            fontColor: this.fontColor,
            fontSize: this.fontSize,
            lineSpace: this.lineSpace,
            aligment: this.aligment,
            angle: this.angle,
        }
    }

    public setJson( data: Format.IText )
    {
        super.setJson( data );
        this.label = data.extData.label;
        this.fontWeight = data.extData.fontWeight;
        this.fontStyle = data.fontStyle;
        this.fontColor = data.fontColor;
        this.fontSize = data.fontSize;
        this.lineSpace = data.lineSpace;
        this.aligment = data.aligment;
        this.angle = data.angle ?? 0;
        this.rotateText();
        this.updateLabel()
    }

    public init( data: Format.IText ): void
    {
        super.init( data )

        this.label = data.extData.label;
        this.fontWeight = data.extData.fontWeight;
        this.updateLabel()
    }

    public get targetInfo(): RaycastClickData
    {
        return {
            ...super[ 'targetInfo' ],
        }
    }

    private updateLabel(): void
    {
        // create a canvas element
        const texture = new TextTexture( {
            labels: this.label,
            fontWeight: this.fontWeight,
            fontStyle: this.fontStyle,
            fontColor: this.fontColor,
            padding: 20,
            fontSize: this.fontSize,
            aligment: this.aligment,
            lineSpace: this.lineSpace,
        }, {
            fillStyle: "rgba(0,0,0,0.0)"
        } );

        texture.needsUpdate = true;
        texture.encoding = THREE.GammaEncoding;

        // store old texture and geometry
        const oldTexture = ( ( this.textMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial ).map;
        const oldGeometry = ( this.textMesh as THREE.Mesh ).geometry;

        //set new geometry and texture
        ( ( this.textMesh as THREE.Mesh ).material as THREE.MeshBasicMaterial ).map = texture;

        let sizeX = texture.width * 2;
        let sizeY = texture.height * 2;

        ( this.textMesh as THREE.Mesh ).geometry = new THREE.PlaneBufferGeometry( sizeX, sizeY );
        //dispose old texture and geometry
        if ( oldTexture )
            oldTexture.dispose()
        if ( oldGeometry )
            oldGeometry.dispose()
        this.updateRaycastMesh( this.textMesh.geometry )
        this.updateBoundaryBox( sizeX, sizeY, 0.001 );
    }

    private rotateText()
    {
        let ori_angle = this.object.rotation.z;
        let target_angle = this.angle / 180 * Math.PI;

        this.object.rotateZ( target_angle - ori_angle );
    }

}
