import * as THREE from 'three'

export default class MouseCursor extends THREE.Mesh
{
    private isGlobalVisible: boolean;

    constructor(color: string)
    {
        const geometry = new THREE.RingGeometry( 0.1, 0.2, 36, 1, 32 );
        const material = new THREE.MeshBasicMaterial( {
            color: new THREE.Color(color),
            side: THREE.DoubleSide,
            transparent: true,
            opacity: 0,
            depthTest: false,
        } );
        super( geometry, material )
        this.renderOrder = 2
        this.isGlobalVisible = true;
    }

    public set opacity( value: number )
    {
        const mat = this.material as THREE.Material
        mat.opacity = value
    }

    public set( rot4: THREE.Matrix4, pos: THREE.Vector3 )
    {
        this.position.set(
            pos.x,
            pos.y,
            pos.z
        );
        this.setRotationFromMatrix( rot4 )
        this.opacity = 1
    }

    public setGlobalVisible(value: boolean) {
        this.isGlobalVisible = value;
        this.visible = this.isGlobalVisible;
    }

    public setVisible(value: boolean)
    {
        if (!this.isGlobalVisible) {
            this.visible = false
        } else {
            this.visible = value;
        }
    }

}