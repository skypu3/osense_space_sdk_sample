import * as THREE from 'three'

export default class ScreenRaycaster
{
    layer: number;

    /**
     *  limit depthmap near/far to get preciser unporject
     */
    depthMapNear: number = 0.8;
    depthMapFar: number = 50;

    private normalDepthMapRenderTarget: THREE.WebGLRenderTarget;
    private normalDepthMapMaterial: THREE.ShaderMaterial;
    private scene: THREE.Scene;
    private _camera: THREE.PerspectiveCamera;
    private _renderer: THREE.WebGLRenderer;
    private height: number;
    private canvas: HTMLCanvasElement;
    private registedobjects = [];

    get texture()
    {
        return this.normalDepthMapRenderTarget.texture;
    }

    constructor( scene: THREE.Scene, canvas: HTMLCanvasElement, renderer: THREE.WebGLRenderer, camera: THREE.Camera, layer: number )
    {
        // debug
        // this._renderer = renderer;

        // work better on safari
        this._renderer = new THREE.WebGLRenderer();
        this._camera = camera as THREE.PerspectiveCamera;
        this.scene = scene;
        this.canvas = canvas;
        this.onResize()
        this.layer = layer;

        window.addEventListener( 'resize', this.onResize.bind( this ), false );
    }

    setcam( near: number, far: number )
    {
        this._camera.near = near;
        this._camera.far = far;
        this._camera.updateProjectionMatrix();
    }

    onResize()
    {
        const { width, height } = this.canvas.getBoundingClientRect()
        const canvasWidth = width * window.devicePixelRatio;
        const canvasHeight = height * window.devicePixelRatio;
        this.height = canvasHeight;
        if ( this.normalDepthMapRenderTarget )
            this.normalDepthMapRenderTarget.setSize( canvasWidth, canvasHeight );
        else
            this.normalDepthMapRenderTarget = new THREE.WebGLRenderTarget( canvasWidth, canvasHeight );
    }

    public registerObject( custumObject3D )
    {
        const objects = [].concat( custumObject3D || [] );
        for ( var i = 0; i < objects.length; i++ )
        {
            const { raycastTarget } = objects[ i ];
            if ( !raycastTarget ) continue;
            this.registedobjects.push( objects[ i ] )
            objects[ i ].enableRaycasterLayer( this.layer );
        }
    }

    public unRegistObject( custumObject3D )
    {
        const objects = [].concat( custumObject3D || [] );
        for ( var i = 0; i < objects.length; i++ )
        {
            const { raycastTarget } = objects[ i ];
            if ( !raycastTarget ) continue;
            const index = this.registedobjects.indexOf( objects[ i ] )
            this.registedobjects.splice( index, 1 )
        }
    }

    getNormalAndPosition( mouse )
    {

        this.update( this.scene )
        const mouseX = mouse.x * window.devicePixelRatio;
        const mouseY = this.height - mouse.y * window.devicePixelRatio;

        const pixelBuffer = new Uint8Array( 4 );
        this._renderer.readRenderTargetPixels( this.normalDepthMapRenderTarget, mouseX, mouseY, 1, 1, pixelBuffer );

        /**
         *  normal value not accurate make object rotate weird
         * (0.991,0.003,0.003) -> (1.0,0.0,0.0)
         */
        function round( float, digit = 2 )
        {
            return Math.round( float * 10 ** digit ) / 10 ** digit;
        }
        const normalX = round( pixelBuffer[ 0 ] / 255 - 0.5 );
        const normalY = round( pixelBuffer[ 1 ] / 255 - 0.5 );
        const normalZ = round( pixelBuffer[ 2 ] / 255 - 0.5 );

        const normal = new THREE.Vector3( normalX, normalY, normalZ ).normalize()
        const rotationMat4 = new THREE.Matrix4().lookAt( normal, new THREE.Vector3( 0, 0, 0 ), this._camera.up );

        let fragmentZ = pixelBuffer[ 3 ] / 255
        if ( fragmentZ === 0 )
        {
            return
        }

        // const css = `background: rgba(${pixelBuffer[0]},${pixelBuffer[1]},${pixelBuffer[2]},${1.0});`
        // console.log(`${pixelBuffer[3]} %c RaycastColor`, css)
        var rect = this.canvas.getBoundingClientRect();
        const screenX = ( mouse.x - rect.left ) / this.canvas.clientWidth * 2 - 1;
        const screenY = - ( mouse.y - rect.top ) / this.canvas.clientHeight * 2 + 1;

        const prevNear = this._camera.near
        const prevFar = this._camera.far
        this.setcam( this.depthMapNear, this.depthMapFar );
        const worldPosition = new THREE.Vector3( screenX, screenY, fragmentZ * 2 - 1 ).unproject( this._camera );
        this.setcam( prevNear, prevFar );

        return { rotationMat4, worldPosition }
    }

    update( scene: THREE.Scene )
    {

        if ( !this.normalDepthMapRenderTarget ) return;

        this.registedobjects.forEach( registObj =>
        {
            registObj.renderNormalAndDepth( true );
        } )

        const prevNear = this._camera.near
        const prevFar = this._camera.far
        this.setcam( this.depthMapNear, this.depthMapFar );
        this._camera.layers.set( this.layer );
        this._renderer.setRenderTarget( this.normalDepthMapRenderTarget );
        this._renderer.clear();
        this._renderer.render( scene, this._camera );

        this.registedobjects.forEach( registObj =>
        {
            registObj.renderNormalAndDepth( false );
        } )

        this.setcam( prevNear, prevFar );
        this._camera.layers.set( 0 );
        this._renderer.setRenderTarget( null );
        scene.overrideMaterial = null;
    }
} 