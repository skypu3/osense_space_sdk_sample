import * as THREE from 'three'
import Global from '../base/Global';
import MediaObject3D from '../mediaObject/MediaObject3D';
export default class ObjectPicker
{
    layer: number;

    private colorRenderTarget: THREE.WebGLRenderTarget;

    private canvas: HTMLCanvasElement;
    private _camera: THREE.Camera;
    private _renderer: THREE.WebGLRenderer;
    private height: number;

    get texture()
    {
        return this.colorRenderTarget.texture;
    }

    private _colorIndex: number = 1;
    private colorObjectDict: { [ key: string ]: MediaObject3D } = {};


    public registerObject( mediaObject3D: MediaObject3D )
    {
        const objects: MediaObject3D[] = [].concat( mediaObject3D || [] );
        for ( var i = 0; i < objects.length; i++ )
        {
            const vaild = objects[ i ].setRaycastMaterial( this.layer, this._colorIndex );
            if ( !vaild ) continue;

            this.colorObjectDict[ this._colorIndex ] = objects[ i ];
            this._colorIndex++
        }
    }

    constructor( canvas: HTMLCanvasElement, renderer: THREE.WebGLRenderer, camera: THREE.Camera, layer: number )
    {
        // this._renderer = renderer;

        // work better on safari
        this._renderer = new THREE.WebGLRenderer();

        this._camera = camera;
        this.canvas = canvas;
        this.layer = layer;
        this.onResize()
        window.addEventListener( 'resize', this.onResize.bind( this ), false );
    }

    onResize()
    {
        const { width, height } = this.canvas
        this.height = height;
        if ( this.colorRenderTarget )
            this.colorRenderTarget.setSize( width, height );
        else
            this.colorRenderTarget = new THREE.WebGLRenderTarget( width, height );
    }

    public getObject( mouse )
    {

        const mouseX = mouse.x * window.devicePixelRatio * Global.inst.scaleDeviceRatio;
        const mouseY = this.height - mouse.y * window.devicePixelRatio * Global.inst.scaleDeviceRatio;

        const pixelBuffer = new Uint8Array( 4 );
        this._renderer.readRenderTargetPixels( this.colorRenderTarget, mouseX, mouseY, 1, 1, pixelBuffer );
        const id = ( pixelBuffer[ 0 ] << 16 ) | ( pixelBuffer[ 1 ] << 8 ) | ( pixelBuffer[ 2 ] );
        return this.colorObjectDict[ id ];
    }

    update( scene: THREE.Scene )
    {
        if ( !this.colorRenderTarget ) return;

        this._camera.layers.set( this.layer )
        this._renderer.setRenderTarget( this.colorRenderTarget );
        this._renderer.clear();
        this._renderer.render( scene, this._camera );

        this._camera.layers.set( 0 )
        this._renderer.setRenderTarget( null );
        scene.overrideMaterial = null;
    }
}