import * as THREE from 'three';
import ScreenRaycaster from 'core/three/SceneRaycaster/ScreenRaycaster';
import ObjectPicker from 'core/three/SceneRaycaster/ObjectPicker';
import { RaycastClickData } from '../mediaObject/RaycastObject';
import MediaObject3D from '../mediaObject/MediaObject3D';
import Global from '../base/Global';
import { Vector3 } from 'three';
import store from 'core/store/store';
import { ViewerMode } from 'core/store/actions/mode';
import { ChatType } from 'core/store/actions/chat';

export interface IRaycastResponse
{
    destroy: any
    target: RaycastClickData,
    rotation: THREE.Matrix4,
    position: THREE.Vector3,
}

export enum RaycastActionType
{
    BOTH,
    SCREEN,
    OBJECT
}

export default class SceneRaycaster
{
    latestHoverObject: MediaObject3D;
    screenRaycaster: ScreenRaycaster;
    objectPicker: ObjectPicker;

    private isMouseUpThreshold: number = 10;
    private mouseDownCoord: THREE.Vector2;
    private mouse: THREE.Vector2;
    private camera: THREE.Camera

    public mouseUp?( info: IRaycastResponse ): void;
    public mouseMove?( res: IRaycastResponse | undefined ): void;
    public mouseDown?( res: IRaycastResponse | undefined ): void;
    public mouseEnter( obj: MediaObject3D ) { }
    public mouseLeave( obj: MediaObject3D ) { }

    constructor(
        scene: THREE.Scene,
        canvas: HTMLCanvasElement,
        renderer: THREE.WebGLRenderer,
        camera: THREE.Camera,
        objlayer: number,
        positionlayer: number
    )
    {
        this.screenRaycaster = new ScreenRaycaster(
            scene,
            canvas,
            renderer,
            camera,
            positionlayer
        )
        this.objectPicker = new ObjectPicker(
            canvas,
            renderer,
            camera,
            objlayer
        )

        this.mouseDownCoord = new THREE.Vector2();
        this.mouse = new THREE.Vector2();

        canvas.addEventListener( 'touchmove', this.onMouseMove.bind( this ), false );
        canvas.addEventListener( 'mousemove', this.onMouseMove.bind( this ), false );
        canvas.addEventListener( 'mousedown', this.onMouseDown.bind( this ), false );
        canvas.addEventListener( 'mouseup', this.onMouseUp.bind( this ), false );
        canvas.addEventListener( 'touchend', this.onMouseUp.bind( this ), false );
        canvas.addEventListener( 'touchstart', this.onMouseDown.bind( this ), false );
        document.addEventListener( "keydown", this.onKeyDown.bind( this ), false );

        this.camera = camera
    }

    onKeyDown( event: KeyboardEvent )
    {
        const mode = store.getState().modeReducer;
        if ( mode !== ViewerMode.Viewer ) return;
        const chatMode = store.getState().chatReducer as ChatType;
        if (chatMode.openChat || chatMode.privateChat) return;
        
        const animationMgr = Global.inst.animationMgr
        const currentHotspot = animationMgr.hotspotdict[ animationMgr.latestHotspotId ]
        let moveDirection = this.camera.getWorldDirection( new THREE.Vector3() )
        
        switch ( event.code )
        {
            case 'KeyW': // w
                break;

            case 'KeyA': // a
                moveDirection.applyAxisAngle(
                    new THREE.Vector3( 0, 1, 0 ), Math.PI / 2
                )
                break;

            case 'KeyS': // a
                moveDirection.applyAxisAngle(
                    new THREE.Vector3( 0, 1, 0 ), Math.PI
                )
                break;

            case 'KeyD': // a
                moveDirection.applyAxisAngle(
                    new THREE.Vector3( 0, 1, 0 ), -Math.PI / 2
                )
                break;
            default:
                moveDirection = undefined;
                break;
        }

        if (!moveDirection) return;

        const hotspots = Global.inst.animationMgr.hotspotdict;
        let hotspotKeys = Object.keys( hotspots );
        let minDistance = Infinity;
        let nearestId = undefined;
        hotspotKeys.forEach( hotspot =>
        {
            const info = animationMgr.hotspotdict[ hotspot ]
            const { position, floor, id } = info;
            if (!currentHotspot || floor !== currentHotspot.floor || id === animationMgr.latestHotspotId ) return;

            // only neighbor can move !!
            const { neighbors } = currentHotspot
            if ( neighbors && !neighbors.includes( id ) ) return;

            var dir = new THREE.Vector3( position.x - this.camera.position.x, 0, position.z - this.camera.position.z ); // create once an reuse it
            const angle = Math.abs( dir.angleTo( moveDirection ) );

            if ( angle < 0.6 )
            {
                const pos = new Vector3( currentHotspot.position.x, currentHotspot.position.y, currentHotspot.position.z )
                const distance = pos.distanceTo( new THREE.Vector3( position.x, position.y, position.z ) )

                if ( distance < minDistance )
                {
                    minDistance = distance;
                    nearestId = id
                }
            }
        } );

        if ( nearestId )
            animationMgr.goHotspot( nearestId )
    }

    updateMouse( event, mouse: THREE.Vector2 )
    {
        if ( event.type.includes( 'touch' ) )
        {
            mouse.x = event.changedTouches[ 0 ].clientX;
            mouse.y = event.changedTouches[ 0 ].clientY;
        } else
        {
            mouse.x = event.clientX;
            mouse.y = event.clientY;
        }
    }

    onMouseDown( event )
    {
        if ( this.mouseDown !== undefined )
        {
            this.mouseDown( undefined )
        }
        this.updateMouse( event, this.mouseDownCoord );
    }

    onMouseUp( event )
    {
        this.updateMouse( event, this.mouse );
        if ( this.mouseDownCoord.distanceTo( this.mouse ) > this.isMouseUpThreshold ) return;
        const object = this.objectPicker.getObject( this.mouse );
        const info = this.screenRaycaster.getNormalAndPosition( this.mouse );

        if ( object && info )
        {
            this.mouseUp( {
                destroy: object.destroy.bind( object ),
                target: object.targetInfo,
                rotation: info.rotationMat4,
                position: info.worldPosition,
            } )
        } else
        {
            this.mouseUp( undefined )
        }
    }

    onMouseMove( event )
    {
        this.updateMouse( event, this.mouse );
        const info = this.screenRaycaster.getNormalAndPosition( this.mouse );
        const object = this.objectPicker.getObject( this.mouse );
        if ( object !== this.latestHoverObject )
        {
            object?.onMouseEnter();
            object && this.mouseEnter( object )
            this.latestHoverObject?.onMouseLeave();
            this.latestHoverObject && this.mouseLeave( this.latestHoverObject )
            this.latestHoverObject = object;
        }
        if ( info )
        {
            this.mouseMove( {
                destroy: undefined,
                target: object ? object.targetInfo : undefined,
                rotation: info.rotationMat4,
                position: info.worldPosition,
            } );
        }
        else
        {
            this.mouseMove( undefined )
        }
    }

    get normalMap()
    {
        return this.screenRaycaster.texture;
    }

    get indexMap()
    {
        return this.objectPicker.texture;
    }

    addTarget( mediaObject3D, type = RaycastActionType.OBJECT )
    {
        if (type === RaycastActionType.BOTH) {
            this.screenRaycaster.registerObject( mediaObject3D );
            this.objectPicker.registerObject( mediaObject3D );
        } else if (type === RaycastActionType.SCREEN) {
            this.screenRaycaster.registerObject( mediaObject3D );
        } else {
            this.objectPicker.registerObject( mediaObject3D );
        }
    }

    private _perfromance_counter = 0;
    private _perfromance_loop = 7;
    update( scene )
    {
        const remainder = this._perfromance_counter % this._perfromance_loop;
        if ( remainder === 0 )
        {
            this.screenRaycaster.update( scene );
            this.objectPicker.update( scene );
        }
        this._perfromance_counter++;
    }

}