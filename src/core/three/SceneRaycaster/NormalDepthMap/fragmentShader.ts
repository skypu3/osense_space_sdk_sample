// normal map 公式：https://community.khronos.org/t/how-can-i-reduce-the-vertices-data-when-doing-flat-shading/107761
export default `
varying vec3 pos;

void main() {
    vec3 normal = normalize(cross(dFdx(pos), dFdy(pos)));

    float z = gl_FragCoord.z;  
    
    gl_FragColor = vec4(normal*0.5+0.5, z);
}
`;

