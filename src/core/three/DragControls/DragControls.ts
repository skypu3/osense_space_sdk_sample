import
{
    EventDispatcher,
    Matrix4,
    Plane,
    Raycaster,
    Vector2,
    Vector3,
    Group
} from 'three';

// source code came from: https://github.com/mrdoob/three.js/blob/master/examples/jsm/controls/DragControls.js
export default class DragControls extends EventDispatcher
{

    _plane = new Plane();
    _raycaster = new Raycaster();

    _mouse = new Vector2();
    _offset = new Vector3();
    _intersection = new Vector3();
    _worldPosition = new Vector3();
    _inverseMatrix = new Matrix4();
    _intersections = [];
    _selected = null
    _hovered = null;
    _domElement = null;
    _objects = null;
    _camera = null;
    enabled = true;
    transformGroup = false;
    _raycastObjects = [];
    _recursive = true;
    _names = [];
    _bindGroupTransform = [];
    //
    constructor( _objects, _camera, _domElement, _recursive = true )
    {
        super();
        this._recursive = _recursive;
        this._objects = _objects;
        this._camera = _camera;
        this._raycastObjects = _objects
        this._domElement = _domElement;
        this.onPointerMove = this.onPointerMove.bind( this );
        this.onPointerDown = this.onPointerDown.bind( this );
        this.onPointerCancel = this.onPointerCancel.bind( this );
        this.onTouchMove = this.onTouchMove.bind( this );
        this.onTouchStart = this.onTouchStart.bind( this );
        this.onTouchEnd = this.onTouchEnd.bind( this );
        this.activate();
    }
    bindTransformGroup( names: string[] )
    {
        names.forEach( name =>
        {
            if ( this._names.includes( name ) )
            {
                this._bindGroupTransform.push( name );
            } else
            {
                console.error( name + " not found ,setRaycastObject before binding." );
            }
        } )
    }
    setRaycastObject( objectNames: string[] )
    {
        this._names = objectNames
        if ( this._objects.length === 0 )
        {
            return;

        }
        this._objects[ 0 ].traverse( ( object ) =>
        {
            const { name } = object;
            if ( this._names.includes( name ) )
            {
                this._raycastObjects.push( object );
            }
        } )
    }

    resetObject( objects )
    {
        this._objects = objects;
        this._raycastObjects = [];
        this.setRaycastObject( this._names );
    }

    activate()
    {
        this._domElement.addEventListener( 'pointermove', this.onPointerMove );
        this._domElement.addEventListener( 'pointerdown', this.onPointerDown );
        this._domElement.addEventListener( 'pointerup', this.onPointerCancel );
        this._domElement.addEventListener( 'pointerleave', this.onPointerCancel );
        // this._domElement.addEventListener( 'touchmove', this.onTouchMove );
        // this._domElement.addEventListener( 'touchstart', this.onTouchStart );
        // this._domElement.addEventListener( 'touchend', this.onTouchEnd );

    }
    deactivate()
    {

        this._domElement.removeEventListener( 'pointermove', this.onPointerMove );
        this._domElement.removeEventListener( 'pointerdown', this.onPointerDown );
        this._domElement.removeEventListener( 'pointerup', this.onPointerCancel );
        this._domElement.removeEventListener( 'pointerleave', this.onPointerCancel );
        // this._domElement.removeEventListener( 'touchmove', this.onTouchMove );
        // this._domElement.removeEventListener( 'touchstart', this.onTouchStart );
        // this._domElement.removeEventListener( 'touchend', this.onTouchEnd );
        this._domElement.style.cursor = '';

    }
    dispose()
    {

        this.deactivate();

    }
    getObjects()
    {

        return this._objects;

    }
    onPointerMove( event )
    {
        event.preventDefault();

        switch ( event.pointerType )
        {

            case 'mouse':
            case 'pen':
                this.onMouseMove( event );
                break;

            // TODO touch
        }

    }

    onMouseMove( event )
    {

        var rect = this._domElement.getBoundingClientRect();

        this._mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
        this._mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;

        this._raycaster.setFromCamera( this._mouse, this._camera );

        if ( this._selected && this.enabled )
        {

            if ( this._raycaster.ray.intersectPlane( this._plane, this._intersection ) )
            {

                this._selected.position.copy( this._intersection.sub( this._offset ).applyMatrix4( this._inverseMatrix ) );

            }

            this.dispatchEvent( { type: 'drag', object: this._selected } );

            return;

        }

        this._intersections.length = 0;

        this._raycaster.setFromCamera( this._mouse, this._camera );
        this._raycaster.intersectObjects( this._raycastObjects, this._recursive, this._intersections );

        if ( this._intersections.length > 0 )
        {

            var object = this._intersections[ 0 ].object;

            this._plane.setFromNormalAndCoplanarPoint( this._camera.getWorldDirection( this._plane.normal ), this._worldPosition.setFromMatrixPosition( object.matrixWorld ) );

            if ( this._hovered !== object )
            {

                this.dispatchEvent( { type: 'hoveron', object: object } );

                this._domElement.style.cursor = 'pointer';
                this._hovered = object;

            }

        }
        else
        {

            if ( this._hovered !== null )
            {

                this.dispatchEvent( { type: 'hoveroff', object: this._hovered } );

                this._domElement.style.cursor = 'auto';
                this._hovered = null;

            }

        }

    }

    onPointerDown( event )
    {
        // event.preventDefault();
        switch ( event.pointerType )
        {

            case 'mouse':
            case 'pen':
                this.onMouseDown( event );
                break;

            // TODO touch

        }

    }

    onMouseDown( event )
    {

        // event.preventDefault();

        this._intersections.length = 0;

        this._raycaster.setFromCamera( this._mouse, this._camera );
        this._raycaster.intersectObjects( this._raycastObjects, this._recursive, this._intersections );
        if ( this._intersections.length > 0 )
        {
            this._selected = ( this.transformGroup === true ) ? this._objects[ 0 ] : this._intersections[ 0 ].object;
            if ( this._bindGroupTransform.includes( this._selected.name ) )
            {
                this._selected = this._objects[ 0 ];
            }
            if ( this._raycaster.ray.intersectPlane( this._plane, this._intersection ) )
            {

                this._inverseMatrix.copy( this._selected.parent.matrixWorld ).invert();
                this._offset.copy( this._intersection ).sub( this._worldPosition.setFromMatrixPosition( this._selected.matrixWorld ) );

            }

            this._domElement.style.cursor = 'move';

            this.dispatchEvent( { type: 'select', object: this._selected } );
            // console.log( "dragstart" )

        }

    }

    onPointerCancel( event )
    {

        event.preventDefault();
        switch ( event.pointerType )
        {

            case 'mouse':
            case 'pen':
                this.onMouseCancel( event );
                break;

            // TODO touch

        }

    }

    onMouseCancel( event )
    {

        event.preventDefault();

        if ( this._selected )
        {

            this.dispatchEvent( { type: 'dragend', object: this._selected } );
            // console.log( "dragend" )
            this._selected = null;

        }

        this._domElement.style.cursor = this._hovered ? 'pointer' : 'auto';

    }

    onTouchMove( event )
    {

        event.preventDefault();
        event = event.changedTouches[ 0 ];

        var rect = this._domElement.getBoundingClientRect();

        this._mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
        this._mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;

        this._raycaster.setFromCamera( this._mouse, this._camera );

        if ( this._selected && this.enabled )
        {

            if ( this._raycaster.ray.intersectPlane( this._plane, this._intersection ) )
            {

                this._selected.position.copy( this._intersection.sub( this._offset ).applyMatrix4( this._inverseMatrix ) );

            }

            this.dispatchEvent( { type: 'drag', object: this._selected } );

            return;

        }

    }

    onTouchStart( event )
    {

        event.preventDefault();
        event = event.changedTouches[ 0 ];

        var rect = this._domElement.getBoundingClientRect();

        this._mouse.x = ( ( event.clientX - rect.left ) / rect.width ) * 2 - 1;
        this._mouse.y = - ( ( event.clientY - rect.top ) / rect.height ) * 2 + 1;

        this._intersections.length = 0;

        this._raycaster.setFromCamera( this._mouse, this._camera );
        this._raycaster.intersectObjects( this._raycastObjects, this._recursive, this._intersections );

        if ( this._intersections.length > 0 )
        {

            this._selected = ( this.transformGroup === true ) ? this._objects[ 0 ] : this._intersections[ 0 ].object;

            this._plane.setFromNormalAndCoplanarPoint( this._camera.getWorldDirection( this._plane.normal ), this._worldPosition.setFromMatrixPosition( this._selected.matrixWorld ) );

            if ( this._raycaster.ray.intersectPlane( this._plane, this._intersection ) )
            {

                this._inverseMatrix.copy( this._selected.parent.matrixWorld ).invert();
                this._offset.copy( this._intersection ).sub( this._worldPosition.setFromMatrixPosition( this._selected.matrixWorld ) );

            }
            this._domElement.style.cursor = 'move';

            this.dispatchEvent( { type: 'dragstart', object: this._selected } );

        }


    }

    onTouchEnd( event )
    {

        event.preventDefault();

        if ( this._selected )
        {

            this.dispatchEvent( { type: 'dragend', object: this._selected } );

            this._selected = null;

        }

        this._domElement.style.cursor = 'auto';

    }
}