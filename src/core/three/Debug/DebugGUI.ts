import * as THREE from 'three';
import GUI from 'lil-gui'; 

export class DebugGUI {

    public static createNewDebugGUI(name: string): GUI {
        const gui = new GUI();
        let debugGUI = name ? gui.addFolder(name) : gui
        return gui;
    }

    public static createDebugGUI(
        object: THREE.Mesh | THREE.Group | THREE.Object3D,
        name?: string
    ): GUI {
        const gui = new GUI();
        let debugGUI = name ? gui.addFolder(name) : gui
        const positionFolder = debugGUI.addFolder('Position')
        for (let pos of ['x', 'y', 'z']) {
            positionFolder.add(object.position, pos)
                .name(pos)
                .min(-5.0)
                .max(5.0)
                .step(0.01);
        }

        const rotationFolder = debugGUI.addFolder('Rotation')
        for (let rot of ['x', 'y', 'z']) {
            rotationFolder.add(object.rotation, rot)
                .name(rot)
                .min(- 2 * Math.PI)
                .max(2 * Math.PI)
                .step(0.01)
        }

        const scaleFolder = debugGUI.addFolder('Scale')
        for (let scal of ['x', 'y', 'z']) {
            scaleFolder.add(object.scale, scal)
                .name(scal)
                .min(-5)
                .max(5)
                .step(0.01)
        }

        debugGUI.add(object, 'visible').name('visible')

        if (object instanceof THREE.Mesh) {
            let mesh = object as THREE.Mesh
            if (!(mesh.material instanceof Array)) {
                let material = mesh.material as THREE.Material
                debugGUI.add(mesh.material, 'wireframe')
                    .name('wireframe')
            }
        }

        return gui;
    }
}