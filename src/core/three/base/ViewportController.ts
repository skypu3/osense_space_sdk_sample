import * as THREE from 'three';
import OrbitControls from 'core/three/OrbitControls';
import { CSS3DRenderer } from 'three/examples/jsm/renderers/CSS3DRenderer';
import BlurController from './BlurController';
import { Quaternion, Scene } from 'three';
import Global from './Global';
import PostProcessingController from './postprocessing/PostProcessingController';
import BloomController from './bloom/BloomController';
import { Format } from '../scene/Format';
import { Tween } from "@tweenjs/tween.js";

const FACTOR = {
    INIT: {
        ANIMATIONANGLE: Math.PI / 1.5,
        ZOOM: 0.7,
        POLARANGLE: Math.PI / 4,
        // init animation Will rotate ANIMATIONANGLE;
        AZIANGLE: Math.PI / 10,
    },
    CONSTARINT: {
        ROTATESPEED: 0.05 * 0.8,
        PANSPEED: 0.05,
        DAMPING: 0.1,

        MAX_ZOOM: 2,
        MIN_ZOOM: 0.8,

        TOPVIEW: {
            minPolarAngle: 0,
            maxPolarAngle: Math.PI / 2,
        },
        FPVIEW: {
            minPolarAngle: 0,
            maxPolarAngle: Math.PI,
        },
        MAXPAN: 0.5,
    },
};

export default class ViewportController {
    public renderer: THREE.WebGLRenderer;
    private cssrenderer: CSS3DRenderer;
    private controls: OrbitControls;
    public camera: THREE.PerspectiveCamera;

    // blur effect
    public blurController: BlurController = null;
    public bloomController: BloomController = null;
    public postProcessingController: PostProcessingController = null;
    public isBlurEffecting: boolean = false;

    public onChangeCameraEvent?: ((oldCameraPose: Format.ICameraPose, newCameraPose: Format.ICameraPose) => void);

    private latestZoom: number;

    get OrbitControls() {
        return this.controls;
    }

    init(canvas: HTMLCanvasElement) {
        this.camera = new THREE.PerspectiveCamera(
            75,
            window.innerWidth / window.innerHeight,
            0.1,
            30000
        );
        this.camera.rotation.order = 'YXZ'
        this.latestZoom = this.camera.zoom;

        const controls = new OrbitControls(this.camera, canvas);
        // default zoom out
        controls.dollyOut(1.2);

        // 畫布設置
        this.renderer = new THREE.WebGLRenderer({
            canvas,
            alpha: true,
            antialias: true,
            powerPreference: "high-performance",
        });
        this.renderer.setPixelRatio(window.devicePixelRatio * Global.inst.scaleDeviceRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight, false);
        this.renderer.autoClear = false;
        this.renderer.setClearColor(0xffffff, 0.0);
        this.renderer.setScissorTest(true)
        this.renderer.physicallyCorrectLights = true;
        this.renderer.outputEncoding = THREE.sRGBEncoding;
        this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
        this.renderer.toneMappingExposure = 1;

        const width = window.innerWidth
        const height = window.innerHeight

        this.cssrenderer = new CSS3DRenderer();
        this.cssrenderer.setSize(
            width % 2 === 0 ? width : width - 1,
            height % 2 === 0 ? height : height - 1
        );

        this.cssrenderer.domElement.style.width = (width % 2 === 0 ? width : width - 1)+'px'
        this.cssrenderer.domElement.style.height= (height% 2 === 0 ? height: height- 1)+'px'
        this.cssrenderer.domElement.style.position = 'fixed';
        document.querySelector('#css3d').appendChild(this.cssrenderer.domElement);

        window.addEventListener('resize', this.windowResized.bind(this), false);

        // init camera
        this.camera.position.set(0, 10, 0);
        this.controls = controls;
        this.controls.target.set(0, 0, 0);
        this.controls.domElement = this.renderer.domElement;
        this.controls.enableDamping = true;

        this.controls.dampingFactor = FACTOR.CONSTARINT.DAMPING;
        this.controls.panSpeed = FACTOR.CONSTARINT.PANSPEED;
        this.controls.rotateSpeed = FACTOR.CONSTARINT.ROTATESPEED;

        this.controls.maxPolarAngle = Math.PI / 2;

        // set initial pose
        this.controls.minZoom = FACTOR.CONSTARINT.MIN_ZOOM;
        this.controls.maxZoom = FACTOR.CONSTARINT.MAX_ZOOM;

        this.polarAngle = FACTOR.INIT.POLARANGLE;
        this.azimuthAngle = FACTOR.INIT.AZIANGLE + FACTOR.INIT.ANIMATIONANGLE;
        this.controls.update();
        this.controls.onChangeRotate = (oldRotate: Format.iCameraRotate, newRotate: Format.iCameraRotate) => {
            if (this.onChangeCameraEvent) {
                let oldCameraPose: Format.ICameraPose = {
                    position: this.camera.position,
                    rotation: oldRotate,
                    zoom: this.camera.zoom,
                }
    
                let newCameraPose = {
                    ...oldCameraPose,
                    rotation: newRotate
                }

                this.onChangeCameraEvent(
                    oldCameraPose,
                    newCameraPose
                );
            }
        }

        this.controls.onZoom = () => {
            if (this.latestZoom === this.camera.zoom) {
                return;
            }

            if (this.onChangeCameraEvent) {
                let oldCameraPose = this.getCurrentPose();
                oldCameraPose.zoom = this.latestZoom;

                let newCameraPose = this.getCurrentPose();

                this.onChangeCameraEvent(
                    oldCameraPose,
                    newCameraPose
                );
            }
            this.latestZoom = this.camera.zoom;
        }

        // this.controls.panConstrain = ( target, offset ) =>
        // {
        //     const newTargetPos = new THREE.Vector3(
        //         target.x + offset.x,
        //         target.y + offset.y,
        //         target.z + offset.z
        //     );

        //     return newTargetPos.length() <= FACTOR.CONSTARINT.MAXPAN;
        // };

        // animation factor
        // this.controlBackVecDistance = 7;
        // this.controlBackVecHeight = 5;

        return this;
    }

    createPostProcessController(scene: Scene, conf: Format.IPostProcessing) {
        const postProcessingController = new PostProcessingController(scene, this.camera, this.renderer, conf);
        // this.renderer.toneMapping = THREE.ReinhardToneMapping;
        this.renderer.toneMappingExposure = 1;
        this.postProcessingController = postProcessingController;
    }

    createBloomController(scene: Scene) {
        const bloomController = new BloomController(scene, this.camera, this.renderer);
        // this.renderer.toneMapping = THREE.ReinhardToneMapping;
        this.renderer.toneMappingExposure = 1;
        this.bloomController = bloomController;
    }

    windowResized() {
        if (this.camera != null) {
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix();
        }
        this.renderer.setSize(window.innerWidth, window.innerHeight, false);
        this.cssrenderer.setSize(window.innerWidth, window.innerHeight);

        if (this.bloomController) {
            this.bloomController.setSize();
        }

        if (this.postProcessingController) {
            this.postProcessingController.setSize();
        }
    };

    setFocusBbox(center: THREE.Vector3, size3D: THREE.Vector3) {
        const size = Math.max(size3D.x, size3D.y, size3D.z);

        this.setLookAt(center);
        this.camera.position.set(center.x, center.y + size, center.z);
        // this.controlBackVecDistance = 0.7 * size;
        // this.controlBackVecHeight = 0.5 * size;
        // this.orthogonalViewHeight = size;
        // this.controls.panConstrain = ( target, offset ) =>
        // {
        //     const newTargetPos = new THREE.Vector3(
        //         target.x + offset.x,
        //         target.y + offset.y,
        //         target.z + offset.z
        //     );

        //     return newTargetPos.length() <= size;
        // };
    }

    render(scene: THREE.Scene) {
        if (this.controls && this.renderer) {
            const w = window.innerWidth, h = window.innerHeight;
            this.controls.update();
            this.renderer.setRenderTarget(null);
            this.cssrenderer.render(scene, this.camera);
            this.renderer.setScissor(0, 0, w, h);
            this.renderer.setViewport(0, 0, w, h)
            if (this.isBlurEffecting && this.blurController) {
                this.blurController.composerRender();
            }
            else if (this.bloomController) {
                this.bloomController.composerRender()
            }
            else if (this.postProcessingController) {
                this.postProcessingController.composerRender()
            }
            else {
                this.renderer.render(scene, this.camera)
            }
        }
        // const { calls } = this.renderer.info.render;
        // const { triangles } = this.renderer.info.render;
        // console.log(`Drawcall:${calls}`, `Triangles:${triangles}`);
    }

    setOrthographicLimit(pos: THREE.Vector3) {
        this.controls.target.set(pos.x, pos.y, pos.z);

        this.controls.minPolarAngle = 0;
        this.controls.maxPolarAngle = 0;
        this.controls.rotateSpeed = FACTOR.CONSTARINT.ROTATESPEED;
        this.controls.enableDamping = true;
        this.controls.enableRotate = true;

        this.controls.update();
    }

    setTopViewLimit(pos: THREE.Vector3) {
        this.controls.target.set(pos.x, pos.y, pos.z);

        this.controls.minPolarAngle = FACTOR.CONSTARINT.TOPVIEW.minPolarAngle;
        this.controls.maxPolarAngle = FACTOR.CONSTARINT.TOPVIEW.maxPolarAngle;

        this.controls.rotateSpeed = FACTOR.CONSTARINT.ROTATESPEED;

        this.controls.enableDamping = true;
        this.controls.enableRotate = true;

        this.controls.update();
    }

    setFirstpersonViewLimit() {
        const vec = new THREE.Vector3(0, 0, -0.00001);
        vec.applyQuaternion(this.camera.quaternion);
        this.controls.target.set(
            this.camera.position.x + vec.x,
            this.camera.position.y + vec.y,
            this.camera.position.z + vec.z
        );

        this.controls.minPolarAngle = FACTOR.CONSTARINT.FPVIEW.minPolarAngle;
        this.controls.maxPolarAngle = FACTOR.CONSTARINT.FPVIEW.maxPolarAngle;

        this.controls.rotateSpeed = -FACTOR.CONSTARINT.ROTATESPEED;

        this.controls.enableDamping = true;
        this.controls.enableRotate = true;

        this.controls.update();
    }

    setGoinglimit(dstIsTop: boolean) {
        const vec = new THREE.Vector3(0, 0, -0.001);
        vec.applyQuaternion(this.camera.quaternion);
        this.controls.target.set(
            this.camera.position.x + vec.x,
            this.camera.position.y + vec.y,
            this.camera.position.z + vec.z
        );

        this.controls.minPolarAngle = 0;
        this.controls.maxPolarAngle = Math.PI;

        this.controls.rotateSpeed =
            (dstIsTop ? 1 : -1) * FACTOR.CONSTARINT.ROTATESPEED;
        this.controls.enableRotate = !dstIsTop;
        this.controls.enableDamping = !dstIsTop;

        this.controls.update();
    }

    setCamerPos(newPos: THREE.Vector3, holdDirection: boolean) {
        const oldPos = new THREE.Vector3().copy(this.camera.position);

        const deltaPos = new THREE.Vector3(
            newPos.x - oldPos.x,
            newPos.y - oldPos.y,
            newPos.z - oldPos.z
        );

        this.camera.position.x += deltaPos.x;
        this.camera.position.y += deltaPos.y;
        this.camera.position.z += deltaPos.z;

        if (holdDirection === true) {
            this.controls.target.x += deltaPos.x;
            this.controls.target.y += deltaPos.y;
            this.controls.target.z += deltaPos.z;
        }
    }

    setLookAt(newLookAt: THREE.Vector3) {
        this.controls.target.set(newLookAt.x, newLookAt.y, newLookAt.z);
    }

    set polarAngle(newValue: number) {
        const oldmin = this.controls.minPolarAngle;
        const oldmax = this.controls.maxPolarAngle;

        this.controls.minPolarAngle = newValue;
        this.controls.maxPolarAngle = newValue;
        this.controls.update();

        this.controls.minPolarAngle = oldmin;
        this.controls.maxPolarAngle = oldmax;
        this.controls.update();
    }

    set azimuthAngle(newValue: number) {
        const oldmin = this.controls.minAzimuthAngle;
        const oldmax = this.controls.maxAzimuthAngle;

        // fix coordinate , origion coordinate range is [-Math.PI,-0] [0,Math.PI] and ±Math.PI are neighbors
        // new value could be [ 0 , 2*Math.PI ]  or [ -0 , -2*Math.PI ]
        let fixCoordinate = newValue;

        if (fixCoordinate > Math.PI) {
            fixCoordinate = newValue - Math.PI + -Math.PI;
        } else if (fixCoordinate < -Math.PI) {
            fixCoordinate = newValue + Math.PI + Math.PI;
        }

        this.controls.minAzimuthAngle = fixCoordinate;
        this.controls.maxAzimuthAngle = fixCoordinate;
        this.controls.update();

        this.controls.minAzimuthAngle = oldmin;
        this.controls.maxAzimuthAngle = oldmax;
        this.controls.update();
    }

    get polarAngle() {
        return this.controls.getPolarAngle();
    }

    get azimuthAngle() {
        return this.controls.getAzimuthalAngle();
    }

    setLookAtCenter(target: THREE.Vector3) {
        this.controls.target.set(target.x, target.y, target.z);
    }

    getLookAtCenter() {
        return this.controls.target;
    }

    getCamerPosition() {
        const cameraWorldPos = new THREE.Vector3();
        this.camera.getWorldPosition(cameraWorldPos);
        return cameraWorldPos;
    }

    getCameraQuaternion() {
        return this.camera.quaternion.clone();
    }

    getSrceenCoordinate(position) {
        const screenCoord = position.clone().project(this.camera);
        screenCoord.x = (screenCoord.x + 1) / 2;
        screenCoord.y = -(screenCoord.y - 1) / 2;

        if (
            screenCoord.x > 0 &&
            screenCoord.x < 1 &&
            screenCoord.y > 0 &&
            screenCoord.y < 1 &&
            screenCoord.z < 1.0 &&
            screenCoord.z > -1.0
        ) {
            return screenCoord;
        } else {
            return undefined;
        }
    }

    get screenHeight() {
        const srceenY =
            Math.tan(this.fov / 2) *
            new THREE.Vector3()
                .subVectors(this.getCamerPosition(), this.getLookAtCenter())
                .length() *
            2;
        return srceenY;
    }

    set fov(value: number) {
        this.camera.fov = (value / Math.PI) * 180;
        this.camera.updateProjectionMatrix();
    }

    get fov() {
        this.camera.updateProjectionMatrix();
        return (this.camera.fov / 180) * Math.PI;
    }

    public setCameraZoom(value: number, duration?: number): Promise<void> {
        let fixValue = value;
        if (value >= FACTOR.CONSTARINT.MAX_ZOOM) {
            fixValue = FACTOR.CONSTARINT.MAX_ZOOM
        } else if (value <= FACTOR.CONSTARINT.MIN_ZOOM) {
            fixValue = FACTOR.CONSTARINT.MIN_ZOOM;
        }

        return new Promise<void>((resolve) => {
            let tween = new Tween(this.camera)
                .to({ zoom: fixValue }, duration ? duration : 1500)
                .onUpdate(() => {
                    this.camera.updateProjectionMatrix();
                })
                .onComplete(() => {
                    resolve()
                })
                .start()
        })

    }

    private getCurrentPose(): Format.ICameraPose {
        return {
            position: this.camera.position.clone(),
            rotation: {
                azimuthAngle: this.azimuthAngle,
                polarAngle: this.polarAngle,
            },
            zoom: this.camera.zoom,
        };
    }
}
