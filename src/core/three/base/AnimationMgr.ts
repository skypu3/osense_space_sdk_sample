import * as THREE from 'three'
import { Format, BehaviorMode } from 'core/three/scene/Format'
import AnimeViewportController from 'core/three/base/AnimeViewportController'
import AnimeRoomModel from 'core/three/mediaObject/roomModel/AnimeRoomModel';
import Animator from 'core/anime/Animator';
import Timeline from 'core/anime/Timeline';
import { ResourcesType } from "core/three/base/ResourcesMgr";
import Global from 'core/three/base/Global';
import Hotspot from '../mediaObject/hotspot/Hotspot';
import { Vector3 } from 'three';
import store from 'core/store/store';
import { ViewerMode } from 'core/store/actions/mode';
import { SpaceSdkCmd } from 'core/store/actions/iframe';
import Media from '../mediaObject/media/Media';

interface AnimationDuration
{
    FAST: number;
    GOHOTSPOT: number,
    GOTOP: number,
    GODOWN: number,
    BY_DISTANCE_DURATION_FACTOR: number,
    BYDISTANCE: Function,
}

const defaultAnimationDuration: AnimationDuration =
{
    FAST: 500,
    GOHOTSPOT: 2000,
    GOTOP: 1500, // go top default duration
    GODOWN: 2200, // go down default duration
    BY_DISTANCE_DURATION_FACTOR: 300, // by distance duration factor

    // calc the duration by distance
    BYDISTANCE: function ( number: number )
    {
        return number * defaultAnimationDuration.BY_DISTANCE_DURATION_FACTOR;
    }
}

interface hotspotdict
{
    [ key: string ]: Format.IHotspot;
}

// 最多preload多少低解析度的cubemap站點
const maximumPreload = 10;

export default class AnimationMgr
{
    public latestHotspotId: string;
    public currentFloor: number;
    public currentMoveHotspotId: string;
    private animationMesh: AnimeRoomModel;
    private camControls: AnimeViewportController;

    public hotspotdict: hotspotdict;
    public hotspotIdList: string[]
    private hotspotMeshList: Hotspot[];

    private animator: Animator;
    private showPanoramaMesh: boolean = true;
    private animationDuration: AnimationDuration; // animation duration object
    public enableCalcDurationByDistance: boolean = true; // enable calculate duration by distance
    public enableBlurEffectAnimation: boolean = false; // enable blur effect when moving
    private preLoadHotspot = {};
    private objectNearHotspot = {};

    public latestCameraPose: Format.ICameraPoseInfo;

    public enableGoHotspot = true;
    public enableUserInteractHotspots = true;

    private _animationID: number | null;
    private _animateFuncList: Function[];

    set enablePanoramaMesh( option: boolean )
    {
        this.showPanoramaMesh = option;
        this.animationMesh.transitionPercentage = option ? 0 : 1;
    }

    get enablePanoramaMesh()
    {
        return this.showPanoramaMesh;
    }

    constructor()
    {
        this.hotspotdict = {};
        this.hotspotMeshList = [];
        this.animator = new Animator();
        this.animationDuration = defaultAnimationDuration;
        this._animateFuncList = [];
    }

    public init( camControls: AnimeViewportController, animationMesh: AnimeRoomModel )
    {
        this.camControls = camControls
        this.animationMesh = animationMesh;

        // 觀察相機轉動事件
        this.camControls.onChangeCameraEvent = ( oldCameraPose: Format.ICameraPose, newCameraPose: Format.ICameraPose ) =>
        {

            let oldCamera = this.getCurrentPoseInfo();
            oldCamera.position = oldCameraPose.position;
            oldCamera.rotation = oldCameraPose.rotation;
            oldCamera.zoom = oldCameraPose.zoom;

            let newCamera = this.getCurrentPoseInfo();
            newCamera.position = newCameraPose.position;
            newCamera.rotation = newCameraPose.rotation;
            newCamera.zoom = newCameraPose.zoom;

            if ( store.getState().iframeReducer !== null )
            {
                const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
                send( SpaceSdkCmd.cameraupdated, {
                    oldCamera,
                    newCamera
                } );
            } else
            {
                // console.log(`camera changed pose to ${newCamera} from ${oldCamera}`);
            }
        }
    }

    /**
     * 
     * @param goHotspotDuration go to hotspot duration
     * @param goDownDuration go down duration (to hotspot)
     * @param goTopDuration go top duration
     * @param byDistanceFactor calc duration by distance factor
     */
    public setAnimationDurationParameters( goHotspotDuration?, goDownDuration?, goTopDuration?, byDistanceFactor?)
    {
        if ( goHotspotDuration )
            this.animationDuration.GOHOTSPOT = goHotspotDuration;
        if ( goDownDuration )
            this.animationDuration.GODOWN = goDownDuration;
        if ( goTopDuration )
            this.animationDuration.GOTOP = goTopDuration;
        if ( byDistanceFactor )
            this.animationDuration.BY_DISTANCE_DURATION_FACTOR = byDistanceFactor;
    }

    /**
     * 
     * @param id  find neighbor list if not found set all hotspot to invisible
     */
    public displayHotspotNeighbors( id )
    {
        const mode = store.getState().modeReducer as ViewerMode;
        if ( mode === ViewerMode.Editor )
        {
            this.hotspotMeshList.forEach( hotspot =>
            {
                hotspot.visible = true;
            } )
            return;
        }

        this.hotspotMeshList.forEach( hotspot =>
        {
            hotspot.visible = false;
        } )
        if ( this.hotspotdict[ id ] )
        {
            let { neighbors } = this.hotspotdict[ id ]
            this.hotspotMeshList.forEach( hotspot =>
            {
                const { id } = hotspot;

                if ( !neighbors )
                {
                    hotspot.visible = false;
                    return
                }

                if ( neighbors.length === 0 || neighbors.includes( id ) )
                {
                    hotspot.visible = true;
                } else
                {
                    hotspot.visible = false;
                }
            } )
        }
    }

    public initHotspotInfos( hotspots: Format.IHotspot[], hotspotMeshs: Hotspot[] )
    {
        hotspots.forEach( hotspot =>
        {
            this.hotspotdict[ hotspot.id ] = hotspot;
        } )
        this.hotspotIdList = Object.keys( this.hotspotdict )
        this.hotspotMeshList = hotspotMeshs
    }

    public async godown()
    {
        if ( this.camControls.isTop )
        {

            let oldBehaviorMode = this.getBehaviorModeByCam();
            let oldCamera = this.getCurrentPoseInfo();
            oldCamera.hotspotId = "";

            await this.goHotspot( this.latestHotspotId )

            let newBehaviorMode = BehaviorMode.WalkingMode;
            let newCamera = this.getCurrentPoseInfo();

            if ( store.getState().iframeReducer !== null )
            {
                const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
                send( SpaceSdkCmd.setdollhousemode, false );

                send( SpaceSdkCmd.modeupdated, {
                    oldMode: oldBehaviorMode,
                    newMode: newBehaviorMode
                } )

                // godown 時，會觸發 gohotspot function ，就會觸發相機更新，所以這裡可不發
                // send(SpaceSdkCmd.cameraupdated, {
                //     oldCamera,
                //     newCamera
                // });

            }
            else
            {
                console.log( `user changed Behavior Mode to ${ newBehaviorMode } from ${ oldBehaviorMode }` );
                // console.log(`camera changed pose to ${newCamera} from ${oldCamera}`);
            }
        }
    }

    public async goTop()
    {
        if ( this.animator.isPlaying ) return;
        if ( this.animationMesh.hotspotid === undefined ) return;

        let oldBehaviorMode = this.getBehaviorModeByCam();
        let oldCamera = this.getCurrentPoseInfo();

        this.latestHotspotId = this.animationMesh.hotspotid
        const goHotspot = new Timeline( this.animationDuration.GOTOP )
        this.displayHotspotNeighbors( undefined )
        goHotspot.addAction( this.camControls.goTop() )
        goHotspot.addAction( this.animationMesh.goTop() )

        await this.animator.play( [ goHotspot ] )

        let newBehaviorMode = BehaviorMode.DollHouseMode;
        let newCamera = this.getCurrentPoseInfo();

        if ( store.getState().iframeReducer !== null )
        {
            const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
            send( SpaceSdkCmd.setdollhousemode, true );

            send( SpaceSdkCmd.modeupdated, {
                oldMode: oldBehaviorMode,
                newMode: newBehaviorMode
            } )

            send( SpaceSdkCmd.cameraupdated, {
                oldCamera,
                newCamera
            } );
        }
        else
        {
            console.log( `user changed Behavior Mode to ${ newBehaviorMode } from ${ oldBehaviorMode }` );
            // console.log(`camera changed pose to ${newCamera} from ${oldCamera}`);
        }
    }

    /**
     * @param hotspotId 
     */
    public async preloadHotspot( hotspotId )
    {
        const info = this.hotspotdict[ hotspotId ]
        if ( !info ) return;
        const { position } = info
        const dstPos = new THREE.Vector3( position.x, 0, position.z )

        let neighborData = info.neighbors
        const neighbors = [];
        if ( !neighborData )
        {
            neighborData = this.hotspotIdList
        }
        neighborData.forEach( ( hotspotId ) =>
        {
            const neighbor = this.hotspotdict[ hotspotId ]
            if ( neighbor )
            {
                const pos = new THREE.Vector3( neighbor.position.x, neighbor.position.y, neighbor.position.z )
                neighbors.push( { 'id': neighbor.id, 'distance': dstPos.distanceTo( pos ) } );
            }
        } );

        neighbors.sort( ( a, b ) => a.distance - b.distance );
        for ( let i = 0; i < neighbors.length; i++ )
        {
            const info = this.hotspotdict[ neighbors[ i ].id ];

            if ( this.preLoadHotspot[ neighbors[ i ].id ] !== undefined )
            {
                // do nothing
            } else
            {
                Global.inst.resLoad.load( info.lowerSkybox, ResourcesType.Cubemap ).then( ( texture ) =>
                {
                    this.preLoadHotspot[ neighbors[ i ].id ] = texture;
                } )
            }

            if ( i === maximumPreload )
                break;
        }
    }

    public async goFloorWithNumber( floor: number )
    {

        const currentHotspot = this.hotspotdict[ this.latestHotspotId ]
        if ( currentHotspot.floor === floor )
        {
            return
        }

        let hotspotKeys = Object.keys( this.hotspotdict );
        let minDistance = Infinity;
        let nearestId = this.latestHotspotId
        const pos = new Vector3( currentHotspot.position.x, 0, currentHotspot.position.z )
        hotspotKeys.forEach( key =>
        {
            const info = this.hotspotdict[ key ];
            if ( info.floor === floor )
            {
                const distance = pos.distanceTo( new THREE.Vector3( info.position.x, 0, info.position.z ) )
                if ( distance < minDistance )
                {
                    minDistance = distance;
                    nearestId = info.id
                }
            }
        } )

        if ( nearestId )
        {
            await this.goHotspot( nearestId );
        }
        else
            console.warn( 'nearest floor hotspot not found.' )
    }

    /**
     * Walking and rotate to face the object
     * @param id 
     * @param srcPos 
     */
    public async goNearHotspotWithAngle( media: Media, goTop?: boolean, polarAngle?: number, id?: string )
    {
        let nearHotspotId = '';
        const dstPos = media.object.position

        if ( id && this.objectNearHotspot[ id ] )
        {
            nearHotspotId = this.objectNearHotspot[ id ];
        } else
        {
            const nearHotspotList = []
            var quaternion = new THREE.Quaternion()
            media.object.getWorldQuaternion( quaternion )
            var direction = new Vector3( 0, 0, -1 ).applyQuaternion( quaternion )
            let minDistance = Infinity;
            let closeHotspotId = ''

            for ( let key in this.hotspotdict )
            {
                const hotspot = this.hotspotdict[ key ]
                const distance = dstPos.distanceTo( hotspot.position as Vector3 )

                if ( minDistance > distance )
                {
                    minDistance = distance
                    closeHotspotId = key
                }
                if ( distance > 1 && distance <= 3.5 )
                {
                    nearHotspotList.push( key )
                }
            }

            if ( nearHotspotList.length == 0 )
            {
                nearHotspotList.push( closeHotspotId )
            }

            let minAngle
            nearHotspotList.forEach( ( key ) =>
            {
                const hotspot = this.hotspotdict[ key ]
                var dir = new THREE.Vector3(
                    dstPos.x - hotspot.position.x,
                    dstPos.y - hotspot.position.y,
                    dstPos.z - hotspot.position.z
                );

                const angle = Math.abs(dir.angleTo(direction));
                const heightDiff = dstPos.y - hotspot.position.y;
                // 限制高度差不能是負的
                if ((!minAngle || minAngle > angle) && heightDiff >= 0.0 )
                {
                    nearHotspotId = key
                    minAngle = angle
                }
                // create once an reuse it
            } )
        }
        if ( nearHotspotId !== '' )
        {
            if ( goTop )
                await this.goTop();
            const promises = [];
            promises.push( this.goHotspot( nearHotspotId ) )
            promises.push( this.faceObjectRotation( dstPos, this.hotspotdict[ nearHotspotId ].position as Vector3, polarAngle ) )
            await Promise.all( promises )
        }
    }

    /**
     * Face object by setting azimuth angle and optional polarAngle
     * @param position 
     * @param polarAngle 
     */
    public async faceObjectRotation( dst: THREE.Vector3, src?: THREE.Vector3, polarAngle?: number )
    {
        if ( !src )
        {
            src = this.hotspotdict[ this.latestHotspotId ].position as Vector3;
        }

        const aAngle = Math.atan2(
            src.x - dst.x,
            src.z - dst.z,
        );
        // const pAngle = Math.atan2(
        //     dst.y - src.y,
        //     new THREE.Vector2(
        //         dst.x - src.x,
        //         dst.z - src.z
        //     ).length(),
        // );

        if ( polarAngle !== undefined )
            await this.camControls.setOrbitControlRotation( aAngle, polarAngle )
        else
            await this.camControls.setOrbitControlRotation( aAngle, this.camControls.polarAngle )
    }

    /**
     * No animation when go to hotspot ...
     * @param hotspotId 
     * @param polarAngle 
     */
    public async goHotspotNoAnimation( hotspotId, polarAngle?)
    {
        const info = this.hotspotdict[ hotspotId ]
        if ( info )
        {
            const { position, cameraHeight, rotation, lowerSkybox, higherSkybox, id, previewUrl } = info

            const loadhighTexture = Global.inst.resLoad.load( higherSkybox, ResourcesType.Cubemap )

            const dstPos = new THREE.Vector3( position.x, position.y + cameraHeight, position.z )

            const lowerCubemap = await Global.inst.resLoad.load( lowerSkybox, ResourcesType.Cubemap );

            const hotspot = {
                id: id,
                position: dstPos,
                rotation: rotation,
                texture: lowerCubemap
            }

            this.animationMesh.hotspotid = hotspot.id;
            // lower cubemap...
            await this.animationMesh.panoMaterial.setTexture( hotspot )
            this.animationMesh.transitionPercentage = 0;

            this.camControls.camera.position.copy( dstPos );
            this.camControls.isTop = false;
            this.camControls.isHotspotAnimating = false;
            this.camControls.setFirstpersonViewLimit()

            if ( polarAngle !== undefined )
            {
                this.camControls.setOrbitControlRotation( this.camControls.azimuthAngle, polarAngle );
            }

            const texture = await loadhighTexture;
            this.displayHotspotNeighbors( id )
            this.animationMesh.setHdTexture( {
                id: id,
                position: dstPos,
                rotation: rotation,
                texture: texture
            } );

            this.latestHotspotId = hotspotId
            this.preloadHotspot( this.latestHotspotId );
        }
    }

    /**
     * 
     * @param hotspotId which hotspot you want to go
     * @param position in case invalid hotspot id
     */
    public async goHotspot(
        hotspotId: string,
        position?: THREE.Vector3,
        polarAngle?: number,
        azimuthAngle?: number,
        duration?: number,
        zoom?: number
    )
    {

        if ( !this.enableGoHotspot )
        {
            return
        }

        if ( !this.camControls.isTop && this.latestHotspotId && ( this.latestHotspotId === hotspotId && hotspotId ) ) return

        // 從上往下動畫不能停止
        if ( this.camControls.isTop && this.animator.isPlaying ) return;

        // 在下面時動畫可持續點擊暫停和播放
        // todo 應該等目前hotspot縮圖讀完才能移動

        const info = this.hotspotdict[ hotspotId ]
        this.currentMoveHotspotId = hotspotId;

        if ( info )
        {

            const { position, cameraHeight, rotation, lowerSkybox, higherSkybox, id, previewUrl } = info
            const isTop = this.camControls.isTop;

            // if ( id === this.transitionMesh.hotspotid ) return;

            const dstPos = new THREE.Vector3(
                position.x,
                position.y + cameraHeight,
                position.z
            )

            let timeDuration = duration ? duration : this.animationDuration.GOHOTSPOT;

            if ( this.camControls.isTop )
            {
                timeDuration = this.animationDuration.GODOWN;
            }
            else
            {
                if ( this.enableCalcDurationByDistance && !duration )
                {
                    timeDuration = this.animationDuration.BYDISTANCE(
                        this.camControls.getCamerPosition().distanceTo( dstPos )
                    );
                }
            }

            // if (isFast)
            //     duration = this.animationDuration.FAST



            const promises = []

            const loadhighTexture = Global.inst.resLoad.load( higherSkybox, ResourcesType.Cubemap )

            const anime = ( lowerSkybox: any ) =>
            {
                if ( !this.camControls.isTop && this.enableBlurEffectAnimation )
                {
                    this.camControls.isBlurEffecting = true;
                }

                if ( this.animator.isPlaying )
                {
                    this.animator.cancel();

                    this.camControls.easingAnime = zoom && zoom !== 0 ? 'linear' : 'easeOutSine'
                    this.animationMesh.panoMaterial.transition = this.animationMesh.panoMaterial.currentIndexFade ? 0 : 1
                }

                if (zoom) {
                    if (!polarAngle && !azimuthAngle) {
                        this.camControls.lookAt(dstPos, timeDuration);
                    }
                }

                const goHotspot = new Timeline( timeDuration )
                goHotspot.addAction( this.camControls.goPosition( dstPos ) )
                goHotspot.addAction( this.animationMesh.goHotspot( {
                    id: id,
                    position: dstPos,
                    rotation: rotation,
                    texture: lowerSkybox
                }, this.showPanoramaMesh ) )

                Global.inst.resLoad.enableSkybox( true )
                return new Promise<void>( ( resolve ) =>
                {
                    this.animator.play( [ goHotspot ] ).then( () =>
                    {
                        resolve();
                    } )
                } )
            }

            // wait lowerskybox
            let animation;
            if ( previewUrl )
            {
                animation = Global.inst.resLoad.load( previewUrl, ResourcesType.Texture ).then( anime )
            } else
            {
                if ( this.preLoadHotspot[ id ] !== undefined )
                {
                    const texture = this.preLoadHotspot[ id ];
                    animation = anime( texture );
                } else
                {
                    if ( this.preLoadHotspot[ hotspotId ] !== undefined )
                    {
                        animation = anime( this.preLoadHotspot[ hotspotId ] );
                    } else
                    {
                        animation = Global.inst.resLoad.load( lowerSkybox, ResourcesType.Cubemap ).then( anime )
                    }
                }
            }

            promises.push(animation)
            promises.push(loadhighTexture)
            if (zoom) {
                let zoomAnimation = this.setCameraZoom(zoom, timeDuration);
                promises.push(zoomAnimation);
            }

            if ( polarAngle !== undefined )
            {
                const angleAnimation = this.camControls.setOrbitControlRotation( azimuthAngle ?? this.camControls.azimuthAngle, polarAngle, this.animationDuration.GOHOTSPOT )
                promises.push( angleAnimation )
            }

            let oldCamera = this.getCurrentPoseInfo();
            await Promise.all( promises ).then( values =>
            {
                const newHotspot = this.hotspotdict[ hotspotId ]

                let newCamera = this.getCurrentPoseInfo();
                newCamera.hotspotId = hotspotId

                if ( !this.latestHotspotId )
                {
                    console.log( 'scene is ready to view!' )
                    let oldFloor = newHotspot.floor.toString();
                    let newFloor = newHotspot.floor.toString();
                }

                if ( this.latestHotspotId )
                {
                    const oldHotspot = this.hotspotdict[ this.latestHotspotId ]
                    oldCamera.hotspotId = this.latestHotspotId;
                    if ( oldHotspot.floor !== newHotspot.floor )
                    {
                        let oldFloor = oldHotspot.floor.toString();
                        let newFloor = newHotspot.floor.toString();
                        if ( store.getState().iframeReducer !== null )
                        {
                            const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void
                            send( SpaceSdkCmd.floorupdated, {
                                oldFloor,
                                newFloor
                            } )
                        }
                        else
                        {
                            console.log( `user changed floor to ${ newFloor } from ${ oldFloor }` );
                        }
                    }

                    if ( store.getState().iframeReducer !== null )
                    {
                        const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void
                        send( SpaceSdkCmd.hotspotupdated, {
                            oldHotspotId: oldHotspot.id,
                            newHotspotId: newHotspot.id,
                        } )

                        send( SpaceSdkCmd.cameraupdated, {
                            oldCamera,
                            newCamera
                        } )
                    }
                    else
                    {
                        console.log( `user changed hotspot ${ newHotspot.id }` )
                        // console.log(`camera changed pose to ${newCamera} from ${oldCamera}`);
                    }
                }

                this.latestHotspotId = hotspotId
                this.currentFloor = this.hotspotdict[ id ].floor;
                this.preloadHotspot( this.latestHotspotId );
                this.displayHotspotNeighbors( id )
                this.camControls.easingAnime = 'easeInOutSine'
                this.animationMesh.setHdTexture( {
                    id: id,
                    position: dstPos,
                    rotation: rotation,
                    texture: values[ 1 ]
                } )
            } )
            if ( store.getState().iframeReducer === null ) return;
            const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
            if ( !isTop ) return;
            send( SpaceSdkCmd.setdollhousemode, false );
        }
        else
        {
            this.goNearHotspot( hotspotId, position, duration, zoom )
            // console.warn( `Hotspot '${ hotspotId }' not found, search nearest hotspot instead.` )
        }
    }

    public async setCameraZoom( value: number, duration?: number )
    {
        let oldCamera = this.getCurrentPoseInfo();
        await this.camControls.setCameraZoom( value, duration );
        let newCamera = this.getCurrentPoseInfo();

        if ( store.getState().iframeReducer !== null )
        {
            const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void
            send( SpaceSdkCmd.cameraupdated, {
                oldCamera,
                newCamera
            } )
        } else
        {
            // console.log(` ${newCamera} from ${oldCamera}`);
        }
    }

    private goNearHotspot( hotspotId: String, pos: THREE.Vector3, duration?: number, zoom?: number )
    {
        if ( !pos ) return;

        let hotspotKeys = this.hotspotIdList

        if ( this.latestHotspotId && !this.camControls.isTop && hotspotId != '' )
        {
            const { neighbors } = this.hotspotdict[ this.latestHotspotId ]
            let neighborsData = []

            if ( neighbors )
            {
                neighborsData = Object.assign( [], neighbors );
            }

            const currentFloor = this.hotspotdict[ this.latestHotspotId ].floor;
            // 增加往下走動方式
            for ( const [ key, _ ] of Object.entries( this.hotspotdict ) )
            {
                if ( key === this.latestHotspotId ) continue;
                const hotspot = this.hotspotdict[ key ];
                if ( hotspot.floor == currentFloor - 1 )
                {
                    neighborsData.push( hotspot.id );
                } else if ( Math.abs( pos.y - hotspot.position.y ) < 0.5 )
                {
                    neighborsData.push( hotspot.id );
                }
            }

            hotspotKeys = neighborsData;
        }

        let minDistance = Infinity;
        let nearestId = undefined;

        hotspotKeys.forEach( id =>
        {
            // if ( id === this.transitionMesh.hotspotid ) return;
            const info = this.hotspotdict[ id ];
            if ( info )
            {
                const { position } = info;
                const { floor } = info;
                const distance = pos.distanceTo( new THREE.Vector3( position.x, position.y, position.z ) )

                if ( distance < minDistance )
                {
                    minDistance = distance;
                    nearestId = id
                }
            }
        } )

        const mode = store.getState().modeReducer;

        if ( nearestId )
        {
            if ( this.camControls.isTop || mode === ViewerMode.Editor )
            {
                this.goHotspot( nearestId );
                return;
            }

            this.goHotspot( nearestId, undefined, undefined, undefined, duration, zoom );
        }
        else
            console.warn( 'nearest hotspot not found.' )
    }

    private getBehaviorModeByCam(): BehaviorMode
    {
        return this.camControls.isTop ? BehaviorMode.DollHouseMode : BehaviorMode.WalkingMode;
    }

    public getCurrentPoseInfo(): Format.ICameraPoseInfo
    {
        let hotspotId = "";
        if ( this.currentMoveHotspotId )
        {
            hotspotId = this.currentMoveHotspotId;
        }
        return {
            hotspotId: hotspotId,
            position: this.camControls.camera.position.clone(),
            rotation: {
                azimuthAngle: this.camControls.azimuthAngle,
                polarAngle: this.camControls.polarAngle,
            },
            zoom: this.camControls.camera.zoom,
            mode: this.getBehaviorModeByCam()
        };
    }

    // Animation controller
    private animate() {
        for (let animate of this._animateFuncList) {
            animate();
        }
        this.startAnimate();
    }

    public startAnimate() {
        this._animationID = requestAnimationFrame(this.animate.bind(this));
    }

    public stopAnimate() {
        if (this._animationID) {
            cancelAnimationFrame(this._animationID);
            this._animationID = undefined;
        }
    }

    public addAnimateFunc(func: Function) {
        this._animateFuncList.push(func);
    }

    public removeAnimateFunc(func: Function) {
        const index = this._animateFuncList.indexOf(func, 0);
        if (index > -1) {
            this._animateFuncList.splice(index, 1);
        }
    }

}