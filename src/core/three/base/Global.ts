import NetworkMgr from './NetworkMgr';
import ResourcesMgr from './ResourcesMgr';
import AnimeViewportController from './AnimeViewportController';
import Raycaster from './Raycaster';
import AnimationMgr from './AnimationMgr';
import FirstPersonControls from './FirstPersonControls';
import MediaManager from './MediaManager';
import ObjectController from './ObjectController';

export default class Global
{
    private static instance: Global = null;
    public network: NetworkMgr = null;
    public viewportController: AnimeViewportController = null;
    public raycaster: Raycaster = null;
    public animationMgr: AnimationMgr = null;
    public resLoad: ResourcesMgr = null;
    public firstPersonControls: FirstPersonControls = null;
    public objectController: ObjectController = null;
    public mediaManager: MediaManager = null;
    public scaleDeviceRatio: number = null;
    public sdkKey: string = null;

    public static get inst()
    {
        if ( Global.instance == null )
        {
            let maxScaleDeviceRatio = 1.5
            Global.instance = new Global();
            Global.instance.network = new NetworkMgr();
            Global.instance.mediaManager = new MediaManager();
            Global.instance.viewportController = new AnimeViewportController();
            Global.instance.raycaster = new Raycaster();
            Global.instance.firstPersonControls = new FirstPersonControls();
            Global.instance.resLoad = new ResourcesMgr();
            Global.instance.animationMgr = new AnimationMgr();

            if ( window.devicePixelRatio >= maxScaleDeviceRatio )
            {
                Global.inst.scaleDeviceRatio = maxScaleDeviceRatio / window.devicePixelRatio;
            } else
            {
                Global.instance.scaleDeviceRatio = 1;
            }
        }

        return Global.instance;
    }
}