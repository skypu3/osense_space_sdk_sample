import * as THREE from 'three'
import * as dat from 'lil-gui'

import
{
    BlendFunction,
    BrightnessContrastEffect,
    HueSaturationEffect,
    EdgeDetectionMode,
    EffectComposer,
    EffectPass,
    RenderPass,
    BokehEffect,
    NoiseEffect,
    VignetteEffect,
    KernelSize,
    BloomEffect,
    SelectiveBloomEffect,
    SMAAEffect,
    SMAAPreset,
    SMAAImageLoader,
} from "postprocessing";
import { sRGBEncoding } from 'three';
import { Format } from 'core/three/scene/Format';
import { isMobile } from 'react-device-detect';
import isWebview from 'is-ua-webview';
import store from 'core/store/store';
import { ViewerMode } from 'core/store/actions/mode';

export default class PostProcessingController
{
    private bloomEffectPass: EffectPass
    private selectiveBloomPass: EffectPass
    private blurPass: EffectPass

    private scene: THREE.Scene;
    private camera: THREE.Camera;
    private renderer: THREE.WebGLRenderer;
    private clock: THREE.Clock;
    private composer: EffectComposer;

    private noiseEffect: NoiseEffect;
    private vignetteEffect: VignetteEffect;
    private bokehEffect: BokehEffect;
    private hueSaturationEffect: HueSaturationEffect;
    private brightnessContrastEffect: BrightnessContrastEffect;
    private bloomEffect: BloomEffect;
    public selectiveBloomEffect: SelectiveBloomEffect;

    private postProcessingEnabled: Boolean;
    private conf: Format.IPostProcessing;

    private area: any;
    private search: any;

    constructor( IScene: THREE.Scene, ICamera: THREE.Camera, IRenderer: THREE.WebGLRenderer, conf: Format.IPostProcessing )
    {
        this.scene = IScene;
        this.camera = ICamera;
        this.renderer = IRenderer;
        this.composer = new EffectComposer( this.renderer );
        this.clock = new THREE.Clock()

        this.renderer.outputEncoding = sRGBEncoding;
        this.renderer.physicallyCorrectLights = true;

        this.noiseEffect = new NoiseEffect( { premultiply: true } );
        this.vignetteEffect = new VignetteEffect();
        this.bokehEffect = new BokehEffect()
        this.conf = conf
        this.postProcessingEnabled = ( this.conf && this.conf.postProcessingEnabled ) ?? false

        this.hueSaturationEffect = new HueSaturationEffect( {
            blendFunction: BlendFunction.SKIP,
            saturation: 0.4,
            hue: 0.0
        } );

        this.brightnessContrastEffect = new BrightnessContrastEffect( {
            blendFunction: BlendFunction.SKIP
        } );

        const bloomOptions = {
            blendFunction: BlendFunction.SOFT_LIGHT,
            kernelSize: KernelSize.SMALL,
            luminanceThreshold: 0.4,
            luminanceSmoothing: 0.1,
            height: 480
        };

        this.bloomEffect = new BloomEffect(
            this.scene,
            this.camera,
            bloomOptions
        )

        /* If you don't need to limit bloom to a subset of objects, consider using
        the basic BloomEffect for better performance. */
        this.selectiveBloomEffect = new SelectiveBloomEffect(
            this.scene,
            this.camera,
            bloomOptions
        );

        this.load();
    }

    private async load()
    {

        await this.loadSmaaImage();

        const smaaEffect = new SMAAEffect(
            this.search,
            this.area,
            SMAAPreset.HIGH,
            EdgeDetectionMode.DEPTH
        );

        smaaEffect.setDepthTexture( this.search )
        smaaEffect.edgeDetectionMaterial.setEdgeDetectionThreshold( 0.02 );

        this.bloomEffectPass = this.getEffectPass( smaaEffect, this.bloomEffect )
        this.selectiveBloomPass = this.getEffectPass( smaaEffect, this.selectiveBloomEffect )

        this.blurPass = new EffectPass(
            this.camera,
            this.bokehEffect,
        );

        this.selectiveBloomPass.renderToScreen = true
        this.bloomEffectPass.renderToScreen = true

        this.resetPostProcess()

        const mode = store.getState().modeReducer;
        if ( mode === ViewerMode.Editor )
        {
            this.defaultSetup()
            const gui = new dat.GUI()
            gui.title( 'Scene Filters Editor' )
            if ( isMobile || isWebview( navigator.userAgent ) )
            {
                gui.hide()
            } else
            {
                this.registerOptions( gui )
                gui.close()
            }
        } else {
            this.defaultSetup()
        }
    }

    public setSize()
    {
        this.composer.setSize( window.innerWidth, window.innerHeight );
    }

    public resetPostProcess()
    {
        this.composer.reset()
        if ( !this.postProcessingEnabled )
        {
            this.composer.addPass( new RenderPass( this.scene, this.camera ) );
        } else
        {
            this.composer.addPass( new RenderPass( this.scene, this.camera ) );
            this.composer.addPass( this.blurPass )
            this.composer.addPass( this.bloomEffectPass )
            this.composer.addPass( this.selectiveBloomPass )
        }
    }

    private getEffectPass( smaa: SMAAEffect, effect: any )
    {
        return new EffectPass(
            this.camera,
            smaa,
            this.noiseEffect,
            this.vignetteEffect,
            this.brightnessContrastEffect,
            this.hueSaturationEffect,
            effect,
        );
    }

    private defaultSetup()
    {
        // defaut value setup
        this.selectiveBloomPass.setEnabled( false )
        this.blurPass.setEnabled( false )
        this.selectiveBloomEffect.inverted = false;
        // this.selectiveBloomEffect.enable = true;

        if ( this.conf )
        {
            this.bloomEffect.resolution.height = this.conf.bloomResolution
            this.bloomEffect.blurPass.setScale( this.conf.bloomBlurScale )
            this.bloomEffect.blurPass.setKernelSize( this.conf.kernelSize )
            this.bloomEffect.blendMode.opacity.value = this.conf.bloomOpacity
            this.bloomEffect.setIntensity( this.conf.bloomIntensity );
            this.bloomEffect.luminanceMaterial.threshold = this.conf.luminThreshold
            this.bloomEffect.luminanceMaterial.smoothing = this.conf.luminSmooth
            this.bloomEffect.blendMode.setBlendFunction( this.conf.bloomBlendMode )
            this.brightnessContrastEffect.uniforms.get( "brightness" ).value = this.conf.brightness
            this.brightnessContrastEffect.blendMode.opacity.value = this.conf.brightnessOpacity
            this.brightnessContrastEffect.uniforms.get( "contrast" ).value = this.conf.contrast
            this.brightnessContrastEffect.blendMode.setBlendFunction( this.conf.brightnessBlendMode )
            this.hueSaturationEffect.setHue( this.conf.hue )
            this.hueSaturationEffect.uniforms.get( "saturation" ).value = this.conf.saturation
            this.hueSaturationEffect.blendMode.opacity.value = this.conf.saturationOpacity
            this.hueSaturationEffect.blendMode.setBlendFunction( this.conf.saturationBlendMode )
            this.blurPass.setEnabled( this.conf.blurEnabled )
            this.bokehEffect.blendMode.opacity.value = this.conf.blurOpacity
            this.noiseEffect.blendMode.opacity.value = this.conf.noiseOpacity
            this.vignetteEffect.blendMode.opacity.value = this.conf.vignetteOpacity
        } else
        {
            this.bloomEffect.resolution.height = 480
            this.bloomEffect.blendMode.setBlendFunction( 17 );
            this.selectiveBloomEffect.blendMode.setBlendFunction( 17 );
            this.hueSaturationEffect.blendMode.setBlendFunction( 17 )
            this.brightnessContrastEffect.blendMode.setBlendFunction( 17 )
            this.bloomEffect.blurPass.setScale( 0.63 )
            this.selectiveBloomEffect.blurPass.setScale( 0.63 )
            this.bloomEffect.blurPass.setKernelSize( KernelSize.SMALL )
            this.selectiveBloomEffect.blurPass.setKernelSize( KernelSize.SMALL )
            this.noiseEffect.blendMode.opacity.value = 0
            this.vignetteEffect.blendMode.opacity.value = 0
            this.bokehEffect.blendMode.opacity.value = 0
        }
    }

    public getOptions()
    {
        return {
            postProcessingEnabled: this.postProcessingEnabled,
            bloomResolution: this.bloomEffect.resolution.height,
            bloomBlurScale: this.bloomEffect.blurPass.getScale(),
            bloomIntensity: this.bloomEffect.getIntensity(),
            bloomOpacity: this.bloomEffect.blendMode.opacity.value,
            kernelSize: this.bloomEffect.blurPass.getKernelSize(),
            luminThreshold: this.bloomEffect.luminanceMaterial.threshold,
            luminSmooth: this.bloomEffect.luminanceMaterial.smoothing,
            brightness: this.brightnessContrastEffect.uniforms.get( "brightness" ).value,
            contrast: this.brightnessContrastEffect.uniforms.get( "contrast" ).value,
            brightnessOpacity: this.brightnessContrastEffect.blendMode.opacity.value,
            saturation: this.hueSaturationEffect.uniforms.get( "saturation" ).value,
            hue: this.hueSaturationEffect.getHue(),
            saturationOpacity: this.hueSaturationEffect.blendMode.opacity.value,
            noiseOpacity: this.noiseEffect.blendMode.opacity.value,
            vignetteOpacity: this.vignetteEffect.blendMode.opacity.value,
            blurEnabled: this.blurPass.isEnabled(),
            blurOpacity: this.bokehEffect.blendMode.opacity.value,
            bloomBlendMode: this.bloomEffect.blendMode.blendFunction,
            saturationBlendMode: this.hueSaturationEffect.blendMode.blendFunction,
            brightnessBlendMode: this.brightnessContrastEffect.blendMode.blendFunction,
        }
    }

    private registerOptions( menu: dat.GUI )
    {

        const selectiveBloomPass = this.selectiveBloomPass
        const bloomEffectPass = this.bloomEffectPass
        const blurPass = this.blurPass

        const bloomEffect = this.bloomEffect
        const selectiveBloomEffect = this.selectiveBloomEffect
        const brightnessContrastEffect = this.brightnessContrastEffect
        const hueSaturationEffect = this.hueSaturationEffect

        const bloomBlendMode = bloomEffect.blendMode
        const selectiveBloomBlendMode = selectiveBloomEffect.blendMode
        const noiseEffectBlendMode = this.noiseEffect.blendMode
        const blurBlendMode = this.bokehEffect.blendMode
        const vignetteEffectBlendeMode = this.vignetteEffect.blendMode

        const params = {
            "effects enabled": this.postProcessingEnabled,
            "resolution": bloomEffect.resolution.height,
            "kernel size": bloomEffect.blurPass.getKernelSize(),
            "blur scale": bloomEffect.blurPass.getScale(),
            "intensity": bloomEffect.getIntensity(),
            "luminance": {
                "filter": bloomEffect.luminancePass.isEnabled(),
                "threshold": bloomEffect.luminanceMaterial.threshold,
                "smoothing": bloomEffect.luminanceMaterial.smoothing
            },
            "bloom": {
                "global disabled": selectiveBloomPass.isEnabled(),
                "bloom opacity": bloomBlendMode.opacity.value,
                "blend mode": bloomBlendMode.blendFunction
            },
            "brightnessContrast": {
                "brightness": brightnessContrastEffect.uniforms.get( "brightness" ).value,
                "contrast": brightnessContrastEffect.uniforms.get( "contrast" ).value,
                "opacity": brightnessContrastEffect.blendMode.opacity.value,
                "blend mode": brightnessContrastEffect.blendMode.blendFunction
            },
            "hueSaturation": {
                "hue": hueSaturationEffect.getHue(),
                "saturation": hueSaturationEffect.uniforms.get( "saturation" ).value,
                "opacity": hueSaturationEffect.blendMode.opacity.value,
                "blend mode": hueSaturationEffect.blendMode.blendFunction
            },
            "moreEffects": {
                "noise": noiseEffectBlendMode.opacity.value,
                "vignette": vignetteEffectBlendeMode.opacity.value,
                "blur enabled": blurPass.isEnabled(),
                "blur opacity": blurBlendMode.opacity.value,
            }
        };

        menu.add( params, "effects enabled" ).onChange( ( value ) =>
        {

            this.postProcessingEnabled = !this.postProcessingEnabled
            this.resetPostProcess()

        } );

        menu.add( params, "resolution", [ 240, 360, 480, 720, 1080 ] ).onChange( ( value ) =>
        {

            bloomEffect.resolution.height = selectiveBloomEffect.resolution.height = Number( value );

        } );

        menu.add( params, "kernel size", KernelSize ).onChange( ( value ) =>
        {

            bloomEffect.blurPass.setKernelSize( Number( value ) );
            selectiveBloomEffect.blurPass.setKernelSize( Number( value ) );

        } );

        menu.add( params, "blur scale", 0.0, 1.0, 0.01 ).onChange( ( value ) =>
        {

            bloomEffect.blurPass.setScale( Number( value ) );
            selectiveBloomEffect.blurPass.setScale( Number( value ) );

        } );

        menu.add( params, "intensity", 0.0, 3.0, 0.01 ).onChange( ( value ) =>
        {

            bloomEffect.setIntensity( Number( value ) );
            selectiveBloomEffect.setIntensity( Number( value ) );

        } );

        let folder = menu.addFolder( "Luminance" );

        folder.add( params.luminance, "filter" ).onChange( ( value ) =>
        {

            bloomEffect.luminancePass.setEnabled( value );
            selectiveBloomEffect.luminancePass.setEnabled( value );

        } );

        folder.add( params.luminance, "threshold", 0.0, 1.0, 0.001 )
            .onChange( ( value ) =>
            {

                bloomEffect.luminanceMaterial.threshold = Number( value );
                selectiveBloomEffect.luminanceMaterial.threshold = Number( value );

            } );

        folder.add( params.luminance, "smoothing", 0.0, 1.0, 0.001 )
            .onChange( ( value ) =>
            {

                bloomEffect.luminanceMaterial.smoothing = Number( value );
                selectiveBloomEffect.luminanceMaterial.smoothing = Number( value );

            } );

        folder.open();
        folder = menu.addFolder( "Bloom" );

        // folder.add( params.bloom, "global disabled" ).onChange( ( value ) =>
        // {
        //     selectiveBloomPass.setEnabled( value )
        //     bloomEffectPass.setEnabled( !value )
        //     this.resetPostProcess()
        // } );

        folder.add( params.bloom, "bloom opacity", 0.0, 2.0, 0.01 ).onChange( ( value ) =>
        {

            bloomBlendMode.opacity.value = value
            selectiveBloomBlendMode.opacity.value = value;

        } );

        folder.add( params.bloom, "blend mode", BlendFunction ).onChange( ( value ) =>
        {

            bloomBlendMode.setBlendFunction( Number( value ) );
            selectiveBloomBlendMode.setBlendFunction( Number( value ) );

        } );

        folder = menu.addFolder( "Brightness & Contrast" );

        folder.add( params.brightnessContrast, "brightness", -1.0, 1.0, 0.001 )
            .onChange( ( value ) =>
            {

                brightnessContrastEffect.uniforms.get( "brightness" ).value = value;

            } );

        folder.add( params.brightnessContrast, "contrast", -1.0, 1.0, 0.001 )
            .onChange( ( value ) =>
            {

                brightnessContrastEffect.uniforms.get( "contrast" ).value = value;

            } );

        folder.add( params.brightnessContrast, "opacity", 0.0, 1.0, 0.01 )
            .onChange( ( value ) =>
            {

                brightnessContrastEffect.blendMode.opacity.value = value;

            } );

        folder.add( params.brightnessContrast, "blend mode", BlendFunction )
            .onChange( ( value ) =>
            {

                brightnessContrastEffect.blendMode.setBlendFunction( Number( value ) );

            } );

        folder = menu.addFolder( "Hue & Saturation" );

        folder.add( params.hueSaturation, "hue", 0.0, Math.PI * 2.0, 0.001 )
            .onChange( ( value ) =>
            {

                hueSaturationEffect.setHue( value );

            } );

        folder.add( params.hueSaturation, "saturation", -1.0, 1.0, 0.001 )
            .onChange( ( value ) =>
            {

                hueSaturationEffect.uniforms.get( "saturation" ).value = value;

            } );

        folder.add( params.hueSaturation, "opacity", 0.0, 1.0, 0.01 )
            .onChange( ( value ) =>
            {

                hueSaturationEffect.blendMode.opacity.value = value;

            } );

        folder.add( params.hueSaturation, "blend mode", BlendFunction )
            .onChange( ( value ) =>
            {

                hueSaturationEffect.blendMode.setBlendFunction( Number( value ) );

            } );

        folder = menu.addFolder( "More Effects" );

        folder.add( params.moreEffects, "noise", 0.0, 2.0, 0.01 ).onChange( ( value ) =>
        {

            noiseEffectBlendMode.opacity.value = value

        } );

        folder.add( params.moreEffects, "vignette", 0.0, 2.0, 0.01 ).onChange( ( value ) =>
        {

            vignetteEffectBlendeMode.opacity.value = value

        } );

        menu.add( params.moreEffects, "blur enabled" ).onChange( ( value ) =>
        {

            blurPass.setEnabled( value )

        } );

        menu.add( params.moreEffects, "blur opacity", 0.0, 2.0, 0.01 ).onChange( ( value ) =>
        {

            blurBlendMode.opacity.value = value;

        } );

        if ( window.innerWidth < 720 )
        {

            menu.close();

        }
    }

    private async loadSmaaImage()
    {
        return new Promise<void>( ( resolve, reject ) =>
        {
            const manager = new THREE.LoadingManager();
            manager.onLoad = () => setTimeout( resolve, 250 );
            manager.onError = url => console.error( `Failed to load ${ url }` );

            const smaaImageLoader: SMAAImageLoader = new SMAAImageLoader( manager );
            const self = this;

            smaaImageLoader.load( ( [ search, area ] ) =>
            {
                self.search = search;
                self.area = area;
            } );
        } );
    }

    public composerRender()
    {
        var delta = this.clock.getDelta();
        this.composer.render( delta );
    }
}