import { TrackChangesTwoTone } from '@material-ui/icons';
import { SpaceSdkCmd } from 'core/store/actions/iframe';
import store from 'core/store/store';
import Raycaster, { IRaycastResponse, RaycastEvent } from 'core/three/base/Raycaster';
import TransformControls from 'core/three/TransfromControls/TransformControls';
import * as THREE from 'three'
// import ArrangeModel from '../Furnishings/ArrangeModel/ArrangeModel';
import { EObjectType } from '../mediaObject/MediaObject3D';
import Image from '../mediaObject/media/image/Image';
import NewTag from '../mediaObject/media/image/NewTag';
import Media from '../mediaObject/media/Media';
import MouseCursor from '../mediaObject/mousecursor/MouseCursor';
import { RaycastType } from '../mediaObject/RaycastObject';
import AnimationMgr from './AnimationMgr';
import AnimeViewportController from './AnimeViewportController';
import Global from './Global';

let self: ObjectController;

export default class ObjectController
{
    onClick: ( obj: object, set: ( obj: object ) => void ) => void;
    public selectedInfo: IRaycastResponse;
    public mouseCursor: MouseCursor;
    public animationMgr: AnimationMgr;
    hoverBbox: THREE.BoxHelper;
    hoverObject: THREE.Object3D;
    transformControls: TransformControls;
    private camera: THREE.Camera;
    private canvas: HTMLCanvasElement;
    private _enabled = true;

    get controls()
    {
        return this.transformControls;
    }

    get enabled()
    {
        return this._enabled;
    }

    set enabled( option: boolean )
    {
        this._enabled = option;
        this.detachEvent();
    }

    constructor( iViewportControls: AnimeViewportController, canvas: HTMLCanvasElement )
    {
        self = this;
        this.canvas = canvas;
        const oribitControls = iViewportControls.OrbitControls;
        const { camera, renderer, render } = iViewportControls;
        this.camera = camera;
        this.transformControls = new TransformControls( camera, renderer.domElement );
        this.transformControls.addEventListener( 'change', render );

        this.transformControls.addEventListener( 'mouseDown', function ()
        {
            oribitControls.enabled = false;
        } );
        this.transformControls.addEventListener( 'mouseUp', function ()
        {
            oribitControls.enabled = true;
        } );

        this.detachEvent = this.detachEvent.bind( this );
    }

    setRaycastScene( fetchworldPosition )
    {
        this.transformControls.setRaycastScene( fetchworldPosition );
    }

    attachTransformControl( media: Media | THREE.Object3D )
    {
        this.transformControls.enabled = true;
        this.transformControls.attachObject( media );
    }

    public raycastMouseMove( info: IRaycastResponse )
    {
        if ( info )
        {
            const { rotation, position } = info;
            if ( this.mouseCursor )
                this.mouseCursor.set( rotation, position )
        } else
        {
            this.mouseCursor.opacity = 0.0
        }

        if ( this._enabled && info && info.target && info.target.editEnabled )
        {
            if ( info.target.mediaClass )
            {
                const { object, mediaClass } = info.target

                if ( object === this.hoverObject ) return;
                this.hoverObject = object;

                this.addHoverHandle( mediaClass );

                this.hoverBbox?.parent?.remove( this.hoverBbox )
                this.hoverBbox?.parent?.remove( this.hoverBbox )
                const quaternion = object.quaternion.clone();
                object.quaternion.set( 0, 0, 0, 1 );

                // if (mediaClass instanceof Image) {
                //     let hotspotTag = mediaClass.gethotspotTag
                //     object.remove(hotspotTag);
                //     this.hoverBbox = new THREE.BoxHelper(object, 0x420042);
                //     object.add(hotspotTag);
                // }
                // else
                //     this.hoverBbox = new THREE.BoxHelper(object, 0x420042);
                this.hoverBbox = new THREE.BoxHelper( object, 0x420042 );
                object.quaternion.copy( quaternion );
                this.hoverBbox.applyMatrix4( object.matrixWorld.invert() )
                object.add( this.hoverBbox )
            }

            this.mouseCursor.setVisible(false);
            document.body.style.cursor = 'pointer'
        } else
        {
            this.removeHoverHandle();
        }

        if ( this.animationMgr && !this.animationMgr.enablePanoramaMesh )
        {
            this.mouseCursor.setVisible(false);

        }
    }

    private addHoverHandle( media: Media )
    {
        if ( media.type === EObjectType.TAG )
        {
            const tag = media as NewTag;
            if ( tag )
            {
                tag.enableLabels( true );
            }
        }
    }

    private removeHoverHandle()
    {
        this.hoverBbox?.parent?.remove( this.hoverBbox )
        this.hoverBbox = undefined
        this.hoverObject = undefined
        if ( this.mouseCursor )
            this.mouseCursor.setVisible(true);
        document.body.style.cursor = 'auto'

        const sprites = Global.inst.mediaManager.mediaList.filter( ( media: Media ) => media.type === EObjectType.TAG );
        sprites.forEach( ( tag: NewTag ) => tag.enableLabels( false ) );
    }


    private unselectObject()
    {
        // 關閉選擇物件
        if ( store.getState().iframeReducer !== null )
        {
            const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
            send( SpaceSdkCmd.selectobject, {
                'tagId': ''
            } );
        }
        this.selectedInfo = undefined;
        this.detachEvent();

    }

    public raycastMouseUp( info: IRaycastResponse )
    {
        if ( !info ) return;

        // sceneModel.info.target.editable == false
        if ( info && info.target && info.target.editEnabled && info.target.mediaClass
            && this.selectedInfo && this.selectedInfo.target
            && info.target.mediaClass.id === this.selectedInfo.target.mediaClass.id )
        {
            if ( this.transformControls.enabled )
            {
                this.unselectObject()
                return
            }
        }
        else if ( info && info.target && info.target.editEnabled && this._enabled )
        {
            const { target } = info

            if ( target.mediaClass && target.mediaClass.id )
            {
                this.selectedInfo = info;
                this.attachTransformControl( target.mediaClass );
                this.canvas.addEventListener( 'contextmenu', this.detachEvent );
                window.addEventListener( 'keydown', this.keyboardEvent );
                // 選擇某個物件
                if ( store.getState().iframeReducer !== null )
                {
                    const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
                    send( SpaceSdkCmd.selectobject, {
                        'tagId': target.mediaClass.id
                    } );
                }
            }

            if ( this.onClick )
            {
                this.onClick( target.json, target.setJson )
            }
        } else
        {
            if ( this.transformControls.enabled )
            {
                this.unselectObject()
                return;
            }
            if ( !Global.inst.viewportController.isTop )
            {
                return
            }
        }

        if ( info && info.target && this.animationMgr.enablePanoramaMesh )
        {
            if ( RaycastType.hasProperty( info.target.type, RaycastType.MOVE ) )
            {
                if ( !this.selectedInfo )
                {
                    const { target, position } = info;
                    Global.inst.animationMgr.goHotspot( target.id, position );
                }
                else 
                {
                    if ( store.getState().iframeReducer === null ) return;
                    const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
                    send( SpaceSdkCmd.sendmessageboxwarning, 'Editing on media now, cannot walking through hotspots' );
                }
            }
        }
    }

    public detachEvent()
    {
        self.canvas.removeEventListener( 'contextmenu', this.detachEvent );
        self.transformControls.detach();
        self.transformControls.setMode( "translate" );
        self.transformControls.enabled = false;
        window.removeEventListener( 'keydown', this.keyboardEvent );
        this.selectedInfo = undefined
    }

    private keyboardEvent( event: KeyboardEvent )
    {
        const { code } = event;
        switch ( code )
        {
            case 'Escape':
                self.unselectObject();
                break;
            case 'Digit1':
                self.transformControls.setMode( "translate" );
                break;
            case 'Digit2':
                self.transformControls.setMode( "scale" );
                break;
            case 'Digit3':
                self.transformControls.setMode( "rotate" );
                break;
        }
    }

    public attachNewObject( obj: Media | THREE.Object3D ): void
    {
        if ( !this._enabled ) return;
        this.attachTransformControl( obj );
        this.canvas.addEventListener( 'contextmenu', this.detachEvent );
        window.addEventListener( 'keydown', this.keyboardEvent );
        this.transformControls.addPointerMoveEvent();
        this.transformControls.setAxis( 'XYZ' );
        this.transformControls.setDragging( true );
        this.transformControls.showTransformControllerHelper( false );
    }
}

