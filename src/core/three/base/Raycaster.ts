import * as THREE from 'three';
import MediaObject3D from 'core/three/mediaObject/MediaObject3D';
import { RaycastObject, RaycastClickData, EObjectMouseState } from '../mediaObject/RaycastObject';

export interface IRaycastResponse
{
    destroy: any
    target: RaycastClickData,
    rotation: THREE.Matrix4,
    position: THREE.Vector3,
}

export interface IRaycastTarget
{
    /**
     * call testobject.onMouseEnter/onMouseMove/onMouseLeave when intersect success
     */
    testobject: RaycastObject[];
    /**
     * 
     * @param info return first hit info
     */
    onMouseUp?( res: IRaycastResponse ): void;
    onMousemove?( res: IRaycastResponse | undefined ): void;

}


interface IRaycastEvent
{
    raycastobjects: THREE.Object3D[];
    eventobjects: RaycastObject[];
    onMouseUp?( info: IRaycastResponse ): void;
    onMousemove?( res: IRaycastResponse | undefined ): void;
}

export class RaycastEvent implements IRaycastEvent
{
    private layer: number;
    public raycastobjects: THREE.Object3D[];
    public eventobjects: RaycastObject[];
    public onMouseUp?( info: IRaycastResponse ): void;
    public onMousemove?( res: IRaycastResponse | undefined ): void;

    constructor( layer: number )
    {
        this.layer = layer;
        this.raycastobjects = []
        this.eventobjects = []
        this.onMouseUp = () => { }
    }

    public addTarget( object3d: RaycastObject | RaycastObject[] )
    {
        if ( object3d instanceof RaycastObject )
        {
            const obj = object3d.raycastTarget;
            obj.userData = {
                mouseEventAgent: object3d
            };
            obj.layers.enable( this.layer );
            this.raycastobjects.push( obj );
            this.eventobjects.push( object3d );

            const scope = this;
            ( object3d as MediaObject3D ).addCustumDestory( () =>
            {
                function removeFromArray( array, obj )
                {
                    const index = array.indexOf( obj )
                    if ( index > -1 )
                    {
                        array.splice( index, 1 );
                    }
                }
                removeFromArray( scope.raycastobjects, obj );
                removeFromArray( scope.eventobjects, object3d );
            } )

        } else if ( object3d instanceof Array )
        {
            object3d.forEach( object =>
            {
                this.addTarget( object );
            } );
        }
    }
}

export default class Raycaster
{
    layer = 1;
    private raycaster: THREE.Raycaster;
    private camera: THREE.Camera;

    private isMouseUpThreshold: number = 10;
    private mouseDownCoord: THREE.Vector2;
    private mouse: THREE.Vector2;


    private targets: RaycastEvent[];

    constructor( layer: number = 1 )
    {
        this.layer = layer;
        this.raycaster = new THREE.Raycaster();
        this.raycaster.layers.set( this.layer );
        this.mouseDownCoord = new THREE.Vector2();
        this.mouse = new THREE.Vector2();
        this.targets = [];
    }

    init( canvas: HTMLCanvasElement, camera: THREE.Camera )
    {
        this.camera = camera;
        canvas.addEventListener( 'mousemove', this.onMouseMove.bind( this ), false );
        canvas.addEventListener( 'mousedown', this.onMouseDown.bind( this ), false );
        canvas.addEventListener( 'mouseup', this.onMouseUp.bind( this ), false );

        canvas.addEventListener( 'touchend', this.onMouseUp.bind( this ), false );
        canvas.addEventListener( 'touchstart', this.onMouseDown.bind( this ), false );

    }

    public addEvent( targets?: IRaycastTarget ): RaycastEvent
    {
        const event = new RaycastEvent( this.layer )

        if ( targets )
        {
            const { testobject, onMouseUp, onMousemove } = targets
            testobject.forEach( object3d =>
            {
                event.addTarget( object3d )
            } )

            event.onMouseUp = onMouseUp
            event.onMousemove = onMousemove
        }

        this.targets.push( event );
        return event;
    }

    updateMouse( event, mouse: THREE.Vector2 )
    {
        if ( event.type.includes( 'touch' ) )
        {
            mouse.x = event.changedTouches[ 0 ].clientX;
            mouse.y = event.changedTouches[ 0 ].clientY;
        } else
        {
            mouse.x = event.clientX;
            mouse.y = event.clientY;
        }
    }

    emitRaycaster( event, callback: ( mouseState: EObjectMouseState, events: RaycastObject, intersect?: THREE.Intersection ) => void, isMove: boolean )
    {
        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components
        const mouse = new THREE.Vector2();
        mouse.x = ( this.mouse.x / window.innerWidth ) * 2 - 1;
        mouse.y = - ( this.mouse.y / window.innerHeight ) * 2 + 1;
        this.raycaster.setFromCamera( mouse, this.camera );

        this.targets.forEach( target =>
        {
            const { raycastobjects, onMouseUp, onMousemove } = target;
            let eventCallBack = ( isMove ? onMousemove : onMouseUp );
            const intersects = this.raycaster.intersectObjects( raycastobjects );

            target.eventobjects.forEach( eventobject =>
            {
                if ( intersects.length > 0 )
                {
                    if ( eventobject.raycastTarget === intersects[ 0 ].object.userData.mouseEventAgent.raycastTarget )
                    {
                        callback( EObjectMouseState.ON, eventobject, intersects[ 0 ] );
                    } else
                        callback( EObjectMouseState.NOTON, eventobject );


                } else
                {
                    callback( EObjectMouseState.NOTON, eventobject );
                }
            } )
            if ( intersects.length > 0 )
            {
                const custumObject = intersects[ 0 ].object.userData.mouseEventAgent;

                if ( eventCallBack )
                    eventCallBack( {
                        destroy: custumObject.destroy.bind( custumObject ),
                        target: custumObject.targetInfo,
                        rotation: this.intersect2rot4( intersects[ 0 ] ),
                        position: intersects[ 0 ].point,
                    } )
            } else
            {
                if ( eventCallBack && isMove )
                {
                    eventCallBack( undefined )
                }
            }
        }
        );
    }

    onMouseDown( event )
    {
        this.updateMouse( event, this.mouseDownCoord );
    }

    private intersect2rot4( intersect: THREE.Intersection ): THREE.Matrix4
    {
        if ( intersect )
        {
            const n = intersect.face.normal.clone();
            intersect.object.updateMatrixWorld( true )
            n.transformDirection( intersect.object.matrixWorld );
            const mat4 = new THREE.Matrix4().lookAt( n, new THREE.Vector3( 0, 0, 0 ), this.camera.up );
            return mat4;
        }

    }
    onMouseMove( event )
    {
        if ( this.targets.length === 0 ) return;
        this.updateMouse( event, this.mouse );
        this.emitRaycaster( event, ( state: EObjectMouseState, object: RaycastObject, intersect?: THREE.Intersection ) =>
        {
            if ( state === EObjectMouseState.NOTON && object.state === EObjectMouseState.ON )
            {
                object.onMouseLeave();
            } else if ( state === EObjectMouseState.ON )
            {
                object.onMouseMove( this.intersect2rot4( intersect ), intersect.point );
                if ( object.state === EObjectMouseState.NOTON )
                    object.onMouseEnter();
            }
            object.state = state;
        }, true )
    }

    onMouseUp( event )
    {
        if ( event.button === 2 ) return
        this.updateMouse( event, this.mouse );
        if ( this.mouseDownCoord.distanceTo( this.mouse ) > this.isMouseUpThreshold ) return;
        if ( this.targets.length === 0 ) return;
        this.emitRaycaster( event, ( state: EObjectMouseState, object: RaycastObject, intersect?: THREE.Intersection ) =>
        {
            if ( state === EObjectMouseState.ON )
            {
                object.onMouseUp( this.intersect2rot4( intersect ), intersect.point );
            }
        }, false )
    }
}