import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { VerticalBlurShader } from 'three/examples/jsm/shaders/VerticalBlurShader';
import { HorizontalBlurShader } from 'three/examples/jsm/shaders/HorizontalBlurShader';

export default class BlurController
{
    private composer: EffectComposer;
    private horizontalBlurShader: ShaderPass;
    private verticalBlurShaderPass: ShaderPass;
    private scene: THREE.Scene;
    private camera: THREE.Camera;
    private renderer: THREE.WebGLRenderer;
    private defaultVerticalBlurFactor: number = 3.5;
    private defaultHorizontalBlurFactor: number = 3.5;

    constructor( IScene: THREE.Scene, ICamera: THREE.Camera, IRenderer: THREE.WebGLRenderer )
    {
        this.scene = IScene;
        this.camera = ICamera;
        this.renderer = IRenderer;

        // blur effect
        this.composer = new EffectComposer( this.renderer );
        this.composer.addPass( new RenderPass( this.scene, this.camera ) );

        this.verticalBlurShaderPass = new ShaderPass( VerticalBlurShader );
        this.horizontalBlurShader = new ShaderPass( HorizontalBlurShader );

        this.verticalBlurShaderPass.uniforms[ 'v' ].value = this.defaultVerticalBlurFactor / window.innerHeight;
        this.horizontalBlurShader.uniforms[ 'h' ].value = this.defaultHorizontalBlurFactor / window.innerWidth;

        this.composer.addPass( this.horizontalBlurShader );
        this.composer.addPass( this.verticalBlurShaderPass );
    }

    // set the blur factor(vertical, horizontal)
    public setBlurEffectFactor( _verticalBlurFactor?: number, _horizontalBlurFactor?: number )
    {
        if ( _verticalBlurFactor )
            this.verticalBlurShaderPass.uniforms[ 'v' ].value.set( _verticalBlurFactor / window.innerHeight );
        if ( _horizontalBlurFactor )
            this.horizontalBlurShader.uniforms[ 'h' ].value.set( _horizontalBlurFactor / window.innerWidth );
    }

    // render function 
    public composerRender()
    {
        if ( this.composer )
        {
            this.composer.render();
        }
    }
}