import { Format } from '../scene/Format';
import Scene from '../scene/Scene';
import MediaObject from 'core/three/mediaObject';
import ViewportController from './ViewportController';
import Hotspot from '../mediaObject/hotspot/Hotspot';
import AnimeRoomModel from '../mediaObject/roomModel/AnimeRoomModel';
import { EObjectType } from "core/three/mediaObject/MediaObject3D";
import Media from '../mediaObject/media/Media';
import store from 'core/store/store';
import { ProductType, setProduct } from 'core/store/actions/products';
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader';

export default class MediaManager {
    scene: Scene | undefined;
    viewportControls: ViewportController | undefined;
    Objects: {};
    mediaList: any[];
    positionObjectlist: any[];
    raycastTargets = [];
    roomModel = new AnimeRoomModel();
    hotspotMeshes: Hotspot[];
    autoplay: boolean;
    hideHotspots: boolean;
    positionObjectsCount: number;

    sceneGltf: GLTF
    imageList = []
    modelList = []
    videoList = []
    tagList = []

    constructor() {
        this.mediaList = [];
        this.positionObjectlist = []
        this.autoplay = false;
        this.hideHotspots = true;
        this.positionObjectsCount = 0;
    }

    init(iScene: Scene, iViewportControls: ViewportController) {
        this.viewportControls = iViewportControls;
        this.scene = iScene;
        this.mediaList = [];
        this.positionObjectlist = []
        this.autoplay = false;
        this.hideHotspots = true;
    }

    public toJson(): any {
        const medias = []
        this.mediaList.forEach(media => {
            const map = Object.entries(media.json).filter((data) => data[1] !== undefined && data[1] !== null);
            medias.push(Object.fromEntries(map));
        })

        const positionObjects = []
        this.positionObjectlist.forEach(object => {
            positionObjects.push(object.json)
        })

        return {
            mediaObjects: medias,
            positioningObjects: positionObjects
        }
    }

    hideAllHotspots(option: boolean) {
        this.hideHotspots = option;

        this.hotspotMeshes.forEach(hotspot => {
            hotspot.object.visible = option;
        });
    }

    findTagObject(id: string): Media {
        const medias = this.mediaList.filter((tagMediaObject) => tagMediaObject.id === id)
        if (medias.length > 0) {
            return medias[0] as Media;
        }
        return null
    }

    createHotspots(hotspotsData: Format.IHotspot[]): { hotspotMeshes: Hotspot[] } {
        const hotspotMeshes: Hotspot[] = [];

        hotspotsData.forEach(hotspot => {
            let newHotspot = new MediaObject.Hotspot();
            newHotspot.info = hotspot;
            newHotspot.object.visible = !this.hideHotspots;
            this.scene.add(newHotspot);
            hotspotMeshes.push(newHotspot)
            // this.raycastEvent.addTarget( newHotspot )
        });
        this.hotspotMeshes = hotspotMeshes;

        return { hotspotMeshes };
    }

    public removeMediaObjects(type: EObjectType) {
        this.mediaList.forEach((media: Media) => {
            if (media.type === type) {
                media.destroy();
                const index = this.mediaList.indexOf(media);
                media = undefined;
                this.mediaList.splice(index, 1);
            }
        });
    }

    restoreScene() {
        // this.sceneModel.setMesh(this.sceneGltf.scene)
        this.createMediaObjects(this.modelList)
        this.createMediaObjects(this.imageList)
    }

    clearScene() {
        // this.sceneModel.removeSceneMesh()
        this.removeMediaObjects(EObjectType.MODEL)
        this.removeMediaObjects(EObjectType.PHOTO)
        this.viewportControls.renderer.renderLists.dispose();
    }

    createSceneModel(data: Format.IScene) {
        this.roomModel.setGLTF(data.space.glb)
        this.roomModel.panoMaterial.axisOnZ = data.space.isMp;
        this.scene.add(this.roomModel);
        return this.roomModel;
    }

    async createMediaObjects(mediaObjects: Format.IMedia[], isCreatedByPos: boolean = false) {
        let newMedias = [];

        for (const media of mediaObjects) {
            let mediaOBJ = undefined;
            switch (media.type) {
                case EObjectType.MODEL:
                    mediaOBJ = new MediaObject.Model();
                    await mediaOBJ.init(media as Format.IModel);
                    break;
                case EObjectType.PHOTO:
                    mediaOBJ = new MediaObject.Image();
                    await mediaOBJ.init(media as Format.IImage)
                    break;
                case EObjectType.TAG:
                    mediaOBJ = new MediaObject.NewTag();
                    await mediaOBJ.init(media as Format.INewTag)
                    break;
                case EObjectType.GIF:
                    mediaOBJ = new MediaObject.Gif();
                    await mediaOBJ.init(media as Format.IGif)
                    break;
                case EObjectType.VIDEO:
                    mediaOBJ = new MediaObject.Video();
                    await mediaOBJ.init(media as Format.IVideo);
                    break;
                case EObjectType.SPRITE:
                    mediaOBJ = new MediaObject.Sprite();
                    await mediaOBJ.init(media as Format.ISprite);
                    break;
                case EObjectType.TEXT:
                    mediaOBJ = new MediaObject.Text();
                    mediaOBJ.init(media as Format.IText);
                    break;
                case EObjectType.PORTAL:
                    mediaOBJ = new MediaObject.Portal();
                    await mediaOBJ.init(media as Format.IImage)
                    break
                case EObjectType.POSITIONPLANE:
                    mediaOBJ = new MediaObject.PositioningPlane();
                    const planeMedia = media as Format.IPositionPlane;
                    planeMedia.positionObjectsCount = this.positionObjectsCount;
                    this.positionObjectsCount++;
                    await mediaOBJ.init(planeMedia);
                    if (planeMedia.swappedProps) {
                        const { newMedias } = await this.createMediaObjects([planeMedia.swappedProps], true);
                        const media: Media = newMedias[0];
                        media.underPositioningObject = true;
                        mediaOBJ.swappedMedia = media;
                    }
                    break;
                case EObjectType.POSITIONCUBE:
                    mediaOBJ = new MediaObject.PositioningCube();
                    const cubeMedia = media as Format.IPositionPlane;
                    await mediaOBJ.init(cubeMedia);
                    if (cubeMedia.swappedProps) {
                        const { newMedias } = await this.createMediaObjects([cubeMedia.swappedProps], true);
                        const media: Media = newMedias[0];
                        media.underPositioningObject = true;
                        mediaOBJ.swappedMedia = media;
                    }
                    break;
            }

            if (mediaOBJ) {
                if (!isCreatedByPos) {
                    mediaOBJ.object.position.set(media.position.x, media.position.y, media.position.z)
                    mediaOBJ.object.quaternion.set(media.rotation.x, media.rotation.y, media.rotation.z, media.rotation.w)
                    mediaOBJ.object.scale.set(media.scale.x, media.scale.y, media.scale.z);

                    this.raycastTargets.push(mediaOBJ);
                    this.scene.add(mediaOBJ)

                    // 增加product demo 型錄
                    const product = mediaOBJ as Media;
                    if (product.productId) {
                        const productInfo: ProductType = {
                            id: product.productId,
                            name: product.name,
                            title: product.title,
                            thumbnail: product.thumbnail,
                        }
                        store.dispatch(setProduct(productInfo));
                    }

                    const scope = this;
                    mediaOBJ.addCustumDestory(() => {
                        function removeFromArray(array, obj) {
                            const index = array.indexOf(obj)
                            if (index > -1) {
                                array.splice(index, 1);
                            }
                        }
                        removeFromArray(scope.mediaList, mediaOBJ)
                    })

                    if (mediaOBJ.swappedMedia) {
                        this.mediaList.push(mediaOBJ.swappedMedia)
                        newMedias.push(mediaOBJ.swappedMedia);
                    } else {
                        this.mediaList.push(mediaOBJ)
                        newMedias.push(mediaOBJ);
                    }
                } else {
                    newMedias.push(mediaOBJ);
                }
            }
        };
        return { raycastTargets: this.raycastTargets, newMedias: newMedias };
    }

    public setAutoplay(enable: boolean) {
        this.autoplay = enable;
    }

    public setCaptureMedia(enable: boolean) {
        for (const media of this.mediaList) {
            media.captureMedia(!enable);
            if (
                media instanceof MediaObject.Video ||
                media instanceof MediaObject.Gif) {
                media.object.visible = enable;
            }

        }
    }
}