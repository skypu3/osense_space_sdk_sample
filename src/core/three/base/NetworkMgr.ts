import { ApolloClient, createHttpLink, gql, InMemoryCache, NormalizedCacheObject } from "@apollo/client";
import { setContext } from '@apollo/client/link/context';

const authLink = setContext( ( _, { headers } ) =>
{
    // get the authentication token from local storage if it exists
    const token = `eyJhbGciOiJSUzI1NiIsImtpZCI6IjNlNTQyN2NkMzUxMDhiNDc2NjUyMDhlYTA0YjhjYTZjODZkMDljOTMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdnJtYWtlci1wcm8tZGV2IiwiYXVkIjoidnJtYWtlci1wcm8tZGV2IiwiYXV0aF90aW1lIjoxNjA2MDM1NzY2LCJ1c2VyX2lkIjoiTlNieEc3ZVFrN2JFQzRCTjY5ZVREZVZhWDlFMiIsInN1YiI6Ik5TYnhHN2VRazdiRUM0Qk42OWVURGVWYVg5RTIiLCJpYXQiOjE2MDYwMzU3NjYsImV4cCI6MTYwNjAzOTM2NiwiZW1haWwiOiJpc3RhZ2luZ0BudHVzdC5vcmcudHciLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsiaXN0YWdpbmdAbnR1c3Qub3JnLnR3Il19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.hLKMwW6Qa2-XiWzI16E1m2a7drK-YrKriYIk3xc6dFIymNG2azUzicftNyuVGovKyfMT08miV4qHiHoUWgmZUxhAropHiH6dpoIVMtp0O4OkP98cVlLDeUtGyZS3sueGDHpd1Ge4WjvY6aoUriCkY-PW6rIAyioMRWAcIcq4DrE_tjHHAT0jpWrJGEo39swyGjogzEraaZ1zuYyuL7UD96Iv7QE-xxHgDFmStoJEe0YP6cWMoH6d_620iaTI_UuK7zwoYdDta_3njq5OSaeCBV7FfRDVGocZ4npS3rztmKV4dxmyfFTKsVS5ZV_GhhbHh7hTen9sQK-HrzfgL723aA`//localStorage.getItem( 'token' );
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${ token }` : "",
        }
    }
} );

const httpLink = createHttpLink( {
    uri: 'https://mochar.herokuapp.com/graphql',
} );


export default class NetworkMgr
{

    public client: ApolloClient<NormalizedCacheObject>

    constructor()
    {
        // Start a client
        this.client = new ApolloClient( {
            link: authLink.concat( httpLink ),
            cache: new InMemoryCache()
        } );
    }

    get( query: string, variables: object, callback: ( res ) => void )
    {
        const res = this.client.query(
            {
                query: gql`${ query }`,
                variables: variables,
            }
        );

        callback( res );
    }

    getAsync( query: string, variables: object ): Promise<any>
    {
        return this.client.query(
            {
                query: gql`${ query }`,
                variables: variables,
            }
        );
    }

    write( query: string, data: object )
    {
        this.client.writeQuery(
            {
                query: gql`${ query }`,
                data: data,
            }
        );
    }
}
