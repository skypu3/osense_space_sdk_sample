import * as THREE from 'three';
import OrbitControls from '../OrbitControls';
import Global from './Global';
let self;

export enum MovingMode
{
    Freeform = 0,
    Viewer = 1
}

export default class FirstPersonControls
{
    private canvas: HTMLCanvasElement;
    private controls: OrbitControls;
    private camera: THREE.Camera;
    public movingMode: MovingMode;
    private moveForward: boolean = false;
    private moveBackward: boolean = false;
    private moveLeft: boolean = false;
    private moveRight: boolean = false;
    public isWalking: boolean = false;
    private _enabled: boolean = true;
    private speed: number = 50;
    private prevTime: number = performance.now();
    private velocity = new THREE.Vector3();
    private direction = new THREE.Vector3();

    constructor()
    {
        self = this;
    }
    init( iCamera: THREE.Camera, controls: OrbitControls, threeCanvas: HTMLCanvasElement )
    {
        this.camera = iCamera;
        this.controls = controls;
        this.canvas = threeCanvas;
        this.attachKeyboardEvent();
    }

    set enabled( option: boolean )
    {
        if ( !option )
        {
            this.detachKeyboardEvent();
        } else
        {
            this.attachKeyboardEvent();
        }
    }

    get enabled(): boolean
    {
        return this._enabled
    }

    get walking(): boolean
    {
        return this.isWalking;
    }

    private onKeyDown( event: KeyboardEvent )
    {
        let moveDirection = self.camera.getWorldDirection( new THREE.Vector3() )
        switch ( event.code )
        {
            case 'KeyW':
                self.moveForward = true;
                self.isWalking = true;
                break;

            case 'KeyA':
                moveDirection.applyAxisAngle(
                    new THREE.Vector3( 0, 1, 0 ), Math.PI / 2
                )
                self.moveLeft = true;
                self.isWalking = true;
                break;

            case 'KeyS': // down
                moveDirection.applyAxisAngle(
                    new THREE.Vector3( 0, 1, 0 ), Math.PI
                )
                self.moveBackward = true;
                self.isWalking = true;
                break;

            case 'KeyD':
                moveDirection.applyAxisAngle(
                    new THREE.Vector3( 0, 1, 0 ), -Math.PI / 2
                )// d
                self.moveRight = true;
                self.isWalking = true;
                break;
            default:
                moveDirection = undefined;
                break;
        }

        if ( self.movingMode === MovingMode.Freeform )
        {
            moveDirection = undefined;
        }
        else 
        {
            self.isWalking = false;
        }

        self.goHotspotWithKeyboard( moveDirection );
    }

    private goHotspotWithKeyboard( moveDirection: THREE.Vector3 )
    {
        if ( !moveDirection ) return;

        const animationMgr = Global.inst.animationMgr
        const currentHotspot = animationMgr.hotspotdict[ animationMgr.latestHotspotId ]
        const hotspots = Global.inst.animationMgr.hotspotdict;
        let hotspotKeys = Object.keys( hotspots );
        let minDistance = Infinity;
        let nearestId = undefined;

        hotspotKeys.forEach( hotspot =>
        {
            const info = animationMgr.hotspotdict[ hotspot ]
            const { position, floor, id } = info;
            if ( floor !== currentHotspot.floor || id === animationMgr.latestHotspotId ) return;

            // only neighbor can move !!
            const { neighbors } = currentHotspot
            if ( neighbors && !neighbors.includes( id ) ) return;

            var dir = new THREE.Vector3( position.x - this.camera.position.x, 0, position.z - this.camera.position.z ); // create once an reuse it
            const angle = Math.abs( dir.angleTo( moveDirection ) );

            if ( angle < 0.6 )
            {
                const pos = new THREE.Vector3( currentHotspot.position.x, currentHotspot.position.y, currentHotspot.position.z )
                const distance = pos.distanceTo( new THREE.Vector3( position.x, position.y, position.z ) )

                if ( distance < minDistance )
                {
                    minDistance = distance;
                    nearestId = id
                }
            }
        } );

        if ( nearestId )
            animationMgr.goHotspot( nearestId )
    }

    private onKeyUp( event: KeyboardEvent )
    {
        switch ( event.code )
        {
            case 'KeyW':
                self.moveForward = false;
                self.isWalking = false;
                break;

            case 'KeyA':
                self.moveLeft = false;
                self.isWalking = false;
                break;

            case 'KeyS': // down
                self.moveBackward = false;
                self.isWalking = false;
                break;

            case 'KeyD': // d
                self.moveRight = false;
                self.isWalking = false;
                break;
            default:
                break;
        }
    };

    update()
    {
        const time = performance.now();
        if ( !this._enabled )
        {
            return;
        }

        const target = new THREE.Vector3();
        const vec = new THREE.Vector3();

        const delta = ( time - this.prevTime ) / 1000;

        this.velocity.x -= this.velocity.x * 10.0 * delta;
        this.velocity.z -= this.velocity.z * 10.0 * delta;
        this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
        this.direction.x = Number( this.moveRight ) - Number( this.moveLeft );
        this.direction.normalize();
        if ( this.direction.x !== 0 || this.direction.z !== 0 )
        {
            if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * this.speed * delta;
            if ( this.moveLeft || this.moveRight ) this.velocity.x -= this.direction.x * this.speed * delta;

            vec.setFromMatrixColumn( this.camera.matrix, 0 );
            vec.crossVectors( this.camera.up, vec );
            target.addVectors( this.camera.position, vec.multiplyScalar( - this.velocity.z * delta ) );

            vec.setFromMatrixColumn( this.camera.matrix, 0 );
            target.add( vec.multiplyScalar( - this.velocity.x * delta ) );

            this.setCameraPosition( target, true );
        }

        this.prevTime = time;
    }

    private setCameraPosition( newPos, holdDirection )
    {
        const oldPos = new THREE.Vector3().copy( this.camera.position );

        const deltaPos = new THREE.Vector3(
            newPos.x - oldPos.x,
            newPos.y - oldPos.y,
            newPos.z - oldPos.z
        );

        this.camera.position.x += deltaPos.x;
        this.camera.position.y += deltaPos.y;
        this.camera.position.z += deltaPos.z;

        if ( holdDirection === true )
        {
            this.controls.target.x += deltaPos.x;
            this.controls.target.y += deltaPos.y;
            this.controls.target.z += deltaPos.z;
        }
    }

    private attachKeyboardEvent()
    {
        this.canvas.addEventListener( "keydown", this.onKeyDown );
        this.canvas.addEventListener( "keyup", this.onKeyUp );
    }

    private detachKeyboardEvent()
    {
        this.canvas.removeEventListener( "keydown", this.onKeyDown );
        this.canvas.removeEventListener( "keyup", this.onKeyUp );
    }
}

