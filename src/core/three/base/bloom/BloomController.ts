import * as THREE from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass';
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js';
import { FilmPass } from 'three/examples/jsm/postprocessing/FilmPass.js';
// import { CopyShader }  from 'three/examples/js/shaders/CopyShader';
import fragmentShader from './shader/fragmentShader';
import vertexShader from './shader/vertexShader';

const BLOOM_SCENE = 3;

const darkMaterial = new THREE.MeshBasicMaterial({ color: 'black' });
const materials = {};

export default class BloomComposer {
    private bloomComposer: EffectComposer;
    private finalComposer: EffectComposer;
    private scene: THREE.Scene;
    private camera: THREE.Camera;
    private renderer: THREE.WebGLRenderer;
    private clock: THREE.Clock;
    private bloomLayer: THREE.Layers;

    constructor(IScene: THREE.Scene, ICamera: THREE.Camera, IRenderer: THREE.WebGLRenderer) {
        this.scene = IScene;
        this.camera = ICamera;
        this.renderer = IRenderer;

        const params = {
            bloomStrength: 1.1,
            bloomThreshold: 0.37,
            bloomRadius: 0.02,
            scene: 'Scene with Glow'
        };

        const bloomPass = new UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);
        bloomPass.threshold = params.bloomThreshold;
        bloomPass.strength = params.bloomStrength;
        bloomPass.radius = params.bloomRadius;

        // var effectFilm = new FilmPass(0.5, 0, 256, 0);
        // effectFilm.renderToScreen = true;

        const renderScene = new RenderPass(this.scene, this.camera);

        this.bloomComposer = new EffectComposer(this.renderer);
        this.bloomComposer.renderToScreen = false;
        this.bloomComposer.setSize(window.innerWidth, window.innerHeight)
        this.bloomComposer.addPass(renderScene);
        this.bloomComposer.addPass(bloomPass);
        this.bloomComposer.render()

        const finalPass = new ShaderPass(
            new THREE.ShaderMaterial({
                uniforms: {
                    baseTexture: { value: null },
                    bloomTexture: { value: this.bloomComposer.renderTarget2.texture }
                },
                vertexShader: vertexShader,
                fragmentShader: fragmentShader,
                defines: {}
            }), 'baseTexture'
        );
        finalPass.needsSwap = true;

        this.finalComposer = new EffectComposer(this.renderer);
        this.finalComposer.addPass(renderScene);
        this.finalComposer.addPass(finalPass);
        this.clock = new THREE.Clock()
        this.bloomLayer = new THREE.Layers();
        this.bloomLayer.set(BLOOM_SCENE);
    }

    public setSize() {
        this.bloomComposer.setSize(window.innerWidth, window.innerHeight);
        this.finalComposer.setSize(window.innerWidth, window.innerHeight);
    }

    private darkenNonBloomed(obj) {
        if (obj.isMesh && this.bloomLayer.test(obj.layers) === false) {
            materials[obj.uuid] = obj.material;
            obj.material = darkMaterial;
        }
    }

    private restoreMaterial(obj) {
        if (materials[obj.uuid]) {
            obj.material = materials[obj.uuid];
            delete materials[obj.uuid];
        }
    }

    public composerRender() {
        var delta = this.clock.getDelta();
        this.renderer.clear();
        this.scene.traverse(this.darkenNonBloomed.bind(this));
        this.bloomComposer.render(delta);
        this.scene.traverse(this.restoreMaterial.bind(this));
        this.finalComposer.render(delta);
    }
}