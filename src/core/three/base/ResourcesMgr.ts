import * as THREE from "three";
import { GLTF, GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import axios from 'axios';
import { Format } from "../scene/Format";
import { Mesh } from "three";
const TWEEN = require('@tweenjs/tween.js')

THREE.Cache.enabled = true;

export interface ResDictionary<T> {
    [key: string]: T;
}

export enum ResourcesType {
    Image,
    Model,
    Music,
    Video,
    Cubemap,
    Texture,
    Preload
}

/**
 * 動態資源管理物件
 */
export default class ResourcesMgr {
    private gltfLoader = new GLTFLoader();
    private cubeTextuerLoader = new THREE.CubeTextureLoader();
    private textureloader = new THREE.TextureLoader();
    public mediaResources: { [id: string]: any } = {};
    public videoResources: { [id: string]: Format.IVideoResource } = {};
    public skybox: Mesh;

    /**當前已讀取的檔案存放區 */
    // private textures: ResDictionary<THREE.Texture> = {};
    // private musics: ResDictionary<THREE.Audio> = {};
    // private models: ResDictionary<THREE.Mesh> = {};

    /**
     * 讀取資源
     * @param url 資源路徑
     * @param type 資源類別
     */
    public load(url: string | string[], type: ResourcesType, onProgress?: any): any {
        switch (type) {
            case ResourcesType.Preload:
                return this.preloadCubeMap(url as string[]);
            case ResourcesType.Cubemap:
                return this.loadCubeMap(url as string[]);
            case ResourcesType.Texture:
                return this.loadTexture(url as string);
            case ResourcesType.Image:
            case ResourcesType.Model:
                return this.loadModel(url as string, onProgress);
            case ResourcesType.Music:
            case ResourcesType.Video:
                return this.loadVideo(url as string);
            default:
        }

    }

    /**
     * 讀取模型 (GLTF only)
     * @param url 資源路徑
     */
    private loadTexture(url: string): Promise<THREE.Texture | null> {
        return new Promise((resolve, reject) => {
            this.textureloader.load(url, (texture) => {
                // make image texture not so bright
                texture.encoding = THREE.GammaEncoding;
                resolve(texture)
            }, () => {}, (error) => {
                reject(error)
            })
        });
    }

    /**
     * 讀取模型 (GLTF only)
     * @param url 資源路徑
     */
    private async loadModel(url: string, onProgress?: any): Promise<GLTF> {
        const gltf: GLTF = await this.gltfLoader.loadAsync(url, onProgress ? onProgress : null);
        return gltf;
    }

    public loadEnvCubeMap(data: Format.ISkybox): THREE.Mesh {

        var urls = data.skybox
        const orderUrls = [
            urls[2],
            urls[4],
            urls[0],
            urls[5],
            urls[1],
            urls[3]
        ]

        const materialArray = orderUrls.map(image => {
            let texture = new THREE.TextureLoader().load(image);
            texture.encoding = THREE.GammaEncoding;
            return new THREE.MeshBasicMaterial({ map: texture, side: THREE.DoubleSide, combine: 0, transparent: true });
        });

        const skyboxGeo = new THREE.BoxGeometry(10000, 10000, 10000);
        const skybox = new THREE.Mesh(skyboxGeo, materialArray);
        if (data.shiftY)
            skybox.position.y = data.shiftY

        skybox.userData['animation'] = data.animation ?? false
        skybox.userData['hide'] = data.hide ?? false

        return skybox;
    }

    public enableSkybox(visible: boolean) {
        if (this.skybox && !this.skybox.userData['hide']) return

        if (this.skybox && this.skybox.userData['hide']) {
            this.skybox.visible = visible
        }
    }

    private loadCubeMap(urls: string[]): any {
        const orderUrls = [
            urls[2],
            urls[4],
            urls[0],
            urls[5],
            urls[1],
            urls[3]
        ]
        return new Promise(resolve => {
            this.cubeTextuerLoader.load(orderUrls, resolve);
        })
    }

    private preloadCubeMap(urls: string[]): Promise<any[]> {
        const orderUrls = [
            urls[2],
            urls[4],
            urls[0],
            urls[5],
            urls[1],
            urls[3]
        ]
        const promises = []
        orderUrls.forEach((url) => {
            const promise = axios.get(url);
            promises.push(promise);
        });
        return Promise.all(promises);
    }

    public removeVideo(videoElement: HTMLVideoElement) {
        videoElement.pause();
        videoElement.removeAttribute('src'); // empty source
        videoElement.load();
    }

    private loadVideo(url: string): Promise<any> {
        return new Promise((resolve, reject) => {
            if (url === undefined) {
                reject();
            }
            const video = document.createElement('video');
            const source = document.createElement('source');
            video.setAttribute("playsinline", "");
            source.src = url;
            source.type = 'video/mp4';
            video.crossOrigin = 'anonymous';
            video.loop = true;
            video.muted = true;
            video.appendChild(source);
            video.load()

            video.addEventListener("loadedmetadata", function (e) {
                resolve({
                    video,
                    width: this.videoWidth,
                    height: this.videoHeight
                });
            }, false);
        })
    }
}