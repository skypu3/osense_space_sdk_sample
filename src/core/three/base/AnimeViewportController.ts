import * as THREE from 'three';
import ViewportController from './ViewportController';
import { IAction } from 'core/anime/Timeline';
import store from "core/store/store";
import Global from 'core/three/base/Global';
import { SpaceSdkCmd } from 'core/store/actions/iframe';
import { Tween, Easing } from '@tweenjs/tween.js';
import { Format } from '../scene/Format';
const TWEEN = require('@tweenjs/tween.js')

export default class AnimeViewportController extends ViewportController {
    constructor() {
        super()
        this.isTop = true;

        this.controlBackVecDistance = 10;
        this.controlBackVecHeight = 10;
        this.isHotspotAnimating = false;
        this.isRotation = false;
        this.isCustomFocusBbox = false

    }

    private controlBackVecDistance: number;
    private controlBackVecHeight: number;
    private isCustomFocusBbox: boolean
    private isRotation: boolean;
    public isHotspotAnimating: boolean;
    public easingAnime: string = 'easeInOutSine'

    private rotateTween?: Tween<{ x: number, y: number }>;
    public onRotateComplete?: Function

    public setCustomFocusBbox(backVecDistance: number, backVecHeight: number) {
        this.isCustomFocusBbox = true;
        this.controlBackVecDistance = backVecDistance
        this.controlBackVecHeight = backVecHeight
    }

    setFocusBbox(center: THREE.Vector3, size3D: THREE.Vector3) {
        super.setFocusBbox(center, size3D)
        if (this.isCustomFocusBbox) return
        const size = Math.max(size3D.x, size3D.y, size3D.z);
        this.controlBackVecDistance = size;
        this.controlBackVecHeight = size3D.y;
    }

    public isTop: boolean;

    private walk(dstPos: THREE.Vector3): IAction {
        const scope = this;
        const srcPos = this.getCamerPosition()
        this.setGoinglimit(false);

        return {
            easing: this.easingAnime,
            progress: {

                update: function (percentage) {
                    scope.isHotspotAnimating = true;
                    const position = new THREE.Vector3().lerpVectors(
                        srcPos,
                        dstPos,
                        percentage
                    );
                    scope.setCamerPos(position, true);
                }
            },
            complete: function () {
                scope.isTop = false;
                scope.isHotspotAnimating = false;
                scope.setFirstpersonViewLimit()
                scope.setCamerPos(dstPos, true);
            }
        };
    }

    private goDown(dstPos: THREE.Vector3): IAction {
        const scope = this;
        const srcPos = this.getCamerPosition()
        this.setGoinglimit(false);

        function posEasing(t) {
            return t < 0.5 ? 8 * t ** 4 : 1 - 8 * (t - 1) ** 4;
        }

        function rotEasing(t) {
            return t < 0.5 ? 16 * t ** 5 : 1 + 16 * (t - 1) ** 5;
        }

        const srcPolarAngle = this.polarAngle;
        const dstPolarAngle = Math.PI / 2;
        return {
            targets: scope,
            polarAngle: [srcPolarAngle, dstPolarAngle],
            easing() {
                return rotEasing
            },
            progress: {
                update: function (percentage) {
                    scope.isHotspotAnimating = true;
                    const position = new THREE.Vector3().lerpVectors(
                        srcPos,
                        dstPos,
                        posEasing(percentage)
                    );
                    scope.setCamerPos(position, true);
                }
            },
            complete: function () {
                scope.isTop = false;
                scope.isHotspotAnimating = false;
                scope.setFirstpersonViewLimit()
            }
        };
    }

    public goPosition(dstPos: THREE.Vector3): IAction {
        if (this.isTop)
            return this.goDown(dstPos)
        else
            return this.walk(dstPos)
    }

    public goTop(): IAction {
        
        const posEasing = (t) => {
            return t < 0.5 ? 8 * t ** 4 : 1 - 8 * (t - 1) ** 4;
        };

        const rotEasing = (t) => {
            const time = t - 1;
            return time * time * time + 1;
        };

        this.isTop = true;
        this.setGoinglimit(true);

        // calculate backward position
        let backwardvec3 = new THREE.Vector3();
        backwardvec3 = new THREE.Vector3(0, 0, this.controlBackVecDistance);

        backwardvec3.applyAxisAngle(
            this.camera.up,
            this.azimuthAngle
        );

        const srcPosition = this.getCamerPosition();
        const dstPosition = new THREE.Vector3().addVectors(srcPosition, backwardvec3);
        dstPosition.y = srcPosition.y + this.controlBackVecHeight;

        // calculate backward polarAngle
        const srcPolarAngle = this.polarAngle;
        let dstPolarAngle = 0

        dstPolarAngle = Math.atan2(
            new THREE.Vector2(
                dstPosition.x - srcPosition.x,
                dstPosition.z - srcPosition.z
            ).length(),
            dstPosition.y - srcPosition.y
        );

        const scope = this;
        return {
            progress: {
                update: function (percentage) {
                    scope.setCamerPos(
                        new THREE.Vector3().lerpVectors(
                            srcPosition,
                            dstPosition,
                            posEasing(percentage)
                        ), true)

                    const rotEasingPercentage = rotEasing(percentage);
                    scope.polarAngle =
                        (1 - rotEasingPercentage) * srcPolarAngle +
                        rotEasingPercentage * dstPolarAngle;
                }
            },
            complete: function () {
                scope.setTopViewLimit(srcPosition);
            }
        }
    }

    public setOrbitControlRotation(
        azimuthAngle: number,
        polarAngle: number,
        rotationTime: number = 1000
    ): Promise<void> {
        return new Promise((resolve, reject) => {
            const diffX = Math.abs(azimuthAngle - this.azimuthAngle);
            const diffY = Math.abs(polarAngle - this.polarAngle);
            const lengthSquared = diffX * diffX + diffY * diffY;
            const scope = this;

            if (lengthSquared >= 0.01 && !this.isRotation && !this.isHotspotAnimating) {
                let target;
                if (this.azimuthAngle >= 0 && azimuthAngle >= 0 || this.azimuthAngle < 0 && azimuthAngle < 0) {
                    target = { x: azimuthAngle, y: polarAngle }
                } else {
                    // distance to -Math.PI or Math.PI
                    const up = Math.abs(Math.abs(azimuthAngle) - Math.PI)
                    // distance to 0
                    const down = Math.abs(Math.abs(azimuthAngle) - 0)
                    if (azimuthAngle < 0) {
                        if (up < down) {
                            const azimuthAngle = Math.PI + up
                            target = { x: azimuthAngle, y: polarAngle }
                        } else {
                            const azimuthAngle = -down
                            target = { x: azimuthAngle, y: polarAngle }
                        }
                    } else { // azuimuthAngle >= 0
                        if (up < down) {
                            const azimuthAngle = -Math.PI - up
                            target = { x: azimuthAngle, y: polarAngle }
                        } else {
                            const azimuthAngle = down
                            target = { x: azimuthAngle, y: polarAngle }
                        }
                    }
                }

                let rotate = { x: this.azimuthAngle, y: this.polarAngle }
                let rotateTween = new TWEEN.Tween(rotate)

                rotateTween
                    .to(target, rotationTime) // Move to (300, 200) in 1 second.
                    .easing(TWEEN.Easing.Circular.Out) // Use an easing function to make the animation smooth.
                    .onUpdate(() => {
                        scope.isRotation = true
                        scope.azimuthAngle = rotate.x;
                        scope.polarAngle = rotate.y;
                    })
                    .onComplete(() => {
                        scope.isRotation = false;
                        resolve();
                    })
                    .start() // Start the tween immediately.
            } else {
                resolve()
            }
        });
    }

    private oldCamera: Format.ICameraPose;
    public setCameraRotation(
        azimuthAngle: number,
        polarAngle: number,
        rotationTime: number = 1500,
        easing = TWEEN.Easing.Linear.None,
    ): Promise<void> {
        return new Promise((resolve, reject) => {
            let rotate = { x: this.azimuthAngle, y: this.polarAngle }
            this.rotateTween = new TWEEN.Tween(rotate)
            let target = { x: azimuthAngle, y: polarAngle }
            const scope = this;

            this.oldCamera = Global.inst.animationMgr.getCurrentPoseInfo();
            this.rotateTween
                .to(target, rotationTime)
                .easing(easing)
                .onUpdate(() => {
                    scope.azimuthAngle = rotate.x;
                    scope.polarAngle = rotate.y;
                })
                .onComplete(() => {
                    if (scope.onRotateComplete) {
                        scope.onRotateComplete()
                    }
                    scope.callCameraUpdateEvent();

                    resolve();
                })
                .start() // Start the tween immediately.
        })
    }

    public getCameraRotateAnimator() {
        if (!this.rotateTween) {
            return undefined
        }
        const scope = this;
        return {
            start: () => {
                scope.rotateTween.isPaused() ? scope.rotateTween.resume()
                    : scope.rotateTween.start();
                return scope.getCameraRotateAnimator();
            },
            stop: () => {
                scope.rotateTween.stop();
                scope.callCameraUpdateEvent()
                return scope.getCameraRotateAnimator();
            },
            pause: () => {
                scope.rotateTween.pause();
                scope.callCameraUpdateEvent()
                return scope.getCameraRotateAnimator();
            },
            onComplete: (handler: () => void) => {
                this.onRotateComplete = handler;
                return this.getCameraRotateAnimator();
            }
        }
    }

    public async lookAt(targetPos: THREE.Vector3, duration?: number) {
        if (!duration) {
            this.camera.lookAt(targetPos);
            return;
        }

        const aAngle = Math.atan2(
            this.camera.position.x - targetPos.x,
            this.camera.position.z - targetPos.z,
        );

        await this.setOrbitControlRotation(aAngle, this.polarAngle, duration);
    }

    private callCameraUpdateEvent() {
        let newCamera = Global.inst.animationMgr.getCurrentPoseInfo();
        if (store.getState().iframeReducer !== null) {
            const send = store.getState().iframeReducer.sendMessage as (command: SpaceSdkCmd, data: any) => void
            send(SpaceSdkCmd.cameraupdated, {
                oldCamera: this.oldCamera,
                newCamera
            })
        } else {
            // console.log(` ${newCamera} from ${this.oldCamera}`);
        }
    }

}