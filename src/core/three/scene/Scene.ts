import * as THREE from 'three';
import MediaObject3D, { EObjectType } from "core/three/mediaObject/MediaObject3D";

export default class Scene extends THREE.Scene
{
    constructor()
    {
        super()
        // add Lighting
        this.background = new THREE.Color( 0x000000 );
        const hemiLight = new THREE.HemisphereLight();
        hemiLight.name = 'hemi_light';
        super.add( hemiLight );

        const light1 = new THREE.AmbientLight( 0xffffff, 0.3 );
        light1.name = 'ambient_light';
        super.add( light1 );

        const light2 = new THREE.DirectionalLight( 0xffffff, 0.2 );
        light2.position.set( 0.5, 0, 0.866 ); // ~60º
        light2.name = 'main_light';
        super.add( light2 );

        const light3 = new THREE.DirectionalLight( 0xffffff, 2.5 );
        light2.position.set( -0.5, 0, -0.866 ); // ~60º
        light2.name = 'main_light';
        super.add( light3 );
    }

    public add( ...obj: any[] ): this
    {
        if ( arguments.length > 1 )
        {
            for ( let i = 0; i < arguments.length; i++ )
            {
                this.add( arguments[ i ] );
            }
            return this;
        }

        if ( obj[ 0 ] instanceof MediaObject3D )
        {
            this.postTagProcess( obj[ 0 ].object, obj[ 0 ].type );
            super.add( obj[ 0 ].object );
        }
        else if ( obj )
        {
            // cancel default scene.add
            // console.warn( 'THREE object :', obj )
            super.add( ...obj )
        }

        return this;
    }

    private postTagProcess( obj: THREE.Object3D, type: EObjectType )
    {
        const onBeforeCompileDiscardTransparent = function ( shader )
        {
            function getShader( str, value, index )
            {
                return str.substr( 0, index ) + value + str.substr( index );
            }
            const endIndex = shader.fragmentShader.lastIndexOf( '}' );
            const addTransparentShader = [
                `if(gl_FragColor.a < 0.1){`,
                `   discard;`,
                `}`,
                ``
            ].join( "\n" );
            shader.fragmentShader = getShader( shader.fragmentShader, addTransparentShader, endIndex )
        }

        switch ( type )
        {
            case EObjectType.TEXT:
            case EObjectType.PHOTO:
            case EObjectType.PORTAL:
            case EObjectType.TAG:
                {
                    obj.traverse( child =>
                    {
                        if ( child instanceof THREE.Mesh )
                        {
                            ( child.material as any ).onBeforeCompile = onBeforeCompileDiscardTransparent
                        }
                    } )
                    break;
                }
        }

        // arrange object render order
        switch ( type )
        {
            case EObjectType.MOUSEICON: {
                obj.renderOrder = 99;
                break;
            }
            default: {
                obj.renderOrder = 0;
                break;
            }
        }
    }
}