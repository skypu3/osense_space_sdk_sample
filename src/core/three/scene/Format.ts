import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";
import { EObjectType } from "../mediaObject/MediaObject3D";
import THREE, { Material, Mesh } from 'three';

export enum BehaviorMode {
    DollHouseMode = "DollHouseMode",
    WalkingMode = "WalkingMode"
}

declare namespace Format {
    export interface IScene {
        space: ISpace,
        postProcessing: IPostProcessing,
        hotspots: IHotspot[],
        positioningObjects: IPositioningObject[],
        mediaObjects: IMedia[],
        thumbnail: string,
        name: string,
        startHotspotId?: string,
        azimuthAngle?: number;
        polarAngle?: number;
        social: boolean,
        fixedHotspot?: boolean,
        roomType: string,
        largeAudiences?: boolean,
        audiencesNumber?: number,
        openChatChannel?: string
    }

    export interface ISpace {
        description: string;
        thumbail: string;
        glbUrl: string;
        glb: GLTF;
        id: string;
        isMp: boolean;
        postProcessing?: IPostProcessing;
        skybox?: ISkybox;
        ocean?: boolean;
        addProfileHeight?: number;
        profileFacePoint?: {
            x: number,
            y: number,
            z: number,
        },
        disableHotspots?: boolean;
        goTopDistance?: number
        cameraTopHeight?: number
        cameraHeight?: number
    }

    export interface ISkybox {
        animation: boolean
        hide: boolean
        name: string
        shiftY; number
        skybox: string[]
    }

    export interface IPostProcessing {
        postProcessingEnabled: boolean;
        bloomBlurScale: number;
        bloomIntensity: number;
        bloomOpacity: number;
        luminSmooth: number;
        luminThreshold: number;
        kernelSize: number;
        brightness: number;
        brightnessOpacity: number;
        contrast?: number;
        hue?: number;
        saturation: number;
        saturationOpacity?: number;
        noiseOpacity: number;
        blurOpacity: number;
        vignetteOpacity: number;
        bloomResolution: number;
        bloomBlendMode: number;
        saturationBlendMode: number;
        brightnessBlendMode: number;
        blurEnabled: boolean;
    }

    export interface IHotspot {
        id: string,
        name: string,
        lowerSkybox: string[6],
        cameraHeight: number,
        spaceId: string,
        floor: number,
        hide: boolean,
        limitBody: boolean,
        higherSkybox: string[6],
        neighbors: string[],
        hotspotColor: string,
        hotspotScale: number,
        rotation: {
            x: number,
            y: number,
            z: number,
            w: number
        },
        position: {
            x: number,
            y: number,
            z: number,
        },
        previewUrl?: string
    }

    export interface IMedia {
        id: string,
        type: string,
        rotation: {
            x: number,
            y: number,
            z: number,
            w: number
        },
        position: {
            x: number,
            y: number,
            z: number,
        },
        scale: {
            x: number,
            y: number,
            z: number,
        },
        showBox: boolean,
        showTitle: boolean,
        editable: boolean,
        searchable?: boolean,
        openLink: string,
        name: string,
        title: string,
        fileUrl: string,
        defaultWidth: number,
        followCamera: boolean,
        isRaycastable: boolean,
        glbObjectUrl?: string,
        usdzObjectUrl?: string,
        purchaseInfo?: string,
        productId?: string,
        isFrame?: boolean,
        moduleId?: string,
        tokenId?: string,
        nftSource?: string,
        floorTargetId?: string,
        componentName?: string,
        componentData?: string,
        targetHotspotId?: string,
        hotspotAnimation?: boolean,
        azimuthAngle?: number,
        thumbnail: string,
        imageHash: string
        polarAngle?: number
    }

    export interface IPositioningObject {
        type: string,
        id: string,
        rotation: {
            x: number,
            y: number,
            z: number,
            w: number
        },
        position: {
            x: number,
            y: number,
            z: number,
        },
        size: {
            x: number,
            y: number,
            z: number,
        },
    }

    export interface IVideoResource {
        width: number
        height: number
        video: HTMLVideoElement
        mesh: Mesh
    }

    export interface ITagData {
        id: string
        type: string
        canOpenComponent: boolean
        moduleId?: string
        tokenId?: string
        play?: boolean
        muted?: boolean
        url?: string
        sdkModal?: boolean
    }

    export interface IModalData {
        id: string
        name: string
        url: string
        glbUrl: string,
        usdzUrl: string,
        purchaseInfo: string,
    }

    export interface IText extends IMedia {
        isVertical: boolean,
        extData: {
            label: string,
            fontWeight: string,
        }
        fontStyle: string,
        fontColor: string,
        fontSize: number,
        lineSpace: number,
        aligment: string,
        angle: number,
    }

    export interface ISprite extends IMedia {
        maxWidth: number,
        verticalNumber: number,
        horizontalNumber: number,
        frameNumber: number,
        speed: number
        isVertical: boolean,
        fontSize?: number,
        text?: string;
        texts?: string[];
    }

    export interface INewTag extends IMedia {
        maxWidth: number,
        verticalNumber: number,
        horizontalNumber: number,
        frameNumber: number,
        speed: number,
        isVertical: boolean,
        iconType: string,
        fontSize?: number,
        text?: string;
        texts?: string[];
        animation: string
    }


    export interface IImage extends IMedia {
        maxWidth: number,
        isVertical: boolean,
        showHotspotTag: boolean,
        hotspotIconIndex: number,
        length: number,
        HotspotTagWidth: number,
        showTitle: boolean,
        showLine: boolean,
        isThickness: boolean,
        animation: string
    }

    export interface IGif extends IMedia {
        maxWidth: number,
        isVertical: boolean,
    }

    export interface IVideo extends IMedia {
        isVertical: boolean,
        videoType: "youtube" | "vimeo" | "greenVideo" | "h5Video",
        videoId: string,
        autoPlay: boolean,
        autoAudio: boolean,
    }

    export interface IModel extends IMedia {
        animation: string
        animationList: string[];
    }

    export interface ITag extends IMedia {
        extData: {
            length: number,
            label: string,
        }
        iconWidth: number,
        showTitle: boolean,
        showLine: boolean,
        isVertical: boolean,
    }

    export interface IWeb extends IMedia {
        isVertical: boolean,
        extData: {
            webPage: string,  // ? equal to openLink ?
        }
    }

    export interface IPositionPlane extends IMedia {
        size: { x: number, y: number };
        positionObjectsCount: number;
        swappedType: string;
        swappedProps: any;
        isVertical: boolean;
        fit: string;
    }

    export interface IPositionCube extends IMedia {
        size: { x: number, y: number, z: number };
        positionObjectsCount: number;
        swappedType: EObjectType.MODEL;
        swappedProps: any;
        fit: string;
    }

    export type IMarker = (IText | IVideo | IImage | ITag | IText | IModel | IGif)

    export interface iCameraRotate {
        azimuthAngle: number,
        polarAngle: number,
    }

    export interface ICameraPose {
        position: { x: number, y: number, z: number };
        rotation: iCameraRotate;
        zoom: number;
    }

    export interface ICameraPoseInfo extends ICameraPose {
        hotspotId: string;
        mode: BehaviorMode;
    }

    export interface IProfileClickInfo {
        type: 'video' | 'photo'
        userId: string,
        streamType?: string,
        fileUrl?: string
    }

    export interface IMediaClickInfo {
        type: 'share_screen' | 'youtube' | 'image',
        userId: string,
        fileUrl?: string
    }
}
export type { Format };
