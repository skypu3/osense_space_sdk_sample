import * as THREE from 'three'

interface IBackgroundConfig
{
    fillStyle: string;
}

interface ITextConfig
{
    labels: string | string[];
    fontWeight: string;
    fontStyle: string;
    fontColor: string;
    padding: number;
    fontSize: number;
    aligment: string;
    lineSpace: number;
}

export default class TextTexture extends THREE.Texture
{
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;

    private textConfig: ITextConfig;
    private backgroundConfig: IBackgroundConfig;

    private padding: number;
    private fontSize: number;
    private aligment: string
    private font: string;
    private maxWidth: number;
    private lines: number;
    private lineSpace: number;

    constructor( texconfig: ITextConfig, backgroundconfig: IBackgroundConfig )
    {
        const canvas = document.createElement( 'canvas' );
        const context = canvas.getContext( '2d' );

        super( canvas )

        this.canvas = canvas;
        this.context = context;

        this.textConfig = texconfig
        this.backgroundConfig = backgroundconfig;
        this.maxWidth = 1100;
        this.create()

    }

    public get ratio()
    {
        return this.canvas.width / this.canvas.height;
    }

    public get lineNumber()
    {
        return this.lines;
    }

    public get width()
    {
        return this.canvas.width / this.maxWidth
    }

    public get height()
    {
        return this.canvas.height / this.maxWidth
    }

    private create()
    {
        const { labels, fontWeight, fontStyle, fontColor, padding, aligment, fontSize, lineSpace } = this.textConfig;

        this.fontSize = fontSize;
        this.aligment = aligment;
        this.padding = padding;
        this.lineSpace = lineSpace;

        this.context.clearRect( 0, 0, this.canvas.width, this.canvas.height );
        this.font = fontStyle + " " + fontWeight + " " + fontSize + "px Arial";
        this.context.font = this.font

        const { textArray, canvasWidth, canvasHeight } = this.wrapText( labels );

        this.lines = textArray.length;
        this.canvas.width = canvasWidth
        this.canvas.height = canvasHeight

        if ( this.backgroundConfig )
        {
            const { fillStyle } = this.backgroundConfig;
            this.context.fillStyle = fillStyle;
            this.context.fillRect( 0, 0, this.canvas.width, this.canvas.height );
        }

        let x = 0;
        let y = this.padding / 2;
        if ( this.aligment == "center" )
            x = this.canvas.width / 2
        else if ( this.aligment == "left" )
            x = this.padding / 2;
        else if ( this.aligment == "right" )
            x = this.canvas.width - this.padding / 2;

        textArray.forEach( line =>
        {
            this.context.textBaseline = "top"
            this.context.font = this.font;
            this.context.fillStyle = fontColor;
            this.context.textAlign = this.aligment as CanvasTextAlign;
            this.context.fillText( line, x, y );
            y += fontSize + lineSpace;
        } );
    }

    private wrapText( labels )
    {

        let canvasWidth, canvasHeight = 0;
        let textArray = [];
        if ( typeof labels != 'string' && !Array.isArray( labels ) )
        {
            return;
        }
        else if ( Array.isArray( labels ) ) 
        {
            this.context.font = this.font;
            textArray = labels;
            let textWidth = 0;
            labels.forEach( element =>
            {
                let metrics = this.context.measureText( element );
                if ( textWidth < metrics.width )
                    textWidth = metrics.width;
            } );
            canvasWidth = textWidth + this.padding;
            canvasHeight = this.fontSize * textArray.length + this.padding + 20;
        }
        else if ( typeof labels == 'string' )
        {

            let arrText = ( labels as string ).split( '' );
            textArray = [];
            let line = '';
            let lineHeight = this.fontSize;

            this.context.font = this.font;
            let metrics = this.context.measureText( labels );
            if ( metrics.width < this.maxWidth )
            {
                canvasWidth = metrics.width + this.padding;
                canvasHeight = this.fontSize + this.padding;
                textArray = [ labels ];
            }
            else
            {
                canvasWidth = this.maxWidth + this.padding;
                canvasHeight = this.fontSize + this.padding;

                for ( var n = 0; n < arrText.length; n++ )
                {
                    var testLine = line + arrText[ n ];
                    this.context.font = this.font;
                    metrics = this.context.measureText( testLine );
                    var testWidth = metrics.width;
                    if ( testWidth > this.maxWidth && n > 0 )
                    {
                        textArray.push( line )
                        line = arrText[ n ];
                        canvasHeight += lineHeight + this.lineSpace;
                    } else
                    {
                        line = testLine;
                    }
                }
                textArray.push( line );
            }
        }
        return { textArray: textArray, canvasWidth: canvasWidth, canvasHeight: canvasHeight };
    }
}