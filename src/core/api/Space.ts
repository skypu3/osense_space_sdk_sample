import Global from "../three/base/Global"

export const getSpace = ( id: string ) =>
{
    return Global.inst.network.getAsync(
        `query GetSpace($id: ID!){
            space(id: $id) {  	
                id
                isMp
                hotSpots {
                id
                position {
                  x
                  y
                  z
                }
                rotation {
                  x
                  y
                  z
                  w
                }
                floor
                neighbors
                cameraHeight
                previewUrl
                higherSkybox
                lowerSkybox
              }
              name
              glbUrl
              thumbnail
            }
        }`,
        { 'id': id }
    )
}

