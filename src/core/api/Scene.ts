import { setFloors } from 'core/store/actions/floor';
import { SpaceSdkCmd } from 'core/store/actions/iframe';
import { SceneType, setScene } from 'core/store/actions/scene';
import store from 'core/store/store';
import { jsonWebWorker } from '@cheprasov/json-web-worker';
import { setEnterprise } from 'core/store/actions/enterprise';
import Global from 'core/three/base/Global';

export function loadjson( url: string ): Promise<any>
{
    return new Promise( resolve =>
    {
        fetch( url ).then( response =>
        {
            return response.json();
        } ).then( data =>
        {
            resolve( data )
        } ).catch( function ( error )
        {
            console.warn( 'media json not found' )
            resolve( undefined )
        } );
    } )
}

export async function getTemplateData( spaceId: string ): Promise<any>
{
    let space = undefined;
    if ( space === undefined )
    {
        const scene = await loadjson( `${ process.env.REACT_APP_API_URL }/space?id=${ spaceId }` );
        if ( scene != undefined )
        {
            space = scene;
        }
    }
    return new Promise( resolve =>
    {
        if ( space && space.glbUrl && space.hotSpots )
        {
            resolve( {
                space: space,
                hotspots: space.hotSpots,
                tagsData: [],
                mediaObjects: [],
                positioningObjects: []
            } )
        }
        else
        {
            resolve( undefined )
        }
    } )
}

export async function getSceneData( sceneId: string ): Promise<any>
{
    let sceneUrl = `./api/${ sceneId }.json`;

    // if localhost開發模式則自動帶入osense sdk key
    if ( window.location.hostname.includes( 'localhost' )
        || store.getState().iframeReducer == null )
    {
        const enterpriseApi = `${ process.env.REACT_APP_API_URL }/enterprise?key=${ process.env.REACT_APP_SDK_KEY }`;
        const json = await loadjson( enterpriseApi )
        store.dispatch( setEnterprise( json.data ) );
        Global.inst.sdkKey = process.env.REACT_APP_SDK_KEY
    }

    const sceneData = store.getState().sceneReducer as SceneType;
    let batchData = undefined;
    let medias = undefined;
    let positionObject = undefined;
    let space = undefined;
    let scene = undefined;

    space = await loadjson( sceneUrl );
    if ( space === undefined )
    {
        sceneUrl = `${ process.env.REACT_APP_API_URL }/scene/basic?id=${ sceneId }`;
        const sceneData = await loadjson( sceneUrl );

        let spaceData = await loadjson( `${ process.env.REACT_APP_API_URL }/space/?id=${ sceneData.spaceId }` );

        if ( sceneData != undefined )
        {
            space = spaceData
            scene = sceneData;
            scene.space = space
        }
    } else
    {
        scene = space;
        space = scene.space;
    }

    if ( scene )
    {
        store.dispatch( setScene( scene ) );
    }

    if ( store.getState().iframeReducer !== null )
    {
        const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
        send( SpaceSdkCmd.sceneinfo, {
            name: scene.name,
            thumbnail: scene.thumbnail,
            imageHash: scene.imageHash,
            music: scene.music,
        } );
        console.log( 'SDK send scene info ' + scene.name )
    }

    positionObject = []

    let tagUrl = `./media3d/${ sceneId }.json`
    let mediaData = await loadjson( tagUrl )

    if ( sceneData && sceneData.tags )
    {
        // support editor
        medias = sceneData.tags;
    }
    else if ( mediaData )
    {
        medias = mediaData ? mediaData.mediaObjects : undefined;
    }
    else
    {
        mediaData = await loadjson( `${ process.env.REACT_APP_API_URL }/tag?id=${ sceneId }` );
        medias = mediaData ? mediaData : undefined;
    }

    if ( store.getState().iframeReducer !== null )
    {
        const send = store.getState().iframeReducer.sendMessage as ( command: SpaceSdkCmd, data: any ) => void;
        send( SpaceSdkCmd.taginfo, medias );
        console.log( 'SDK send tag info ' + scene.name )
    }

    return new Promise( resolve =>
    {
        if ( space && space.glbUrl && space.hotSpots )
        {
            resolve( {
                space: space,
                scene: scene,
                hotspots: space.hotSpots,
                tagsData: batchData ? batchData.extras.tags : [],
                mediaObjects: medias ?? [],
                positioningObjects: positionObject ?? []
            } )
        }
        else
        {
            resolve( undefined )
        }
    } )
}