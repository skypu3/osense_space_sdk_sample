import i18n from 'i18next';
import translation from './zh.json';
import appTranslation_zh from 'app/i18n/zh.json';
import { initReactI18next } from 'react-i18next';

const i18nData = Object.assign( {}, translation, appTranslation_zh );

export const resources = {
    zh: {
        translation: i18nData
    },
} as const;

i18n.use( initReactI18next ).init( {
    fallbackLng: 'zh',
    lng: 'zh',
    resources,
} );

export default i18n;