export const SET_MODE = 'SET_MODE';

export enum ViewerMode
{
    Editor = 0,
    Viewer = 1
}

interface ISetMode
{
    type: typeof SET_MODE,
    payload: ViewerMode
}

export const setMode = ( mode: ViewerMode ): ISetMode => ( {
    type: SET_MODE,
    payload: mode,
} );

export type ModeActionType = ISetMode;
