export const SET_CAPTURE = 'SET_CAPTURE';

interface ISetCapture {
    type: typeof SET_CAPTURE,
    payload: {
        capture: () => void;
    }
}

export const setCapture = (capture: () => void) : ISetCapture => ({
    type: SET_CAPTURE,
    payload: {
        capture,
    }
});

export type SceneActionType = ISetCapture;