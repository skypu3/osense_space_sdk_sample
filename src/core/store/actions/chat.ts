export const SET_CHAT = 'SET_CHAT';

interface IChat {
    openChat?: boolean
    privateChat?: boolean
}

interface ISetChat {
    type: typeof SET_CHAT,
    payload: IChat
}

export const setChat = (chat: IChat) : ISetChat => ({
    type: SET_CHAT,
    payload: chat
});


export type SetChatActionType = ISetChat;
export type ChatType = IChat;