export const SET_ENTERPRISE = 'SET_ENTERPRISE';

interface IEnterprise {
    name: string,
    sdkKey: string,
    agoraAppId?: string,
    sendBirdAppId?: string
}

interface ISetEnterprise {
    type: typeof SET_ENTERPRISE,
    payload: IEnterprise
}

export const setEnterprise = (enterprise: IEnterprise) : ISetEnterprise => ({
    type: SET_ENTERPRISE,
    payload: enterprise
});

export type SetEnterpriseActionType = ISetEnterprise;
export type EnterpriseType = IEnterprise;