export const SET_IFRAME_HANDLE = 'SET_IFRAME_HANDLE';

export enum SpaceSdkCmd
{
    sceneinfo,
    taginfo,
    progressbar,
    sceneready,
    floorupdated,
    hotspotupdated,
    modeupdated,
    cameraupdated,
    userhasinteract,
    createobject,
    deleteobject,
    selectobject,
    setmovemode,
    setdollhousemode,
    opencomponent,
    redirecturl,
    enableui,
    opentag,
    closecomponent,
    setusernotification,
    sendmessageboxwarning,
    membercount, // how many users join the main channel
    memberjoin, // users join main channel
    memberleave, // users leave main channel
    memberupdated, // update micro and video status
    memberfollowme, // send follow me message
    memberfollowcount,
    raisehand, // how many users follow me
    memberunfollow, // send unfollow status to parent UI
    memberclick,
    updatemicrophonestatus,
    updatevideostatus,
    socialmediaclick,
    updatesharescreenstatus,
    socialprofileclick
}

interface ISetIframeHandler
{
    type: typeof SET_IFRAME_HANDLE,
    payload: {
        sendMessage: ( command: SpaceSdkCmd, data: any ) => void
    }
}

export const setIframeHandler = ( sendMessage: any ): ISetIframeHandler => ( {
    type: SET_IFRAME_HANDLE,
    payload: {
        sendMessage,
    }
} );

export type IframeHandlerActionType = ISetIframeHandler;