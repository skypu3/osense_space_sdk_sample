import { Format } from "core/three/scene/Format";

export const SET_LAST_TAG = 'SET_LAST_TAG';

interface ISetLastTag {
    type: typeof SET_LAST_TAG,
    payload: Format.ITagData
}

export const setLastestTag = (tag: Format.ITagData) : ISetLastTag => ({
    type: SET_LAST_TAG,
    payload: tag
});


export type SetLastestTagActionType = ISetLastTag;
export type TagType = Format.ITagData;