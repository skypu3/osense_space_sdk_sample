export const SET_PRODUCT_CATALOG = 'SET_PRODUCT_CATALOG';
export const SET_SYNC = 'SET_SYNC';

interface IControlStatus {
    productCatalog: boolean;
    sync: boolean
}

interface ISetProductCatalog {
    type: typeof SET_PRODUCT_CATALOG,
    payload: boolean
}

export const setProductCatalog = (productCatalog: boolean) : ISetProductCatalog => ({
    type: SET_PRODUCT_CATALOG,
    payload: productCatalog
});

interface ISetSync {
    type: typeof SET_SYNC,
    payload: boolean
}

export const setSync = (sync: boolean) : ISetSync => ({
    type: SET_SYNC,
    payload: sync
});

export type SetProductCatalogActionType = ISetProductCatalog
export type SetSyncActionType = ISetSync
export type ControlType = IControlStatus