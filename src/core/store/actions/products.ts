export const SET_PRODUCT = 'SET_PRODUCT';

interface IProduct
{
    id: string
    name: string,
    title: string,
    thumbnail: string
}

interface ISETProduct
{
    type: typeof SET_PRODUCT,
    payload: IProduct
}

export const setProduct = ( product: IProduct ): ISETProduct => ( {
    type: SET_PRODUCT,
    payload: product
} );

export type ProductType = IProduct;
export type SetProductActionType = ISETProduct;