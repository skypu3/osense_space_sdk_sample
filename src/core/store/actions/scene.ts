import { Format } from "core/three/scene/Format";

export const SET_SCENE = 'SET_SCENE';

export enum RoomType {
    webinar = "webinar",
    social = "social"
}

interface ISetScene
{
    type: typeof SET_SCENE,
    payload: IScene
}

// todo userId === session token
interface IScene
{
    sceneId: string
    data: Format.IScene
    tags: any[]
    space: Format.ISpace
    roomType: RoomType,
}

export const setScene = ( data: any ): ISetScene => ( {
    type: SET_SCENE,
    payload: {
        sceneId: data.id ?? data.sceneId,
        data,
        tags: data.tags,
        space: data.space,
        roomType: RoomType[data.roomType]
    }
} );

export type SceneActionType = ISetScene;
export type SceneType = IScene;