export const SET_FLOORS = 'SET_Floors';

interface ISetFloors {
    type: typeof SET_FLOORS,
    payload: IFloor[]
}

interface IFloor {
    id: string
    name: string;
    azimuthAngle?: number;
    polarAngle?: number;
}

export const setFloors = (floorData: { [key: string]: any }): ISetFloors => ({
    type: SET_FLOORS,
    payload: Object.entries(floorData).map(([key, value]) => {
        return <IFloor> {
            id: key,
            name: value.name,
            azimuthAngle: value.azimuthAngle,
            polarAngle: value.polarAngle
        }
    })
});

export type FloorsActionType = ISetFloors;
export type FloorType = IFloor