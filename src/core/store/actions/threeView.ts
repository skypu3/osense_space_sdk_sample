export const SET_THREE_VIEW = 'SET_THREE_VIEW';

interface ISetThreeView
{
    type: typeof SET_THREE_VIEW,
    payload: any
}

export const setThreeView = ( view: any ): ISetThreeView => ( {
    type: SET_THREE_VIEW,
    payload: view
} );

export type ThreeViewActionType = ISetThreeView;