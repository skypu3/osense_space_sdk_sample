import { ControlType } from '../actions/control';
import { SET_PRODUCT_CATALOG, SET_SYNC } from '../actions/control';

const reducer = (state: ControlType = { productCatalog: false, sync: false }, 
    action: any): ControlType => {
    switch (action.type) {
        case SET_PRODUCT_CATALOG:
            return { productCatalog: action.payload, sync: state.sync}
        case SET_SYNC:
            return { productCatalog: state.productCatalog, sync: action.payload}
        default:
            return state;
    }
};

export default reducer;