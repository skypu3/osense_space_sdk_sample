import { ModeActionType, ViewerMode, SET_MODE } from '../actions/mode';

const reducer = (state = ViewerMode.Editor, action: ModeActionType) => {
    switch(action.type) {
        case SET_MODE:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;