import { SceneActionType, SET_CAPTURE } from '../actions/capture';

const reducer = (state = null, action: SceneActionType) => {
    switch(action.type) {
        case SET_CAPTURE:
            return action.payload.capture;
        default:
            return state;
    }
};

export default reducer;