import { SET_FLOORS, FloorType, FloorsActionType } from '../actions/floor';

const reducer = (state = null, action: FloorsActionType): FloorType[] => {
    switch (action.type) {
        case SET_FLOORS:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;