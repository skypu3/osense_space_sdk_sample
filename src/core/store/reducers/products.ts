import { SetProductActionType, ProductType, SET_PRODUCT } from '../actions/products';

const reducer = (state = [], action: SetProductActionType) : ProductType[] => {
    switch (action.type) {
        case SET_PRODUCT:
            return [...state, action.payload];
        default:
            return state;
    }
};

export default reducer;