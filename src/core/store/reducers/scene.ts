import { SceneActionType, SET_SCENE, SceneType } from '../actions/scene';

const reducer = (state = null, action: SceneActionType) : SceneType => {
    switch(action.type) {
        case SET_SCENE:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;