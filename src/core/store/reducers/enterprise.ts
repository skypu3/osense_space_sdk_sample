import { SetEnterpriseActionType, EnterpriseType, SET_ENTERPRISE } from '../actions/enterprise';

const reducer = (state : EnterpriseType = null, action: SetEnterpriseActionType) : EnterpriseType  => {
    switch(action.type) {
        case SET_ENTERPRISE:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;