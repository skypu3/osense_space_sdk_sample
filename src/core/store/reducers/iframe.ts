import { IframeHandlerActionType, SET_IFRAME_HANDLE } from '../actions/iframe';

const reducer = (state = null, action: IframeHandlerActionType) => {
    switch(action.type) {
        case SET_IFRAME_HANDLE:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;