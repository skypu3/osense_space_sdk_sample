import { ThreeViewActionType, SET_THREE_VIEW } from '../actions/threeView';

const reducer = (state = null, action: ThreeViewActionType) => {
    switch(action.type) {
        case SET_THREE_VIEW:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;