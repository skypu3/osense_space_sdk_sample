import { SetLastestTagActionType, TagType, SET_LAST_TAG } from '../actions/tag';

const reducer = (state : TagType = null, action: SetLastestTagActionType) : TagType  => {
    switch(action.type) {
        case SET_LAST_TAG:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;