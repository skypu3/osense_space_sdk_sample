import { SetChatActionType, ChatType, SET_CHAT } from '../actions/chat';

const reducer = (state: ChatType = { openChat: false, privateChat: false }, action: SetChatActionType) : ChatType => {
    switch(action.type) {
        case SET_CHAT:
            return { ...state, ...action.payload }
        default:
            return state;
    }
};

export default reducer;