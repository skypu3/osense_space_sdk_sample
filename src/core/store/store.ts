import { createStore, combineReducers, applyMiddleware } from 'redux';
import threeViewReducer from './reducers/threeView';
import sceneReducer from './reducers/scene';
import floorReducer from './reducers/floor';
import iframeReducer from './reducers/iframe';
import productsReducer from './reducers/products';
import enterpriseReducer from './reducers/enterprise';
import controlReducer from './reducers/control';
import chatReducer from './reducers/chat';
import tagReducer from './reducers/tag';
import captureReducer from './reducers/capture';
import modeReducer from './reducers/mode';
import thunkMiddleware from "redux-thunk";

const rootReducer = combineReducers({
    threeViewReducer,
    sceneReducer,
    floorReducer,
    iframeReducer,
    productsReducer,
    controlReducer,
    enterpriseReducer,
    chatReducer,
    captureReducer,
    modeReducer,
    tagReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

export type storeTypes = ReturnType<typeof rootReducer>;

export default store;