import anime from 'animejs';
import { AnimeTimelineInstance } from 'animejs';
import Timeline from 'core/anime/Timeline';

export default class Animator {
    public isPlaying: boolean;
    public timelines: AnimeTimelineInstance[];

    constructor() {
        this.isPlaying = false;
        this.timelines = []
    }

    private end() {
        this.isPlaying = false;
        this.timelines = []
    }

     /**
     * stop anime
     */
    public cancel() {
        let activeInstances = anime.running;
        this.timelines.forEach(animation => {
            let index = activeInstances.indexOf(animation);
            activeInstances.splice(index, 1);
        })
        this.end();
    }

    /**
     * start anime
     */
    play(animations: Timeline[]): Promise<void> {
        if (this.isPlaying) {
            console.warn('animation is playing')
            return new Promise(resolve => { resolve(); })
        }
        this.isPlaying = true;
        let self = this;
        /**
         * create timeline instance from configs
         */
        animations.forEach((timeline: Timeline) => {
            this.timelines.push(timeline.inst);
        })

        /**
         *  do anime one-by-one
         */
        return new Promise<void>((resolve, reject) => {
            let currentPlayIndex = 0;

            function play() {
                if (self.timelines.length <= currentPlayIndex) reject();

                self.timelines[currentPlayIndex].play();
                self.timelines[currentPlayIndex].finished.then(playNextAnime);
            }

            function playNextAnime() {
                currentPlayIndex++;
                if (self.timelines.length <= currentPlayIndex) {
                    resolve();
                } else {
                    play()
                }
            }
            play();
        }).then(this.end.bind(this));
    }
}