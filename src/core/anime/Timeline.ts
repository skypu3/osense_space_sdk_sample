import anime, { AnimeAnimParams } from "animejs";
import { AnimeTimelineInstance } from 'animejs';

export interface IAction extends AnimeAnimParams
{
    progress?: {
        inverse?: boolean;
        update?( percentage: number ): void
    }
};

export default class Timeline
{
    private duration: number;
    private concurrentActions: IAction[];

    constructor( duration: number )
    {
        this.duration = duration;
        this.concurrentActions = [];
    }

    addAction( action: IAction )
    {
        this.concurrentActions.push( action );
    }

    public get inst(): AnimeTimelineInstance
    {
        const timeline = anime.timeline( {
            autoplay: false,
            duration: this.duration
        } );

        this.concurrentActions.forEach( ( action: IAction ) =>
        {
            let animeParams: AnimeAnimParams = {
                easing: 'easeInOutCirc', 
                ...action
            }
            const { progress } = action;

            if ( animeParams.targets )
            {
                animeParams.update = function animate( anim )
                {
                    const percentage = anim.progress / 100;

                    if ( progress.inverse === true )
                    {
                        progress.update( 1 - percentage );
                    } else
                    {
                        progress.update( percentage );
                    }
                }

            } else if ( 'progress' in action )            
            {
                let easingValue = {
                    progress: 0
                }
                let targetArray: any = [ easingValue ];
                animeParams.targets = targetArray;
                animeParams.progress = [ 0, 1 ];

                animeParams.update = function animate( anim )
                {
                    const percentage = easingValue.progress;

                    if ( progress.inverse === true )
                    {
                        progress.update( 1 - percentage );
                    } else
                    {
                        progress.update( percentage );
                    }
                }
            }

            timeline.add( animeParams, 0 );
        } )

        return timeline;
    }

}